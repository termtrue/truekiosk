﻿module.exports = function (app) {
    var kiosk = require('../controllers/kiosk.controller');
    var utility = require('../helper/utility.helper');

    app.get('/kiosk', utility.helper, kiosk.render);
    app.get('/kiosk/dailybill', utility.helper, kiosk.dailybill);


    app.post('/kiosk/dailyrecheck', utility.helper, kiosk.submitRecheckData);
    app.post('/kiosk/main', utility.helper, kiosk.kioskMain);
};

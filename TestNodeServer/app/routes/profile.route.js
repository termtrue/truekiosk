﻿module.exports = function (app) {
    var profileController = require('../controllers/profile.controller');
    var utility = require('../helper/utility.helper');

    app.get('/profile', utility.helper, profileController.render);
 
};

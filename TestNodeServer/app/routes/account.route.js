module.exports = function (app) {
    var changePassword = require("../controllers/changePassword.controller");
    var forgotPassword = require("../controllers/forgotPassword.controller");

    app.get('/changePassword_init', changePassword.render);
    app.post('/changePassword_change',changePassword.submit);
    app.post('/changePassword_back', changePassword.back);

    
    app.get('/forgotPassword', forgotPassword.render);
    app.post('/forgotPassword_send', forgotPassword.sendEmail);
    app.post('/forgotPassword_back', forgotPassword.back);
};
var multer = require('multer');
var upload = multer({storage:'advertUploads/'})

module.exports = function (app) {
    var advertController = require('../controllers/advert.controller');
    var utility = require('../helper/utility.helper');

    app.get('/advert', utility.helper,  advertController.render);
    app.post('/advert_searchDataTable', utility.helper, advertController.searchDataTable);
	
    //Detail advert page
	app.get('/advert_add', utility.helper, advertController.add);
 	app.post('/advert_add_save', utility.helper, upload.any(), advertController.addSave);
 	app.get('/advert_delete', utility.helper, advertController.delete);
 	app.post('/advert_edit_save', utility.helper, advertController.editSave);
 	app.post('/advert_add_back', utility.helper, advertController.back);

	//Config page
	app.get('/advert_config', utility.helper,  advertController.config);
	app.get('/config_edit_save', utility.helper, advertController.configEditSave);

};
﻿module.exports = function (app) {
    var userController = require('../controllers/user.controller');
    var utility = require('../helper/utility.helper');

    app.get('/user', utility.helper, userController.render);    

    app.post('/user/logUpload', utility.helper, userController.render_LogUpload);
    app.post('/user/upload', utility.helper, userController.render_upload);
    app.post('/user_search', utility.helper, userController.search);
    app.post('/user_initialData', utility.helper, userController.initialData);   
    app.post('/user/upload_submit', utility.helper, userController.userUpload);
   
};

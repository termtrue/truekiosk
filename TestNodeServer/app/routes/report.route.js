module.exports = function (app) {
    var chart01 = require("../controllers/report01.controller");
    var chart02 = require("../controllers/report02.controller");
    var chart02_01 = require("../controllers/report02_01.controller");
    var chart02_03 = require("../controllers/report02_03.controller");
    var chart03 = require("../controllers/report03.controller");
    var dashboard = require("../controllers/dashboard.controller");
    var chart04 = require("../controllers/report04.controller");
    var chart05 = require("../controllers/report05.controller");
    var chart05_01 = require("../controllers/report05_01.controller");
    var chart06 = require("../controllers/report06.controller");
    var chart07 = require("../controllers/report07.controller");
    var utility = require('../helper/utility.helper');

    app.get('/report_01', utility.helper, chart01.render);
    app.post('/report_01', utility.helper, chart01.submit);
    app.get('/report_02', utility.helper, chart02.render);  

   

    app.get('/report_02_01', utility.helper, chart02_01.render);
    app.get('/report_02_01_search', utility.helper, chart02_01.render);
    app.post('/report_02_01_search', utility.helper, chart02_01.search);
    app.post('/report_02_01_initialData', utility.helper, chart02_01.initialData);
    app.post('/report_02_01_initialChannel', utility.helper, chart02_01.initialChannel);
    
    app.post('/report_02_01_DetailData', utility.helper, chart02_01.detailData);
    app.post('/report_02_01_export', utility.helper, chart02_01.export); 

    app.get('/report_02_03', utility.helper, chart02_03.render);
    app.get('/report_02_03_search', utility.helper, chart02_03.render);
    app.post('/report_02_03_search', utility.helper, chart02_03.search);
    app.post('/report_02_03_initialData', utility.helper, chart02_03.initialData);
    app.post('/report_02_03_InsertCallLog', utility.helper, chart02_03.InsertCallLog);
    app.post('/report_02_03_export', utility.helper, chart02_03.export); 

    app.get('/report_03', utility.helper, chart03.render);
    app.get('/report_03_search', utility.helper, chart03.render);
    app.get('/report_03_export', utility.helper, chart03.render);
    app.post('/report_03_search', utility.helper, chart03.search);
    app.post('/report_03_initialData', utility.helper, chart03.initialData);
    app.post('/report_03_DataDetail', utility.helper, chart03.DataDetail);
    app.post('/report_03_export', utility.helper, chart03.export);

    app.get('/report_04', utility.helper, chart04.render);
    app.post('/report_04_search', utility.helper, chart04.search);
    app.post('/report_04_submit', utility.helper, chart04.submit);
    app.post('/report_04_submitDrillDown4', utility.helper, chart04.submitDrillDown4);
    
    app.get('/report_05', utility.helper, chart05.render);
    app.post('/report_05_search', utility.helper, chart05.search);
    app.post('/report_05_initialData', utility.helper, chart05.initialData);
    app.post('/report_05_detailData', utility.helper, chart05.detailData);
    app.post('/report_05_export', utility.helper, chart05.export);
    app.post('/report_05_exportOnlyCom', utility.helper, chart05.exportOnlyComission);
    
    app.get('/report_05_01', utility.helper, chart05_01.render);
    app.post('/report_05_01_search', utility.helper, chart05_01.search);
    app.post('/report_05_01_renderlv0', utility.helper, chart05_01.RenderLv0);
    app.post('/report_05_01_renderlv1', utility.helper, chart05_01.RenderLv1); 
    app.post('/report_05_01_renderlv1Detail', utility.helper, chart05_01.RenderLv1WithProductDetail);
    app.post('/report_05_01_renderlv2', utility.helper, chart05_01.RenderLv2);
    app.post('/report_05_01_renderlv2Detail', utility.helper, chart05_01.RenderLv2WithProductDetail);

    app.get('/report_06', utility.helper, chart06.render);
    app.post('/report_06_search', utility.helper, chart06.search);
    app.post('/report_06_submit', utility.helper, chart06.submit);
    app.post('/report_06_dailyDetail01', utility.helper, chart06.dailyDetail01)
    app.post('/report_06_dailyDetail02', utility.helper, chart06.dailyDetail02)
    app.post('/report_06_submitDrillDown1', utility.helper, chart06.submitDrillDown1);

    app.get('/report_06_01', utility.helper, chart06.detail1);
    app.post('/report_06_submitDrillDown2', utility.helper, chart06.submitDrillDown2);
    app.post('/report_06_submitDrillDown3', utility.helper, chart06.submitDrillDown3);
    app.post('/report_06_export', utility.helper, chart06.export);

    app.get('/report_07', utility.helper, chart07.render);
    app.post('/report_07_search', utility.helper, chart07.search);
    app.post('/report_07_initialData', utility.helper, chart07.initialData);
    app.post('/report_07_detailData', utility.helper, chart07.detailData);
    app.post('/report_07_export', utility.helper, chart07.export);
};
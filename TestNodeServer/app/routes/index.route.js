﻿module.exports = function (app) {
    var index = require("../controllers/index.controller");
    app.get('/', index.render);
    app.get('/logout', index.logout);

    app.post('/', index.login);
};
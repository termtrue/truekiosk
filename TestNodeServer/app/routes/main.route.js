﻿module.exports = function (app) {
    var main = require("../controllers/main.controller");
    var dashboard = require("../controllers/dashboard.controller");
    var utility = require('../helper/utility.helper');
  
    app.get('/main', utility.helper, main.render);
    app.get('/empty', utility.helper, main.renderEmpty);

    app.get('/trans_main', utility.helper, dashboard.render);  
    app.post('/dashboard_initialData', utility.helper, dashboard.initialData);  
};
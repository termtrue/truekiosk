﻿
var moment = require('moment');

exports.helper = function (req, res, next) {

    if (req.session.user == null) {
        res.redirect('/');
        res.end();
    } else {

        //console.log(res.locals.user.agentUsername);
        next();
    }
};

exports.errorCallBack = function (err, data) {
    if (err) {
        console.error('There was an error', err);
        return;
    }
    console.log(data);
}

exports.GetYesterdayToDB = function () {

    var d = moment(new Date()).add(-1, 'days');
    var exDate = d.format().toString() + '';
    var exDateArray = exDate.split('T');
    return exDateArray[0];
}

exports.GetYesterdayToDisplay = function () {

    var d = moment(new Date()).add(-1, 'days');
    var exDate = d.format().toString() + '';
    var exDateString = exDate.split('T')[0].split('-');
    var result = exDateString[2] + '/' + exDateString[1] + '/' + (parseInt(exDateString[0]) + 543).toString();
    return result;
},

exports.YMDToDisplayDMY = function (date) {  
  
    var exDateString = date.split('/');
    var result = exDateString[2] + '/' + exDateString[1] + '/' + (parseInt(exDateString[0]) + 543).toString();
    return result;
    }
exports.YMDToDisplayDMYMisun = function (date) {

    var exDateString = date.split('/');
    var result = exDateString[0] + '/' + exDateString[1] + '/' + (parseInt(exDateString[2]) - 543).toString();
    return result;
}
exports.HashData = function (username) {
    var hash = require("bcrypt-nodejs");
    var hashedPassword = hash.hashSync(username);
    return hashedPassword;

}



﻿var models = require('../models');

module.exports = function (sequelize, seq) {

    var agentKioskRole = sequelize.define('kio_t_agent_kiosk_role', {
        ID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'ID'
        },
        roleID: {
            type: seq.INTEGER,
            field: 'roleID'
        },

        agentID: {
            type: seq.INTEGER,
            field: 'agentID'
        
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        }
    },
        {
            tableName: 'kio_t_agent_kiosk_role', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });  
   
    return agentKioskRole;
};

module.exports = function (sequelize, seq) {

    var kioAdvert = sequelize.define('kio_t_advert', {
     
        advertID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'advertID'
        },
        title: {
            type: seq.STRING,
            field: 'title'
        },
        url: {
            type: seq.STRING,
            field: 'url'
        },
        description: {
            type: seq.STRING,
            field: 'description'
        },
        pictureName: {
            type: seq.STRING,
            field: 'pictureName'
        },
        startDate: {
            type: seq.DATE,
            field: 'startDate'
        },
        endDate: {
            type: seq.DATE,
            field: 'endDate'
        },
        status: {
            type: seq.STRING,
            field: 'status'
        }, 
        categoryID: {
            type: seq.DATE,
            field: 'categoryID'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        updateDate: {
            type: seq.DATE,
            field: 'updateDate'
        },
        updateBy: {
            type: seq.STRING,
            field: 'updateBy'
        }
    },
        {
            tableName: 'kio_t_advert', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioAdvert;
};

﻿module.exports = function (sequelize, seq) {

    var logAuthen = sequelize.define('kio_t_logAuthen', {
        logID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'logID'
        },
        logUserID: {
            type: seq.STRING,
            field: 'logUserID'
        },
        logLoginStatus: {
            type: seq.STRING,
            field: 'logLoginStatus'
        },
        logErrorCode: {
            type: seq.STRING,
            field: 'logErrorCode'
        },
        logRemark: {
            type: seq.STRING,
            field: 'logRemark'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        logUserIP: {
            type: seq.STRING,
            field: 'logUserIP'
        },
    },
        {
            tableName: 'kio_t_logAuthen', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return logAuthen;
};

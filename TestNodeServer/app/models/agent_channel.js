﻿
module.exports = function (sequelize, seq) {

    var agentChannel = sequelize.define('agent_channel', {
        id: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'id'
        },
        agent_channel_name: {
            type: seq.STRING,
            field: 'agent_channel_name'
        },
        date_create: {
            type: seq.DATE,
            field: 'date_create'
        },
        user_create: {
            type: seq.STRING,
            field: 'user_create'
        },
        date_modified: {
            type: seq.DATE,
            field: 'date_modified'
        },
        user_modified: {
            type: seq.STRING,
            field: 'user_modified'
        }
    },
        {
            tableName: 'agent_channel', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return agentChannel;
};

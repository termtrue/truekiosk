module.exports = function (sequelize, seq) {

    var kioCategory = sequelize.define('kio_t_category', {
     
        categoryID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'categoryID'
        },
        categoryName: {
            type: seq.STRING,
            field: 'categoryName'
        },
        description: {
            type: seq.STRING,
            field: 'description'
        },
        status: {
            type: seq.STRING,
            field: 'status'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        updateDate: {
            type: seq.DATE,
            field: 'updateDate'
        },
        updateBy: {
            type: seq.STRING,
            field: 'updateBy'
        }
    },
        {
            tableName: 'kio_t_category', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioCategory;
};

﻿module.exports = function (sequelize, seq) {

    var kioskRole = sequelize.define('kio_t_kiosk_role', {
     
        kioskID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'kioskID'
        },
        roleID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'roleID'
        },       
        agentTypeCode: {
            type: seq.STRING,
            field: 'agentTypeCode'
        },
        agentChannelCode: {
            type: seq.STRING,
            field: 'agentChannelCode'
        },
        agentRegionCode: {
            type: seq.STRING,
            field: 'agentRegionCode'
        },
        kioskSerialNum: {
            type: seq.STRING,
            field: 'kioskSerialNum'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        }
    },
        {
            tableName: 'kio_t_kiosk_role', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioskRole;
};

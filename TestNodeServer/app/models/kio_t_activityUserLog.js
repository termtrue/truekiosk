﻿module.exports = function (sequelize, seq) {

    var activityUserLog = sequelize.define('kio_t_activityUserLog', {
        logID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'logID'
        },
        logType: {
            type: seq.STRING,
            field: 'logType'
        },
        logDate: {
            type: seq.DATE,
            field: 'logDate'
        },
        logValue: {
            type: seq.STRING,
            field: 'logValue'
        },
        kioskID: {
            type: seq.STRING,
            field: 'kioskID'
        },
        userID: {
            type: seq.STRING,
            field: 'userID'
        },
        bank1000: {
            type: seq.INTEGER,
            field: 'bank1000'
        },
        bank500: {
            type: seq.INTEGER,
            field: 'bank500'
        },
        bank100: {
            type: seq.INTEGER,
            field: 'bank100'
        },
        bank50: {
            type: seq.INTEGER,
            field: 'bank50'
        },
        bank20: {
            type: seq.INTEGER,
            field: 'bank20'
        },
        coin10: {
            type: seq.INTEGER,
            field: 'coin10'
        },
        coin5: {
            type: seq.INTEGER,
            field: 'coin5'
        },
        coin2: {
            type: seq.INTEGER,
            field: 'coin2'
        },
        coin1: {
            type: seq.INTEGER,
            field: 'coin1'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        logStatus: {
            type: seq.STRING,
            field: 'logStatus'
        }


    },
        {
            tableName: 'kio_t_activityUserLog', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return activityUserLog;
};

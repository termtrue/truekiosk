﻿module.exports = function (sequelize, seq) {

    //not complete
    var kioProfile = sequelize.define('kio_t_kioskProfile', {
        kioskProfileID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'kioskProfileID'
        },
        kioskSerialNumber: {
            type: seq.STRING,
            field: 'kioskSerialNumber'
        },
        kioskTemplateVersion: {
            type: seq.DATE,
            field: 'kioskTemplateVersion'
        },
        kioskPwdMaintenamce: {
            type: seq.STRING,
            field: 'kioskPwdMaintenamce'
        },
        kioskPwdAgent: {
            type: seq.DATE,
            field: 'kioskPwdAgent'
        },
        kioskAPN: {
            type: seq.STRING,
            field: 'kioskAPN'
        },

        kioskIPMonMasServer: {
            type: seq.DATE,
            field: 'kioskIPMonMasServer'
        },
        kioskIPMonSlaveServer: {
            type: seq.DATE,
            field: 'kioskIPMonSlaveServer'
        },
        kioskTmnServer: {
            type: seq.DATE,
            field: 'kioskTmnServer'
        },
        kioskFTPMasServer: {
            type: seq.DATE,
            field: 'kioskFTPMasServer'
        },
        kioskUserFTPMasServer: {
            type: seq.DATE,
            field: 'kioskUserFTPMasServer'
        },
        kioskPassFTPMasServer: {
            type: seq.DATE,
            field: 'kioskPassFTPMasServer'
        },
        kioskFTPSlaveServer: {
            type: seq.DATE,
            field: 'kioskFTPSlaveServer'
        },
        kioskUserFTPSlaveServer: {
            type: seq.DATE,
            field: 'kioskUserFTPSlaveServer'
        },
        kioskPassFTPSlaveServer: {
            type: seq.DATE,
            field: 'kioskPassFTPSlaveServer'
        },
        kioskConfigID: {
            type: seq.DATE,
            field: 'kioskConfigID'
        },
    },
        {
            tableName: 'kio_t_kioskProfile', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioProfile;
};
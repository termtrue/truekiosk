﻿module.exports = function (sequelize, seq) {

    var logTransaction = sequelize.define('kio_t_logTransaction', {
        logTransID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'logTransID'
        },
        logTransProgram: {
            type: seq.STRING,
            field: 'logTransProgram'
        },
        logTransAction: {
            type: seq.STRING,
            field: 'logTransAction'
        },
        logTransParam: {
            type: seq.STRING,
            field: 'logTransParam'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        updateDate: {
            type: seq.DATE,
            field: 'updateDate'
        },
        updateBy: {
            type: seq.STRING,
            field: 'updateBy'
        }
    },
        {
            tableName: 'kio_t_logTransaction', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return logTransaction;
};
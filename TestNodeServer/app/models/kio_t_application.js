module.exports = function (sequelize, seq) {

    var kioApplication = sequelize.define('kio_t_application', {
     
        appID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'appID'
        },
        appName: {
            type: seq.STRING,
            field: 'appName'
        },
        status: {
            type: seq.STRING,
            field: 'status'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        updateDate: {
            type: seq.DATE,
            field: 'updateDate'
        },
        updateBy: {
            type: seq.STRING,
            field: 'updateBy'
        }
    },
        {
            tableName: 'kio_t_application', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioApplication;
};

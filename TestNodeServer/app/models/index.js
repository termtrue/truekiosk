﻿var fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize'),
    db = {};

var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
}).forEach(function (file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
});

Object.keys(db).forEach(function (modelName) {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.kio_t_agentUser = require('../models/kio_t_agentUser.js')(sequelize, Sequelize);
db.kio_t_agent_kiosk_role = require('../models/kio_t_agent_kiosk_role.js')(sequelize, Sequelize);
db.kio_t_kiosk_role = require('../models/kio_t_kiosk_role.js')(sequelize, Sequelize);
db.kio_t_role = require('../models/kio_t_role.js')(sequelize, Sequelize);
db.kio_t_kioskProfile = require('../models/kio_t_kioskProfile.js')(sequelize, Sequelize);

db.kio_t_agentUser.belongsToMany(db.kio_t_kiosk_role, {
    through: 'kio_t_agent_kiosk_role',
    foreignKey: 'agentID',
    constraints: false });
db.kio_t_kiosk_role.belongsToMany(db.kio_t_agentUser, {
    through: 'kio_t_agent_kiosk_role',
    foreignKey: 'roleID',
    constraints: false});

db.kio_t_role.belongsToMany(db.kio_t_kioskProfile, {
    through: 'kio_t_kiosk_role',
    foreignKey: 'roleID',
    constraints: false
});
db.kio_t_kioskProfile.belongsToMany(db.kio_t_role, {
    through: 'kio_t_kiosk_role',
    foreignKey: 'kioskID',
    constraints: false
});

module.exports = db;
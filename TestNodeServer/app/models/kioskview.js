﻿
module.exports = function (sequelize, seq) {

    var kisokview = sequelize.define('kioskview', {
        agent_true_wallet_id: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'agent_true_wallet_id'
        },
        shop_name: {
            type: seq.STRING,
            field: 'shop_name'
        },
        agent_address: {
            type: seq.STRING,
            field: 'agent_address'
        },
        agentTypeCode: {
            type: seq.STRING,
            field: 'agentTypeCode'
        },
        agent_type_name: {
            type: seq.STRING,
            field: 'agent_type_name'
        },
        agentChannelCode: {
            type: seq.STRING,
            field: 'agentChannelCode'
        },
        agent_channel_name: {
            type: seq.STRING,
            field: 'agent_channel_name'
        },
        kioskSerialNum: {
            type: seq.STRING,
            field: 'kioskSerialNum'
        },
        host_name: {
            type: seq.STRING,
            field: 'host_name'
        },
        alias: {
            type: seq.STRING,
            field: 'alias'
        },
        hostStatus: {
            type: seq.STRING,
            field: 'hostStatus'
        }
    },
        {
            tableName: 'kioskview', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kisokview;
};

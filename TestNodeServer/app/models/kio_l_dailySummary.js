module.exports = function (sequelize, seq) {

    var dailySummary = sequelize.define('kio_l_dailySummary', {
        id: {
			type: seq.INTEGER,
			primaryKey: true,
            field: 'id'
		},
        serialNumber: {
			type: seq.STRING,
            field: 'serialNumber'
		},
        siteID: {
            type: seq.STRING,
            field: 'siteID'
		},
        agentUsername: {
			type: seq.STRING,
            field: 'agentUsername'
		},
        serviceName: {
			type: seq.STRING,
            field: 'serviceName'
		},
        transAmount: {
			type: seq.INTEGER,
            field: 'transAmount'
		},
        moneyAmount: {
            type: seq.INTEGER,
            field: 'moneyAmount'
		},
        summaryDate: {
            type: seq.DATE,
            field: 'summaryDate'
		},
        comission: {
            type: seq.INTEGER,
            field: 'comission'
		},
        vat_of_com: {
            type: seq.INTEGER,
            field: 'vat_of_com'
		},
        withholding_tax_of_com: {
            type: seq.INTEGER,
            field: 'withholding_tax_of_com'
		},
        net_com: {
            type: seq.INTEGER,
            field: 'net_com'
		}


	},
        {
            tableName: 'kio_l_dailySummary', // this will define the table's name
        	timestamps: false           // this will deactivate the timestamp columns
        });

    return dailySummary;
};

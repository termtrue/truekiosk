module.exports = function (sequelize, seq) {

    var kioConfig = sequelize.define('kio_t_config', {
     
        configID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'configID'
        },
        configCode: {
            type: seq.INTEGER,
            field: 'configCode'
        },
        configName: {
            type: seq.STRING,
            field: 'configName'
        },
        configValue: {
            type: seq.STRING,
            field: 'configValue'
        },
        type: {
            type: seq.STRING,
            field: 'type'
        },
        appID: {
            type: seq.INTEGER,
            field: 'appID'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        updateDate: {
            type: seq.DATE,
            field: 'updateDate'
        },
        updateBy: {
            type: seq.STRING,
            field: 'updateBy'
        }
    },
        {
            tableName: 'kio_t_config', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return kioConfig;
};

﻿module.exports = function (sequelize, seq) {

    var errorCode = sequelize.define('kio_t_errorCode', {
        errorID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'errorID'
        },
        errorCode: {
            type: seq.STRING,
            field: 'errorCode'
        },
        errorNameTH: {
            type: seq.STRING,
            field: 'errorNameTH'
        }, 
        errorNameEn: {
            type: seq.STRING,
            field: 'errorNameEn'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        }
    },
        {
            tableName: 'kio_t_errorCode', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return errorCode;
};

﻿module.exports = function (sequelize, seq) {

    var kioskUser = sequelize.define('kio_t_agentUser', {
        agentUserID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'agentUserID'
        },
        agentUsername: {
            type: seq.STRING,
            field: 'agentUsername'
        },
        agentPassword: {
            type: seq.STRING,
            field: 'agentPassword'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        },
        update_date: {
            type: seq.DATE,
            field: 'updateDate'
        },
        update_by: {
            type: seq.STRING,
            field: 'updateBy'
        },
        agentSessionID: {
            type: seq.STRING,
            field: 'agentSessionID'
        },
        agentLastLogin: {
            type: seq.DATE,
            field: 'agentLastLogin'
        },
        agentType: {
            type: seq.STRING,
            field: 'agentType'
        },
        agentTypeCode: {
            type: seq.STRING,
            field: 'agentTypeCode'
        },
        agentChannelCode: {
            type: seq.STRING,
            field: 'agentChannelCode'
        },
        agentRegionCode: {
            type: seq.STRING,
            field: 'agentRegionCode'
        },
       
        agentLoginFailCounted: {
            type: seq.INTEGER,
            field: 'agentLoginFailCounted'
        },
        IsLock: {
            type: seq.INTEGER,
            field: 'IsLock'
        },
        IsDisable: {
            type: seq.INTEGER,
            field: 'IsDisable'
        },
        agentLevel: {
            type: seq.INTEGER,
            field: 'agentLevel'
        },
        agentGroup: {
            type: seq.INTEGER,
            field: 'agentGroup'
        },
        agentFullName: {
            type: seq.STRING,
            field: 'agentFullName'
        }
    },
        {
            tableName: 'kio_t_agentUser', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });
   
    return kioskUser;
};

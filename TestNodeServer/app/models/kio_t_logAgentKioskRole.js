﻿module.exports = function (sequelize, seq) {

    var logAuthen = sequelize.define('kio_t_logAgentKioskRole', {
        logID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'logID'
        },
        agentUserCode: {
            type: seq.STRING,
            field: 'agentUserCode'
        },
        roleCode: {
            type: seq.STRING,
            field: 'roleCode'
        },
        errorCode: {
            type: seq.STRING,
            field: 'errorCode'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        }      
    },
        {
            tableName: 'kio_t_logAgentKioskRole', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return logAuthen;
};

﻿module.exports = function (sequelize, seq) {

    var role = sequelize.define('kio_t_role', {
        roleID: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'roleID'
        },
        roleName: {
            type: seq.STRING,
            field: 'roleName'
        },
        roleStatus: {
            type: seq.STRING,
            field: 'roleStatus'
        },       
        roleRemark: {
            type: seq.STRING,
            field: 'roleRemark'
        },
        createDate: {
            type: seq.DATE,
            field: 'createDate'
        },
        createBy: {
            type: seq.STRING,
            field: 'createBy'
        }
    },
        {
            tableName: 'kio_t_role', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return role;
};

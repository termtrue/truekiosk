﻿
module.exports = function (sequelize, seq) {

    var agentProfile = sequelize.define('agent_profile', {
        id: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'id'
        },
        agent_true_wallet_id: {
            type: seq.STRING,
            field: 'agent_true_wallet_id'
        },
        agent_name: {
            type: seq.STRING,
            field: 'agent_name'
        },
        agent_lastname: {
            type: seq.STRING,
            field: 'agent_lastname'
        },
        agent_thai_id: {
            type: seq.STRING,
            field: 'agent_thai_id'
        },
        agent_e_mail: {
            type: seq.STRING,
            field: 'agent_e_mail'
        },
        agent_address: {
            type: seq.STRING,
            field: 'agent_address'
        },
        agent_tel_1: {
            type: seq.STRING,
            field: 'agent_tel1'
        },
        agent_tel_2: {
            type: seq.STRING,
            field: 'agent_tel_2'
        },
        agent_tel_3: {
            type: seq.STRING,
            field: 'agent_tel_3'
        },
        agent_tel_4: {
            type: seq.STRING,
            field: 'agent_tel_4'
        },
        agent_tel_5: {
            type: seq.STRING,
            field: 'agent_tel_5'
        },
        agent_profile_status: {
            type: seq.STRING,
            field: 'agent_profile_status'
        },
        agent_user_id: {
            type: seq.INTEGER,
            field: 'agent_user_id'
        },
        date_create: {
            type: seq.DATE,
            field: 'date_create'
        },
        user_create: {
            type: seq.STRING,
            field: 'user_create'
        },
        date_modified: {
            type: seq.DATE,
            field: 'date_modified'
        },
        user_modified: {
            type: seq.STRING,
            field: 'user_modified'
        },
        agent_channel: {
            type: seq.INTEGER,
            field: 'agent_channel'
        },
        agent_type: {
            type: seq.INTEGER,
            field: 'agent_type'
        },
    },
        {
            tableName: 'agent_profile', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return agentProfile;
};

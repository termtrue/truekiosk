﻿
module.exports = function (sequelize, seq) {

    var agentType = sequelize.define('agent_type', {
        id: {
            type: seq.INTEGER,
            primaryKey: true,
            field: 'id'
        },
        agent_type_name: {
            type: seq.STRING,
            field: 'agent_type_name'
        },
        date_create: {
            type: seq.DATE,
            field: 'date_create'
        },
        user_create: {
            type: seq.STRING,
            field: 'user_create'
        },
        date_modified: {
            type: seq.DATE,
            field: 'date_modified'
        },
        user_modified: {
            type: seq.STRING,
            field: 'user_modified'
        }
    },
        {
            tableName: 'agent_type', // this will define the table's name
            timestamps: false           // this will deactivate the timestamp columns
        });

    return agentType;
};

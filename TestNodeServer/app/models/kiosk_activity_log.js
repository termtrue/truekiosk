
module.exports = function (sequelize, seq) {

    var activityLog = sequelize.define('kiosk_activity_log', {
        id: {
            type: seq.BIGINT,
            primaryKey: true,
            field: 'id'
        },
        time_kiosk_id_unique: {
            type: seq.STRING,
            field: 'time_kiosk_id_unique'
        },
        time_stamp: {
            type: seq.DATE,
            field: 'time_stamp'
        },
        serial_number: {
            type: seq.STRING,
            field: 'serial_number'
        },
        money_inbox: {
            type: seq.STRING,
            field: 'money_inbox'
        },
        mode: {
            type: seq.STRING,
            field: 'mode'
        },
        coin_10: {
            type: seq.INTEGER,
            field: 'coin_10'
        },
        coin_5: {
            type: seq.INTEGER,
            field: 'coin_5'
        },
        coin_2: {
            type: seq.INTEGER,
            field: 'coin_2'
        },
        coin_1: {
            type: seq.INTEGER,
            field: 'coin_1'
        },
        bank_1000: {
            type: seq.INTEGER,
            field: 'bank_1000'
        },
        bank_500: {
            type: seq.INTEGER,
            field: 'bank_500'
        },
        bank_100: {
            type: seq.INTEGER,
            field: 'bank_100'
        },
        bank_50: {
            type: seq.INTEGER,
            field: 'bank_50'
        },
        bank_20: {
            type: seq.INTEGER,
            field: 'bank_20'
        }
    },
        {
            tableName: 'kiosk_activity_log', // this will define the table's name
            timestamps: false          // this will deactivate the timestamp columns
        });

    return activityLog;
};

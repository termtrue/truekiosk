﻿
var transRoleRep = require('../data/transectionLogRepo');
var advertRepo = require('../data/advertRepo');
var configRepo = require('../data/configRepo');
var helper = require('../helper/utility.helper');

exports.render = function (req, res) {

    var type = ((Object.keys(req.query).length === 0) ? "1" : req.query.type);
    var kiosk1Count = 0;
    var kiosk1Trans = 0;
    var kiosk1Money = 0;
    var kiosk1Notification = 0;
    var kiosk2 = null;
    var kiosk3 = null;

    var advert_list = null;
    var advert_height = "";
    var advert_width = "";
    var pic_count = "";
    var exDate = helper.GetYesterdayToDB();
    var displayDate = helper.GetYesterdayToDisplay();
    var user = req.session.user;
    console.log('Page: Main');

    if (typeof user != 'undefined' && user) {
        console.log('----------TransLog: findLastDayHasData----------');
        transRoleRep.findLastDayHasData(user.agentTypeCode, user.agentGroup, user.agentRegionCode,user.agentUserID, user.agentLevel)
            .then(function (lstDate) {
                if (typeof lstDate != 'undefined' && lstDate) {

                    displayDate = lstDate[0].lstTime;    
                    console.log('----------TransLog: findDailySummaryDashboardByLevel_Header----------');
                    return transRoleRep.findDailySummaryDashboardByLevel_Header(user.agentTypeCode, user.agentLevel, user.agentGroup, lstDate[0].lstTime, user.agentUserID);
                }
            })
            .then(function (header) {               
                if (typeof header[0] != 'undefined') {
                    kiosk1Count = typeof header[0].kioskCount != 'undefined' ? header[0].kioskCount : 0;
                    kiosk1Trans = typeof header[0].transCount != 'undefined' ? header[0].transCount : 0;
                    kiosk1Money = typeof header[0].moneyCount != 'undefined' ? header[0].moneyCount : 0;
                    kiosk1Notification = typeof header[0].notificationCount != 'undefined' ? header[0].notificationCount : 0;
                }
                console.log('----------TransLog: findDailySummaryDashboardByLevel_Detail_L1----------');
                return transRoleRep.findDailySummaryDashboardByLevel_Detail_L1(user.agentTypeCode, user.agentLevel, user.agentGroup, displayDate, user.agentUserID);
            })
            .then(function (detail_L1) {
                kiosk2 = detail_L1;
                console.log('----------TransLog: findDailySummaryDashboardByLevel_Detail_L2----------');
                return transRoleRep.findDailySummaryDashboardByLevel_Detail_L2(user.agentTypeCode, user.agentLevel, user.agentGroup, displayDate, user.agentUserID);
            })
            .then(function (detail_L2) {
                kiosk3 = detail_L2;
                return configRepo.selectConfigValueByConfigName("Height");
            })
            .then(function (height) {
                if (typeof height[0] != 'undefined' && height[0]) {
                    advert_height = typeof height[0].configValue != 'undefined' ? height[0].configValue : 0;
                }
                console.log("height: " + advert_height);
                return configRepo.selectConfigValueByConfigName("Width");
            })
            .then(function (width) {
                if (typeof width[0] != 'undefined' && width[0]) {
                    advert_width = typeof width[0].configValue != 'undefined' ? width[0].configValue : 0;
                }
                console.log("width: " + advert_width);
                return configRepo.selectConfigValueByConfigName("PicCount");
            })
            .then(function (picCount) {
                if (typeof picCount[0] != 'undefined' && picCount[0]) {
                    pic_count = typeof picCount[0].configValue != 'undefined' ? picCount[0].configValue : 0;
                }
                console.log("picCount: " + pic_count);
                return advertRepo.selectRandomAdvert(pic_count);
            })
            .then(function (randomAdvertList) {
                if (typeof randomAdvertList[0] != 'undefined' && randomAdvertList[0] && randomAdvertList.length > 0) {
                    console.log("selectRandomAdvert have data");
                    return randomAdvertList;
                } else {
                    console.log("selectDefaultAdvert");
                    return advertRepo.selectDefaultAdvert();
                }
            })
            .then(function (advertList) {
                if (typeof advertList[0] != 'undefined' && advertList[0]) {
                    advert_list = advertList;
                    console.log("advertList: " + advert_list);
                }
                return transRoleRep.findDailySummaryMainChart(user.agentTypeCode, user.agentLevel, user.agentGroup, user.agentRegionCode, user.agentUserID, type);
            })
            .then(function (sumChartData) {
                console.log('------------');
                console.log(sumChartData);

                res.render('main', {
                    username: user.agentFullName,
                    userlv: user.agentLevel,
                    kiosk1Count: (typeof kiosk1Count === "undefined" ? 0 : kiosk1Count),
                    kiosk1Money: (typeof kiosk1Money === "undefined" ? 0 : kiosk1Money),
                    kiosk1Trans: (typeof kiosk1Trans === "undefined" ? 0 : kiosk1Trans),
                    kiosk1Notification: (typeof kiosk1Notification === "undefined" ? 0 : kiosk1Notification),
                    kiosk2: kiosk2,
                    kiosk3: kiosk3,
                    executeDate: helper.YMDToDisplayDMY(displayDate),
                    chartData: sumChartData,
                    chartType: type,
                    info: [],
                    advert_list: advert_list,
                    advert_height: advert_height,
                    advert_width: advert_width
                });
            });
    }
};
exports.renderEmpty = function (req, res) {
    res.render('empty');
};

function sort(object) {
    if (typeof object != "object" || object instanceof Array) // Not to sort the array
        return object;
    var keys = Object.keys(object);
    keys.sort();

    var newObject = {};
    for (var i = keys.length; i > 0; i--) {
        newObject[keys[i]] = sort(object[keys[i]])
        console.log(keys[i]);
    }
    return newObject;
}
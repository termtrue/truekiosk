exports.render = function (req, res) {
  	console.log('param : '+req.query.param);
  	var agentUserRep = require('../data/AuthenticationRepo');
  	var agentProfileRep = require('../data/agentUserRepo');
  	var arr = null;
  	var error = true;
  	var param =req.query.param;
  	var email = null;
  	var dateTime = null;
  	var userName = null;
  	//decrypt param
	var Cryptr = require('cryptr'),
	    cryptr = new Cryptr('myTotalySecretKey');
	var decryptedString = cryptr.decrypt(param);
	console.log('decrypt param:'+decryptedString);
  	// Split email and dateTime
  	if(typeof decryptedString != 'undefined' || null != decryptedString){
  		arr = decryptedString.split("|");
  		console.log('Split email and dateTime length:'+arr.length);
		if(arr.length == 3){
  			//Validate Email and dateTime
  			  	email = arr[0];
			  	dateTime = arr[1];
			  	userName = arr[2];
  			console.log('Validate Email and dateTime');
			console.log('Email:'+email);
			console.log('DateTime:'+dateTime);
			console.log('UserName:'+userName);
			//TODO: check date time
			var split = dateTime.split('/');
			var date = new Date(split[0],split[1],split[2],split[3],split[4],split[5]);
			var dateTimestamp = date.getTime();
			var d = new Date();
			var current = new Date(d.getYear(),d.getMonth(),d.getDate(),d.getHours(),d.getMinutes(),d.getSeconds());
			var currentTimestamp = current.getTime();
			console.log('currentTimestamp:'+ currentTimestamp);
			console.log('dateTimestamp:'+ dateTimestamp);
    	   if (dateTimestamp > currentTimestamp) {
					agentUserRep.findByUsername(userName)
					.then(function(data){
						console.log('typeof' + typeof data);
						console.log('agentUserRep>>findByUsernameAndPassword:'+data);
						if (typeof data != 'undefined' && data != null) {
							return agentProfileRep.findByagentUserID(data.agentUserID);
						}
					})
					.then(function(data){
						console.log('agentProfileRep>>findByagentUserID:'+data);
			 			if (typeof data != 'undefined' && data != null) {
			 				console.log('email:'+data.agent_e_mail);
							if(data.agent_e_mail == email){
								error = false;
							}
						}
					  //Redirect Page
					  	console.log('error:'+error);
						if(error){
						res.render('changePassword',{ changePasswordStatus: false, errorObj: 'Data is Invalid.', username : null});
						}else{
						 res.render('changePassword',{ changePasswordStatus: true, errorObj: null, username : userName});
						}
					})
    	    }else{
    	    	res.render('changePassword',{ changePasswordStatus: false, errorObj: 'Time out to changePassword.', username : null});
    	    }
		}else{
			res.render('changePassword',{ changePasswordStatus: false, errorObj: 'Data is Invalid.', username : null});
		}
	}else{
		res.render('changePassword',{ changePasswordStatus: false, errorObj: 'Data is Invalid.', username : null});
	}

};
exports.submit = function (req, res) {
	var agentUserRep = require('../data/AuthenticationRepo');
	var pwd = null;
	var username = null;

	//update password
  	username = req.body.username;
    pwd = req.body.password;
  	console.log('Username :'+ username);
  	console.log('Password :'+ pwd);
  
	agentUserRep.updatePwdByUsernameAndPwd(username,pwd)
	console.log('update new password');

  	//redirect Login
  	res.render('index', { loginStatusObj: null });
};
exports.back = function (req, res) {
    res.render('index', { loginStatusObj: null });
};  
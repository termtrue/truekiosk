
exports.render = function (req, res) {
    res.render('forgotPassword', {  forgotPassStatus: true,errorObj: null});
};
exports.sendEmail = function (req, res) {
	var input_username = req.body.user;
	var password = null;
    var agentUserRep = require('../data/AuthenticationRepo');
    var agentProfileRep = require('../data/agentUserRepo');
    var chkError = false;
    var errorMsg = null;
	//validate user
  	agentUserRep.findByUsername(input_username)
    .then(function (data) {
    		console.log('findByUsername : '+data);
            if (typeof data == 'undefined'|| data == null || typeof data.agentUsername == 'undefined' || data.agentUsername == null) {
            	res.render('forgotPassword',{ forgotPassStatus: false, errorObj: 'User is invalid.'});
            	chkError = true;
            	errorMsg = 'User is invalid.';
            	return null;
            }else{
				return data;
            }
    })
    .then(function (data) {
		if (typeof data != 'undefined' && data != null){
			
			//find Email
			console.log('Find Email by userId : '+data.agentUserID);
			return agentProfileRep.findByagentUserID(data.agentUserID)
		}
	 })
	 .then(function (data) {
		if (typeof data != 'undefined' && data != null){ 
			console.log('Find Email : ' + data.agent_e_mail);

			//encrypt param
			var Cryptr = require('cryptr'),
			    cryptr = new Cryptr('myTotalySecretKey');
			var d = new Date();
			d.setMinutes(d.getMinutes() + 15);
			var dateTime =d.getYear()+"/"+d.getMonth()+"/"+d.getDate()+"/"+d.getHours()+"/"+d.getMinutes()+
"/"+d.getSeconds();

			var param = data.agent_e_mail + '|' +  dateTime+ '|' + input_username
			var encryptedParam = cryptr.encrypt(param);
			console.log('param :' + param);

			//prepare email content
			const nodemailer = require('nodemailer');
			var smtpTransport = nodemailer.createTransport({
		    service: 'gmail',
		    auth: {
		        user: 'sasithornsupamarkpukdee@gmail.com',
		        pass: '48711062'
		    	}
			});
			var mailOptions={
				   from: '"Sasithorn Supamark" <sasithornsupamarkpukdee@gmail.com>',
				   to : data.agent_e_mail,
				   subject : 'Change your password',
				   html : '<b>Hi,'+input_username+'</b><br>'+
				   		  'There was a request to change your password.<br>'+
				   		  'Please click the link below to change your password:<br>'+
				   		  '<a href="http://localhost:3000/changePassword_init?param='+encryptedParam+'" >change your password</a>'

			}

			//send mail
			smtpTransport.sendMail(mailOptions, function(error, response){
				chkError = error;
				errorMsg = 'Can not send you a link';
			});
		}	
    })
	.then(function () {
			if(chkError){
				res.render('forgotPassword',{ forgotPassStatus: false, errorObj: errorMsg});
			}else{
			 	res.render('forgotPassword',{ forgotPassStatus: false, errorObj: null});
			}
	})
	 
};
exports.back = function (req, res) {
    res.redirect('/');
};
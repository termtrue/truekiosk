﻿var activityUserLog = require('../data/activityUserLogRepo.data');
var kiosk = require('../data/kioskProfileRepo');

exports.render = function (req, res) {
    console.log('---------- Page: Kiosk Detail ----------');

    var user = req.session.user;
    kiosk.findDataByTypeAndChannel(user.agentTypeCode, user.agentChannelCode, user.agentLevel, user.agentUsername)
        .then(function (id) {
            res.render('kiosk/', { username: user.agentUsername, info: id });
        });
};
exports.submitRecheckData = function (req, res) {
    console.log('Page: Submit Recheck');

    if (typeof req.body != 'undefined' && req.body) {
        var userID = req.session.user.agentUsername;
        var createBy = req.session.user.agentUsername;
        var logDate = req.body.dtpickerFrom;
        var logValue = req.body.cTotalMoney;
        var cBank1000 = req.body.cBank1000;
        var cBank500 = req.body.cBank500;
        var cBank100 = req.body.cBank100;
        var cBank50 = req.body.cBank50;
        var cBank20 = req.body.cBank20;
        var cCoin10 = req.body.cCoin10;
        var cCoin5 = req.body.cCoin5;
        var cCoin2 = req.body.cCoin2;
        var cCoin1 = req.body.cCoin1;
        kiosk.findOneDataByRole(req.session.user.agentUserID).then(function (data) {
            console.log(req.session.user.agentUserID);
            console.log(data);


            activityUserLog.updateToDisableLogActivityUser(data[0].kioskSerialNumber, logDate)
                .then(function () {

                    activityUserLog.createActivityUserLog('0', logDate, logValue, data[0].kioskSerialNumber, userID, createBy,
                        cBank1000, cBank500, cBank100, cBank50, cBank20, cCoin10, cCoin5, cCoin2, cCoin1)
                        .then(function (id) {
                            res.render('dailyRecheck/', { kioskSN: data[0].kioskSerialNumber, username: req.session.user.agentUsername, isFinish: true });
                        })
                });

        });
    } else {

    }
};

exports.dailybill = function (req, res) {
    console.log('----------Page: Daily Bill----------');
    var user = req.session.user;
    kiosk.findOneDataByRole(user.agentUserID).then(function (data) {

        res.render('dailyRecheck/', {
            kioskSN: data[0].kioskSerialNumber,
            username: req.session.user.agentUsername,
            isFinish: false
        });

    });
};
exports.kioskMain = function (req, res) {
    console.log('----------Page: Kiosk Main----------');
    res.setHeader('Content-Type', 'application/json');
    var user = req.session.user;

    kiosk.findDataByTypeAndChannel(user.agentTypeCode, user.agentChannelCode, user.agentLevel, user.agentUsername)
        .then(function (kioskData) {
            var result = JSON.stringify(kioskData);
            res.send(kioskData);
        });
};


﻿var agentUserRep = require('../data/AuthenticationRepo');
var log = require('../data/logAuthenRepo.data');
var md5 = require('md5');
var async = require('async');
var helper = require('../helper/utility.helper');
var system = 'system';

exports.render = function (req, res) {
    var cookie = req.cookies.rmKiosk;

    console.log(cookie);
    if (typeof cookie != 'undefined' && cookie) {

        var cookieSplited = cookie.split("|");
        agentUserRep.findByUsernameAndPassword(cookieSplited[0], cookieSplited[1])
            .then(function (user) {
                if (typeof user != 'undefined' && user) {

                    req.session.user = user;
                    log.createLog(req.session.user.agentUsername, 'T', '', 'SUCCESS', system, '');

                    switch (req.session.user.agentType) {
                        case "O":
                            res.redirect("/operation");
                            break;
                        case "A":
                            res.redirect("/main");
                            break;
                        default:
                            res.redirect("/kiosk/dailybill");
                            break;
                    }
                } else {
                    log.createLog(input_username, 'F', '', 'FAIL', system, '');
                    res.render('index', { loginStatusObj: false, errorObj: "er01", errorMsg: "error" });
                }
            });
    } else {
        res.render('index', { loginStatusObj: null });
    }
};
exports.login = function (req, res) {
    "use strict";  

    var input_username = req.body.user;
    var input_password = req.body.password;   

    agentUserRep.findByUsername(input_username)
        .then(function (userByName) {
            console.log('pass:');
            if (typeof userByName != 'undefined' && userByName) {
                console.log('pass:' + md5(input_password));

                agentUserRep.findByUsernameAndPassword(input_username, md5(input_password))
                    .then(function (user) {

                        if (typeof user != 'undefined' && user) {

                            req.session.user = user;
                            console.log(user);

                            if (req.body.chkRemember) res.cookie('rmKiosk', input_username + "|" + md5(input_password), { maxAge: 90000 });

                            agentUserRep.updateLockCounterToZero(input_username);
                            log.createLog(req.session.user.agentUsername, 'T', '', 'SUCCESS', system, '');

                            switch (req.session.user.agentType) {
                                case "O":
                                    res.redirect("/operation");
                                    break;
                                case "A":
                                    res.redirect("/main");
                                    break;
                                default:
                                    res.redirect("/kiosk/dailybill");
                                    break;
                            }

                        } else {
                            if (userByName.agentLoginFailCounted < 3) {
                                log.createLog(input_username, 'F', '', 'FAIL', system, '');
                                agentUserRep.updateLockCounterByUsername(input_username);
                                res.render('index', { loginStatusObj: false, errorObj: "er01", errorMsg: "error" });
                            } else {
                                log.createLog(input_username, 'F', '', 'FAIL', system, '');
                                res.render('forgotPassword', { forgotPassStatus: true, errorObj: null });
                            }
                        }
                    });
            } else {
                log.createLog(input_username, 'F', '', 'FAIL', system, '');
                res.render('index', { loginStatusObj: false, errorObj: "er01", errorMsg: "error" });
            }
        });
};
exports.logout = function (req, res) {

    var cookie = req.cookies.rmKiosk;

    if (typeof cookie != 'undefined' && cookie) {
        res.clearCookie("rmKiosk");
    }
    req.session.user = 'undefined';
    res.redirect("/");


};






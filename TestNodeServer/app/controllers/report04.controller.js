﻿var helper = require('../helper/utility.helper');

exports.render = function (req, res) {
    var date = req.query.date;
    var type = req.query.type;
    var user = req.session.user;
    res.render('report_04', { username: user.agentFullName,type: type, from: null, to: null, date: date });
}
exports.search = function (req, res) {
    var type = '4';
    var dtpickerFrom = (req.body.dtpickerFrom);
    var dtpickerTo = (req.body.dtpickerTo);
    var user = req.session.user;
    console.log("dtpickerFrom>>>>>>>>>>>>>>>>>>" + dtpickerFrom);
    console.log("dtpickerTo>>>>>>>>>>>>>>>>>>" + dtpickerTo);
    res.render('report_04', { username: user.agentFullName,type: type, from: dtpickerFrom, to: dtpickerTo, date: null });
}
exports.submit = function (req, res) {
    var activityRepository = require('../data/activityLogRepo');
    var tranRepository = require('../data/transectionLogRepo');
    var type = '4';

    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var user = req.session.user;
    res.setHeader('Content-Type', 'application/json');


    tranRepository.findDetailErrorReportMain(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });


}

exports.submitDrillDown4 = function (req, res) {
    var tranRepository = require('../data/transectionLogRepo');

    var user = req.session.user;
    var type = '4';
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var kioskID = req.body.kioskID;


    res.setHeader('Content-Type', 'application/json');
    tranRepository.findDetailErrorReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.export = function (req, res) {
    var summaryRepository = require('../data/activityLogRepo');
    var tranRepository = require('../data/transectionLogRepo');
    res.setHeader('Content-Type', 'application/json');
    var user = req.session.user;
    var dataType = req.body.sel1;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);

    console.log(dtpickerFrom);
    console.log(dtpickerTo);

    if (dataType == '1') {
        summaryRepository.findDetailDailyReportByRoleIDAndRange(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8', 'col9', 'col10', 'col11', 'col12', 'col13'];
                var fieldNames = ['CreateDate', 'SerialNumber', 'Coin 10', 'Coin 5', 'Coin 2', 'Coin 1', 'Bank 1000', 'Bank 500', 'Bank 100', 'Bank 50', 'Bank 20', 'Auto/Manual', 'EOD', 'Total'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,', fieldNames: fieldNames });

                console.log(tsv);
                res.attachment('daliyReportPerKiosk.csv');
                res.status(200).send(tsv);
            });

    } else if (dataType == '1') {
        tranRepository.findDetailDailyProductReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'product_name', 'service_amount', 'fee', 'customer_credit', 'collect_money'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,' });

                console.log(tsv);
                res.attachment('daliyProductReport.csv');
                res.status(200).send(tsv);
            });
    } else {
        tranRepository.findDetailTransectionReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'timestamp', 's_n_kiosk', 'product_name', 'collect_money', 'result_status', 'coin_10', 'coin_5', 'coin_2', 'coin_1', 'bank_1000', 'bank_500', 'bank_100', 'bank_50', 'bank_20'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,' });

                console.log(tsv);
                res.attachment('daliyTransectionReport.csv');
                res.status(200).send(tsv);
            });
    }

}
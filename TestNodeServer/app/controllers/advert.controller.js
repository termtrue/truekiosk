var fs = require('fs');
var advertRepo = require('../data/advertRepo');
var categoryRepo = require('../data/categoryRepo');
var configRepo = require('../data/configRepo');

exports.render = function (req, res) {
    var user = req.session.user;
    res.render('advert/main_advert', { username: user.agentUsername});
};
exports.searchDataTable = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    advertRepo.findAllData()
        .then(function (data) {
            res.send(data);
        });
};
exports.add = function (req, res) {
    var p_title = req.query.title;
    var p_url = req.query.url;
    var p_description = req.query.description;
    var p_picture_name = req.query.picture_name;
    var p_start_date = req.query.start_date;
    var p_end_date = req.query.end_date;
    var p_status = req.query.status;
    var p_category_name = req.query.category_name;
    var p_action = req.query.action;
    var p_advert_id = req.query.advert_id
    var p_category_id = "";

    //Get Category
    var data = null;

    categoryRepo.findAll()
        .then(function (result) {
            console.log(result);
            data = result;
            for (i = 0; i < data.length; i++) {
                if (p_category_name == data[i].categoryName) {
                    p_category_id = data[i].categoryID;
                }
            }
            res.render('advert/detail_advert', { categoryAll: data, title: p_title, url: p_url, description: p_description, picture_name: p_picture_name, start_date: p_start_date, end_date: p_end_date, status: p_status, category_id: p_category_id, advert_id: p_advert_id, action: p_action });
        })

};
exports.addSave = function (req, res) {
    //Get Data
    title = req.body.title;
    url = req.body.url;
    description = req.body.description;
    category_id = req.body.category;
    start_date = req.body.dtpickerFrom;
    end_date = req.body.dtpickerTo;
    status = req.body.status;
    picture_name = 'logo.png';
    //TODO: Upload file

    //TODO: Change file name

    //TODO: Save file

    //TODO: Delete old file

    //parse the incoming request containing the form data
    // picture_name=req.body.files.picture_name.name;
    console.log(">>>>>>>>>>>>>>>>>" + req.files);

    fs.readFile(req.files.picture_name.path, function (err, data) {
        var newPath = __dirname + "../../img";
        fs.writeFile(newPath, data, function (err) {
            res.redirect("back");
        });
    });

    console.log('title:' + title + '|url:' + url + '|description:' + description);
    console.log('|category:' + category_id + '|start_date:' + start_date + '|end_date:' + end_date);
    console.log('|status:' + status + '|userID' + req.session.user.agentUserID);

    //Save

    advertRepo.insertAdvertStringSql(title, url, description, picture_name, start_date, end_date, status, category_id, req.session.user.agentUserID)

        .then(function (result) {
            res.render('advert/main_advert');
        });

};
exports.delete = function (req, res) {
    var p_data = req.query.data;
    value_arr = p_data.split("|");
    for (i = 0; i < value_arr.length; i++) {

        advertRepo.deleteAdvertStringSql(value_arr[i])
    }
    res.render('advert/main_advert');
};
exports.editSave = function (req, res) {
    //Get Data
    title = req.body.title;
    url = req.body.url;
    description = req.body.description;
    category_id = req.body.category;
    start_date = req.body.dtpickerFrom;
    end_date = req.body.dtpickerTo;
    status = req.body.status;
    advert = req.body.advert_id;
    picture_name = "xxx";

    //TODO: Upload File

    //TODO: Change file name

    //TODO: Save file

    //TODO: Delete old file

    console.log('title:' + title + '|url:' + url + '|description:' + description);
    console.log('|category:' + category_id + '|start_date:' + start_date + '|end_date:' + end_date);
    console.log('|status:' + status + '|userID' + req.session.user.agentUserID + '|advert_id:' + advert);

    //Save
    var advertRepo = require('../data/advertRepo');
    advertRepo.updateAdvertStringSql(title, url, description, picture_name, start_date, end_date, status, category_id, req.session.user.agentUserID, advert)
        // .then(function(result){
        //     return  advertRepo.updateImageAdvert(advert,'/1.jpg');
        // })
        .then(function (result) {
            res.render('advert/main_advert');
        });
};
exports.back = function (req, res) {
    res.render('advert/main_advert');
};

//----------------------------------
exports.config = function (req, res) {
    //Get all data from config
    var user = req.session.user;
    configRepo.findAllData()
        .then(function (result) {
            res.render('advert/main_config_advert', { info: result ,username: user.agentUsername, });
        })
};
exports.configEditSave = function (req, res) {
    var p_input_value = req.query.input_value;
    var p_input_id = req.query.input_id;
    value_arr = p_input_value.split("|");
    id_arr = p_input_id.split("|");
    for (i = 0; i < value_arr.length; i++) {

        configRepo.updateConfigStringSql(value_arr[i], id_arr[i], req.session.user.agentUserID)
    }
    var logTransactionRepo = require('../data/logTransactionRepo');
    logTransactionRepo.createLog("advert", "U", "input value: " + p_input_value + "|input id: " + p_input_id, req.session.user.agentUserID)

    res.render('advert/main_advert');
};




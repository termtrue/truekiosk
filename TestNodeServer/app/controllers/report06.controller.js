var helper = require('../helper/utility.helper');
var activityRepository = require('../data/activityLogRepo');
var tranRepository = require('../data/transectionLogRepo');
var summaryRepository = require('../data/activityLogRepo');

exports.render = function (req, res) {
    var date = req.query.date;
    var type = req.query.type;
    var dtpickerFrom = (req.body.dtpickerFrom);
    var dtpickerTo = (req.body.dtpickerTo);
    var txtKioskID = req.body.txtKioskID;
    res.render('report_06', { username: user.agentFullName,type: type, from: null, to: null, date: date ,txtKioskID: ''});
}
exports.search = function (req, res) {
    var type = req.body.sel1;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var KioskID = req.body.txtKioskID;
    console.log("dtpickerFrom>>>>>>>>>>>>>>>>>>" + dtpickerFrom);   
    res.render('report_06', { username: user.agentFullName,type: type, from: dtpickerFrom, to: dtpickerTo, date: null, txtKioskID: KioskID});
}
exports.submit = function (req, res) {
    
    console.log(req.body);
    var type = req.body.sel;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var txtKioskID = req.body.txtKioskID;
    var user = req.session.user;
    res.setHeader('Content-Type', 'application/json');

    if (type == '1') {
        activityRepository.findDailyReportByRoleIDAndRange(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (data) {
                var result = JSON.stringify(data);
                res.send(data);
            });
    }
    else if (type == '3') {
        tranRepository.findDetailTransectionReportMain(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, txtKioskID)
            .then(function (data) {
                var result = JSON.stringify(data);
                res.send(data);
            });

    }
    else if (type == '4') {
        tranRepository.findDetailErrorReportMain(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (data) {
                var result = JSON.stringify(data);
                res.send(data);
            });
    }
    else {
        tranRepository.findDailyProductReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, txtKioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
    }
}

exports.submitDrillDown1 = function (req, res) {   
    console.log(req.body);
    var user = req.session.user;
    var type = req.body.sel;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var kioskID = req.body.kioskID;
    console.log(kioskID);

    res.setHeader('Content-Type', 'application/json');
    summaryRepository.findDetailDailyReportByRoleIDAndRangeAndKioskID(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.submitDrillDown2 = function (req, res) {   
    console.log(req.body);
    var user = req.session.user;
    var type = req.body.sel;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var productName = req.body.productName;
    var kioskID = req.body.kioskID;
    console.log(kioskID);

    res.setHeader('Content-Type', 'application/json');
    tranRepository.findDetailDailyProductReport01(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, productName, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.submitDrillDown3 = function (req, res) {   
    console.log(req.body);
    var user = req.session.user;
    var type = req.body.sel;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var kioskID = req.body.kioskID;
    console.log(kioskID);

    res.setHeader('Content-Type', 'application/json');
    tranRepository.findDetailTransectionReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.submitDrillDown4 = function (req, res) {  
    console.log(req.body);
    var user = req.session.user;
    var type = req.body.sel;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var kioskID = req.body.kioskID;
    console.log(kioskID);

    res.setHeader('Content-Type', 'application/json');
    tranRepository.findDetailErrorReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}

exports.export = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var user = req.session.user;
    var dataType = req.body.sel1;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var productName = '';
    var kioskID = req.body.txtKioskID;
    console.log(dtpickerFrom);
    console.log(dtpickerTo);

    if (dataType == '1') {
        summaryRepository.findDetailDailyReportByRoleIDAndRange(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'col1', 'col2', 'col12', 'col13'];
                var fieldNames = ['CreateDate', 'SerialNumber', 'Auto/Manual', 'EOD', 'Total'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,', fieldNames: fieldNames });

                console.log(tsv);
                res.attachment('daliyReportPerKiosk.csv');
                res.status(200).send(tsv);
            });

    } else if (dataType == '2') {
        tranRepository.findDailyProductReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'product_name', 'service_amount', 'fee', 'customer_credit', 'collect_money'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,' });

                console.log(tsv);
                res.attachment('daliyProductReport.csv');
                res.status(200).send(tsv);
            });
    } else {
        tranRepository.findDetailTransectionReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
            .then(function (info) {
                var json2csv = require('json2csv');
                var fields = ['day', 'timestamp', 's_n_kiosk', 'product_name', 'collect_money', 'result_status', 'coin_10', 'coin_5', 'coin_2', 'coin_1', 'bank_1000', 'bank_500', 'bank_100', 'bank_50', 'bank_20'];
                var tsv = json2csv({ data: info, fields: fields, del: '\,' });

                console.log(tsv);
                res.attachment('daliyTransectionReport.csv');
                res.status(200).send(tsv);
            });
    }

}
exports.detail1 = function (req, res) {   
    var dtpickerFrom = req.query.dtpickerFrom;
    var dtpickerTo = req.query.dtpickerTo;
    var kioskID = req.query.kioskId;
    var searchKioskID = req.query.searchKioskID;
    console.log("kioskID>>" + kioskID);
    console.log("dtpickerFrom>>" + dtpickerFrom);
    console.log("dtpickerTo>>" + dtpickerTo);
    res.render('report_06_detail_daily', { username: req.session.user.agentUsername,kioskID: kioskID, dtpickerTo: dtpickerTo, dtpickerFrom: dtpickerFrom, searchKioskID: searchKioskID });
}
exports.dailyDetail01 = function (req, res) {   
    console.log(req.body);
    var user = req.session.user;
    var kioskID = req.body.kioskID;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    console.log("kioskID>>" + kioskID);
    console.log("dtpickerFrom>>" + dtpickerFrom);
    console.log("dtpickerTo>>" + dtpickerTo);
    res.setHeader('Content-Type', 'application/json');
    summaryRepository.findDetailDailyReportByRoleIDAndRangeAndKioskID(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.dailyDetail02 = function (req, res) {
    console.log(req.body);
    var user = req.session.user;
    var kioskID = req.body.kioskID;
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    console.log("kioskID>>" + kioskID);
    console.log("dtpickerFrom>>" + dtpickerFrom);
    console.log("dtpickerTo>>" + dtpickerTo);
    res.setHeader('Content-Type', 'application/json');
    tranRepository.findDetailTransectionReport(user.agentTypeCode, user.agentChannelCode, user.agentRegionCode, dtpickerFrom, dtpickerTo, kioskID)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
}
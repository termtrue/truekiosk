var xl = require('excel4node');
var dailySumRepo = require('../data/dailySummaryRepo');
var serviceRepo = require('../data/serviceProfileRepo');
var summaryRepository = require('../data/activityLogRepo');
var tranRepository = require('../data/transectionLogRepo');
var AgentTypeRepo = require('../data/v_AgentTypeRepo');

exports.render = function (req, res) {
    var month = new Array();
    month[0] = "01";
    month[1] = "02";
    month[2] = "03";
    month[3] = "04";
    month[4] = "05";
    month[5] = "06";
    month[6] = "07";
    month[7] = "08";
    month[8] = "09";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";

    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;

    var agentTypeID = (req.body.typeID == '' ? user.agentTypeCode : req.body.typeID);
    var agentChannelID = (req.body.channelID == '' ? user.agentGroup : req.body.channelID);
    var agentLevel = (req.body.level == '' ? user.agentLevel : req.body.level);

    var makeDate = new Date();
    makeDate.setYear(makeDate.getFullYear() + 543);
    var CurDate = new Date(makeDate.setMonth(makeDate.getMonth()));
    var PreviousDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));

    var To = CurDate.getDate() + '/' + month[CurDate.getMonth()] + '/' + CurDate.getFullYear();
    var From = PreviousDate.getDate() + '/' + month[PreviousDate.getMonth()] + '/' + PreviousDate.getFullYear();

    //dailySumRepo.SelectSummaryByUserWithOption(agentTypeID, agentLevel, agentChannelID, user.agentUserID, From, To, agentLevel,
    //    function (err, resultSummary) {
    console.log(req.body);
    AgentTypeRepo.GetAllType(
        function (err, agentType) {

            //return serviceRepo.findServiceProfile(agentLevel, agentTypeID)
            //    .then(function (serviceData) {


            res.render('report_05', {
                agentType: agentType,
                selectedAgentType: '',
                selectedChannelType: '',
                selectedLevel: '',
                username: user.agentFullName,
                from: dtpicker,
                dateto: To,
                datefrom: From,
                company: agentTypeID,              
                service: [],
                numberTrans: 0,
                moneyAmount: 0,
                comission: 0,
                vat_of_com: 0,
                withholding_tax_of_com: 0,
                net_com: 0,
                userLv: user.agentLevel,
         
                issearch: 0
            });
        });
    //        });
    //});
}
exports.search = function (req, res) {

    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var agentTypeID = (req.body.ddlType == '' || typeof req.body.ddlType === "undefined" ? user.agentTypeCode : req.body.ddlType);
    var agentChannelID = (req.body.ddlChannel == '' || typeof req.body.ddlChannel  === "undefined" ? user.agentGroup : req.body.ddlChannel);
    var agentLevel = (req.body.ddlLevel == '' || typeof req.body.ddlLevel  === "undefined" ? user.agentLevel : req.body.ddlLevel);

    console.log(req.body);
    console.log(user.agentLevel);
    dailySumRepo.SelectSummaryByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
        function (err, resultSummary) {

            var serviceTable = '';

            AgentTypeRepo.GetAllType(
                function (err, agentType) {

                    return serviceRepo.findServiceProfile(agentLevel, agentTypeID)
                        .then(function (serviceData) {
                            
                            res.render('report_05', {
                                agentType: agentType,
                                selectedAgentType: agentTypeID,
                                selectedChannelType: agentChannelID,
                                selectedLevel: agentLevel,
                                username: user.agentFullName,
                                from: dtpicker,
                                dateto: dtpickerTo,
                                datefrom: dtpickerFrom,
                                company: agentTypeID,
                               
                                service: serviceData,
                                numberTrans: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].transAmount),
                                moneyAmount: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].moneyAmount),
                                comission: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].comission),
                                vat_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].vat_of_com),
                                withholding_tax_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].withholding_tax_of_com),
                                net_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].net_com),
                                userLv: user.agentLevel,
                                
                                issearch: 1
                            });
                        });
                });
        });
}
exports.initialData = function (req, res) {


    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpicker = req.body.dtpickerToDate;
    var user = req.session.user;
    var agentTypeID = (req.body.ddlType == '' || typeof req.body.ddlType === "undefined" ? user.agentTypeCode : req.body.ddlType);
    var agentChannelID = (req.body.ddlChannel == '' || typeof req.body.ddlChannel === "undefined" ? user.agentGroup : req.body.ddlChannel);
    var agentLevel = (req.body.ddlLevel == '' || typeof req.body.ddlLevel === "undefined" ? user.agentLevel : req.body.ddlLevel);


    res.setHeader('Content-Type', 'application/json');

    dailySumRepo.SelectByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
        function (err, data) {
            var result = JSON.stringify(data);
            res.send(result);
        });

}

exports.detailData = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpicker = req.body.dtpickerToDate;
    var kioskID = req.body.kioskID;
    var user = req.session.user;
    var agentTypeID = (req.body.ddlType == '' || typeof req.body.ddlType === "undefined" ? user.agentTypeCode : req.body.ddlType);
    var agentChannelID = (req.body.ddlChannel == '' || typeof req.body.ddlChannel === "undefined" ? user.agentGroup : req.body.ddlChannel);
    var agentLevel = (req.body.ddlLevel == '' || typeof req.body.ddlLevel === "undefined" ? user.agentLevel : req.body.ddlLevel);


    dailySumRepo.SelectByUserAndSerialNumWithOption(kioskID, agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
        function (err, data) {



            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.export = function (req, res) {
    var month = new Array();
    month[0] = "01";
    month[1] = "02";
    month[2] = "03";
    month[3] = "04";
    month[4] = "05";
    month[5] = "06";
    month[6] = "07";
    month[7] = "08";
    month[8] = "09";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";

    res.setHeader('Content-Type', 'application/json;  charset=tis-620;');
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpicker = req.body.dtpickerToDate;
    var user = req.session.user;
    var agentTypeID = (req.body.ddlType == '' || typeof req.body.ddlType === "undefined" ? user.agentTypeCode : req.body.ddlType);
    var agentChannelID = (req.body.ddlChannel == '' || typeof req.body.ddlChannel === "undefined" ? user.agentGroup : req.body.ddlChannel);
    var agentLevel = (req.body.ddlLevel == '' || typeof req.body.ddlLevel === "undefined" ? user.agentLevel : req.body.ddlLevel);


    var dataGroup;
    console.log(user);
    // Create a new instance of a Workbook class 
    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 
    var wsc = wb.addWorksheet('Pivot');
    var wst = wb.addWorksheet('Transaction Log');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });

    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataSummaryBoldFontStylewithTopBotBorder = wb.createStyle({
        fill: {

            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: 'f5f5f5' // you can
        },
        font: {
            color: '#24292E',
            bold: true,
            size: 13,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });

    var dataSummaryStyle = wb.createStyle({
        fill: {

            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: 'f5f5f5' // you can
        },
        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });

    var dataTransStyle = wb.createStyle({

        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });

    var lstHeader = null;
    var lstChild = null;
    var ae = null;
    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);
    wsc.column(5).setWidth(20);
    wsc.column(6).setWidth(20);
    wsc.column(7).setWidth(35);

    wst.column(1).setWidth(35);
    wst.column(2).setWidth(25);
    wst.column(3).setWidth(30);
    wst.column(4).setWidth(45);
    wst.column(5).setWidth(25);
    wst.column(6).setWidth(5);
    wst.column(7).setWidth(25);
    wst.column(8).setWidth(25);
    wst.column(9).setWidth(25);
    wst.column(10).setWidth(25);
    wst.column(11).setWidth(25);
    wst.column(12).setWidth(25);
    wst.column(13).setWidth(30);
    wst.column(14).setWidth(30);
    wst.column(15).setWidth(10);
    wst.column(16).setWidth(10);
    wst.column(17).setWidth(10);
    wst.column(18).setWidth(10);
    wst.column(19).setWidth(10);
    wst.column(20).setWidth(10);
    wst.column(21).setWidth(10);
    wst.column(22).setWidth(10);
    wst.column(23).setWidth(10);
    wst.column(24).setWidth(10);
    wst.column(25).setWidth(10);
    wst.column(26).setWidth(10);
    wst.column(27).setWidth(45);
    wst.column(28).setWidth(45);
    wst.column(29).setWidth(25);
    wst.column(30).setWidth(25);
    wst.column(31).setWidth(25);
    wst.column(32).setWidth(25);
    wst.column(33).setWidth(25);
    wst.column(34).setWidth(25);
    wst.column(35).setWidth(45);

    var currentRow = 17;
    var sumTransAmount = 0;
    var summoneyAmount = 0;
    var sumcomission = 0;
    var sumvat_of_com = 0;
    var sumwithholding_tax_of_com = 0;
    var sumnet_com = 0;

    dailySumRepo.SelectSummaryByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
        function (err, a) {


            return dailySumRepo.SelectByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
                function (err, lstData) {

                    return serviceRepo.findServiceProfile(agentLevel, agentTypeID)
                        .then(function (serviceData) {

                            lstHeader = lstData;

                            // *** Header 
                            wsc.cell(1, 1, 1, 6, true).string('Monthly  Commission Report ');
                            // *** Master Comission Panel
                            wsc.cell(3, 1).string('TermTrue Service').style(headerBoxStyle);
                            wsc.cell(3, 2).string('2.1 True operate (CP group)').style(headerBoxStyle);

                            var row = 3;
                            for (h = 0; h < Object.keys(serviceData).length; h++) {
                                row++;
                                wsc.cell(row, 1).string(serviceData[h].serviceName).style(boxStyle);
                                wsc.cell(row, 2).string(serviceData[h].com).style(boxStyle);
                            }

                            wsc.cell(8, 1).string('Remark').style(boldSmallFontStyle);
                            wsc.cell(9, 1).string('   1.Net Commission (include vat)').style(boldSmallFontStyle);
                            wsc.cell(10, 1).string('   2. Commission calculate from harvest money').style(boldSmallFontStyle);
                            wsc.cell(11, 1).string('   3. Cut off report when 23:59:59 of day').style(boldSmallFontStyle);

                            // ***** Summary Comission Panel
                            wsc.cell(2, 4).string('Company: ' + agentTypeID);
                            wsc.cell(3, 4).string('Report for month: ');

                            wsc.cell(4, 4).string('Number of transaction:').style(boxStyle);
                            wsc.cell(4, 5).string(a[0].transAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(5, 4).string('Amount:').style(boxStyle);
                            wsc.cell(5, 5).string(a[0].moneyAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(6, 4).string('Commission   (excluding vat):').style(boxStyle);
                            wsc.cell(6, 5).string(a[0].comission.toString()).style(dataMoneyStyle);

                            wsc.cell(7, 4).string('Vat of commission (excluding vat):').style(boxStyle);
                            wsc.cell(7, 5).string(a[0].vat_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(8, 4).string('Withholding tax of commission (excluding vat) :').style(boxStyle);
                            wsc.cell(8, 5).string(a[0].withholding_tax_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(9, 4).string('Net Commission (include vat):').style(boxStyle);
                            wsc.cell(9, 5).string(a[0].net_com.toString()).style(dataMoneyStyle);

                            // ***** Condition body panel
                            wsc.cell(14, 1).string('Commission lot (Month): ').style(dataHeaderStyle);
                            wsc.cell(15, 1).string('Status Collect Money : AUTO').style(dataHeaderStyle);

                            wsc.cell(17, 1).string('Row Labels').style(dataHeaderStyle);
                            wsc.cell(17, 2).string('Count of Transection ID').style(dataHeaderStyle);
                            wsc.cell(17, 3).string('Sum of Collect_money').style(dataHeaderStyle);
                            wsc.cell(17, 4).string('Sum of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 5).string('Sum of Vat of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 6).string('Sum of withholding tax of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 7).string('Sum of Net Commission (include vat)').style(dataHeaderStyle);
                            console.log(lstHeader);
                            return lstData;

                        }).then(function (lstHeader) {
                            return dailySumRepo.SelectByUserAndSerialNumWithOptionNoGroupBy(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,

                                function (err, dataDetail) {
                                    console.log('-----------------')
                                    for (i = 0; i < Object.keys(lstHeader).length; i++) {

                                        currentRow = currentRow + 1;
                                        wsc.cell(currentRow, 1).string(' # ' + lstHeader[i].dealerName).style(boldFontStylewithTopBotBorder);
                                        wsc.row(currentRow).group(1, true);

                                        for (j = 0; j < Object.keys(dataDetail).length; j++) {
                                            if (lstHeader[i].serialNumber == dataDetail[j].serialNumber) {
                                                currentRow = currentRow + 1;
                                                console.log('detail' + currentRow);

                                                wsc.cell(currentRow, 1).string('  -  ' + dataDetail[j].serviceName).style(boldFontStylewithTopBotBorder);
                                                wsc.cell(currentRow, 2).string(dataDetail[j].transAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 3).string(dataDetail[j].moneyAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 4).string(dataDetail[j].comission.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 5).string(dataDetail[j].vat_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 6).string(dataDetail[j].withholding_tax_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 7).string(dataDetail[j].net_com.toString()).style(dataDetailStyle);

                                                wsc.row(currentRow).group(1, true);
                                            }
                                        }

                                        console.log('-----------------' + lstHeader[i].serialNumber)
                                        currentRow = currentRow + 1;
                                        console.log('header' + currentRow);
                                        wsc.cell(currentRow, 1).string(lstHeader[i].aliasName).style(dataSummaryBoldFontStylewithTopBotBorder);
                                        wsc.cell(currentRow, 2).string(lstHeader[i].transAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 3).string(lstHeader[i].moneyAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 4).string(lstHeader[i].comission.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 5).string(lstHeader[i].vat_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 6).string(lstHeader[i].withholding_tax_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 7).string(lstHeader[i].net_com.toString()).style(dataSummaryStyle);

                                        sumTransAmount = sumTransAmount + lstHeader[i].transAmount;
                                        summoneyAmount = summoneyAmount + lstHeader[i].moneyAmount;
                                        sumcomission = sumcomission + lstHeader[i].comission;
                                        sumvat_of_com = sumvat_of_com + lstHeader[i].vat_of_com;
                                        sumwithholding_tax_of_com = sumwithholding_tax_of_com + lstHeader[i].withholding_tax_of_com;
                                        sumnet_com = sumnet_com + lstHeader[i].net_com;
                                    }

                                    currentRow = currentRow + 1;
                                    wsc.cell(currentRow, 1).string('Total ').style(dataTotalStyle);
                                    wsc.cell(currentRow, 2).string(sumTransAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 3).string(summoneyAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 4).string(sumcomission.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 5).string(sumvat_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 6).string(sumwithholding_tax_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 7).string(sumnet_com.toString()).style(dataTotalStyle);

                                    return dailySumRepo.selectTranByConditionWithComNew(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
                                        function (err, data) {

                                            console.log('Data received from Db:\n' + Object.keys(data).length);
                                            var tRow = 1;

                                            //set header 
                                            wst.cell(1, 1).string('time_kiosk_id_unique').style(boldFontStyle);
                                            wst.cell(1, 2).string('true_wallet_id').style(boldFontStyle);
                                            wst.cell(1, 3).string('serial_number').style(boldFontStyle);
                                            wst.cell(1, 4).string('transection_id').style(boldFontStyle);
                                            wst.cell(1, 5).string('time_stamp').style(boldFontStyle);
                                            wst.cell(1, 6).string('phone_no').style(boldFontStyle);
                                            wst.cell(1, 7).string('ref1').style(boldFontStyle);
                                            wst.cell(1, 8).string('product_group').style(boldFontStyle);
                                            wst.cell(1, 9).string('product_name').style(boldFontStyle);
                                            wst.cell(1, 10).string('select_amount').style(boldFontStyle);
                                            wst.cell(1, 11).string('fee').style(boldFontStyle);
                                            wst.cell(1, 12).string('credit_customer').style(boldFontStyle);
                                            wst.cell(1, 13).string('insert_amount').style(boldFontStyle);
                                            wst.cell(1, 14).string('result').style(boldFontStyle);
                                            wst.cell(1, 15).string('result_kiosk').style(boldFontStyle);
                                            wst.cell(1, 16).string('result_tmx').style(boldFontStyle);
                                            wst.cell(1, 17).string('result_msg').style(boldFontStyle);
                                            wst.cell(1, 18).string('COIN_10').style(boldFontStyle);
                                            wst.cell(1, 19).string('COIN_5').style(boldFontStyle);
                                            wst.cell(1, 20).string('COIN_2').style(boldFontStyle);
                                            wst.cell(1, 21).string('COIN_1').style(boldFontStyle);
                                            wst.cell(1, 22).string('BANK_1000').style(boldFontStyle);
                                            wst.cell(1, 23).string('BANK_500').style(boldFontStyle);
                                            wst.cell(1, 24).string('BANK_100').style(boldFontStyle);
                                            wst.cell(1, 25).string('BANK_50').style(boldFontStyle);
                                            wst.cell(1, 26).string('BANK_20').style(boldFontStyle);
                                            wst.cell(1, 27).string('harvest').style(boldFontStyle);
                                            wst.cell(1, 28).string('returnopenkiosk').style(boldFontStyle);
                                            wst.cell(1, 29).string('openkiosk').style(boldFontStyle);
                                            wst.cell(1, 30).string('Status Collect Money').style(boldFontStyle);
                                            wst.cell(1, 31).string('Commision rate').style(boldFontStyle);
                                            wst.cell(1, 32).string('commission  = 7% (excluding vat)').style(boldFontStyle);
                                            wst.cell(1, 33).string('Vat of commission (excluding vat) = 7%').style(boldFontStyle);
                                            wst.cell(1, 34).string('withholding tax of commission (excluding vat) = 3% ').style(boldFontStyle);
                                            wst.cell(1, 35).string('Net Commission (include vat)').style(boldFontStyle);
                                            wst.cell(1, 36).string('Code shop').style(boldFontStyle);

                                            console.log(data);
                                            console.log(Object.keys(data).length + '11111111111')
                                            for (i = 0; i < Object.keys(data).length; i++) {

                                                tRow = tRow + 1;

                                                wst.cell(tRow, 1).string(data[0, i].time_kiosk_id_unique).style(dataTransStyle);
                                                wst.cell(tRow, 2).string(data[0, i].true_wallet_id).style(dataTransStyle);
                                                wst.cell(tRow, 3).string(data[0, i].serial_number).style(dataTransStyle);
                                                wst.cell(tRow, 4).string(data[0, i].transection_id).style(dataTransStyle);

                                                wst.cell(tRow, 5).string(
                                                    data[0, i].time_stamp.getDate() + "/"
                                                    + month[data[0, i].time_stamp.getMonth()] + "/"
                                                    + data[0, i].time_stamp.getFullYear() + " "
                                                    + (data[0, i].time_stamp.getHours() < 10 ? "0" + data[0, i].time_stamp.getHours() : data[0, i].time_stamp.getHours()) + ":"
                                                    + (data[0, i].time_stamp.getMinutes() < 10 ? "0" + data[0, i].time_stamp.getMinutes() : data[0, i].time_stamp.getMinutes()) + ":"
                                                    + (data[0, i].time_stamp.getSeconds() < 10 ? "0" + data[0, i].time_stamp.getSeconds() : data[0, i].time_stamp.getSeconds())
                                                ).style(dataTransStyle);
                                                //wst.cell(tRow, 5).string(data[0, i].time_stamp.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 6).string(data[0, i].phone_no).style(dataTransStyle);
                                                wst.cell(tRow, 7).string(data[0, i].ref1).style(dataTransStyle);
                                                wst.cell(tRow, 8).string(data[0, i].serviceName.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 9).string(data[0, i].product_name).style(dataTransStyle);
                                                wst.cell(tRow, 10).string(data[0, i].select_amount).style(dataTransStyle);
                                                wst.cell(tRow, 11).string(data[0, i].fee.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 12).string(data[0, i].credit_customer).style(dataTransStyle);
                                                wst.cell(tRow, 13).string(data[0, i].insert_amount).style(dataTransStyle);
                                                wst.cell(tRow, 14).string(data[0, i].result).style(dataTransStyle);
                                                wst.cell(tRow, 15).string(data[0, i].result_kiosk).style(dataTransStyle);
                                                wst.cell(tRow, 16).string(data[0, i].result_tmx).style(dataTransStyle);
                                                wst.cell(tRow, 17).string(data[0, i].result_msg).style(dataTransStyle);
                                                wst.cell(tRow, 18).string(data[0, i].COIN_10).style(dataTransStyle);
                                                wst.cell(tRow, 19).string(data[0, i].COIN_5).style(dataTransStyle);
                                                wst.cell(tRow, 20).string(data[0, i].COIN_2).style(dataTransStyle);
                                                wst.cell(tRow, 21).string(data[0, i].COIN_1).style(dataTransStyle);
                                                wst.cell(tRow, 22).string(data[0, i].BANK_1000).style(dataTransStyle);
                                                wst.cell(tRow, 23).string(data[0, i].BANK_500).style(dataTransStyle);
                                                wst.cell(tRow, 24).string(data[0, i].BANK_100).style(dataTransStyle);
                                                wst.cell(tRow, 25).string(data[0, i].BANK_50).style(dataTransStyle);
                                                wst.cell(tRow, 26).string(data[0, i].BANK_20).style(dataTransStyle);
                                                wst.cell(tRow, 27).string(data[0, i].harvest.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 28).string(data[0, i].fromdt.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 29).string(data[0, i].todt.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 30).string('AUTO').style(dataTransStyle);
                                                wst.cell(tRow, 31).string(data[0, i].com_rate.toString()).style(dataTransStyle);
                                                wst.cell(tRow, 32).string(data[0, i].com.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTransStyle);
                                                wst.cell(tRow, 33).string(data[0, i].vat_of_com.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTransStyle);
                                                wst.cell(tRow, 34).string(data[0, i].withholding_tax_of_com.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTransStyle);
                                                wst.cell(tRow, 35).string(data[0, i].net_com.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTransStyle);
                                                wst.cell(tRow, 36).string(data[0, i].shopName.toString()).style(dataTransStyle);

                                            }


                                            wb.write('Commission Report.xlsx', res);
                                            console.log('----------------Finish------------------');

                                        });
                                });
                        })
                })
        });
}
exports.exportOnlyComission = function (req, res) {

    res.setHeader('Content-Type', 'application/json;  charset=tis-620;');
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpicker = req.body.dtpickerToDate;
    var user = req.session.user;

    var agentTypeID = (req.body.ddlType == '' || typeof req.body.ddlType === "undefined" ? user.agentTypeCode : req.body.ddlType);
    var agentChannelID = (req.body.ddlChannel == '' || typeof req.body.ddlChannel === "undefined" ? user.agentGroup : req.body.ddlChannel);
    var agentLevel = (req.body.ddlLevel == '' || typeof req.body.ddlLevel === "undefined" ? user.agentLevel : req.body.ddlLevel);


    var dataGroup;
    console.log(user);
    // Create a new instance of a Workbook class 
    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 
    var wsc = wb.addWorksheet('Pivot');
    var wst = wb.addWorksheet('Transaction Log');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });

    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });

    var dataSummaryStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });

    var lstHeader = null;
    var lstChild = null;
    var ae = null;
    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);
    wsc.column(5).setWidth(20);
    wsc.column(6).setWidth(20);
    wsc.column(7).setWidth(35);


    var currentRow = 17;
    var sumTransAmount = 0;
    var summoneyAmount = 0;
    var sumcomission = 0;
    var sumvat_of_com = 0;
    var sumwithholding_tax_of_com = 0;
    var sumnet_com = 0;

    dailySumRepo.SelectSummaryByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
        function (err, a) {
            return dailySumRepo.SelectByUserWithOption(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
                function (err, lstData) {

                    return serviceRepo.findServiceProfile(agentLevel, agentTypeID)
                        .then(function (serviceData) {

                            lstHeader = lstData;

                            // *** Header 
                            wsc.cell(1, 1, 1, 6, true).string('Monthly  Commission Report ');
                            // *** Master Comission Panel
                            wsc.cell(3, 1).string('TermTrue Service').style(headerBoxStyle);
                            wsc.cell(3, 2).string('2.1 True operate (CP group)').style(headerBoxStyle);

                            var row = 3;
                            for (h = 0; h < Object.keys(serviceData).length; h++) {
                                row++;
                                wsc.cell(row, 1).string(serviceData[h].serviceName).style(boxStyle);
                                wsc.cell(row, 2).string(serviceData[h].com).style(boxStyle);
                            }



                            wsc.cell(8, 1).string('Remark').style(boldSmallFontStyle);
                            wsc.cell(9, 1).string('   1.Net Commission (include vat)').style(boldSmallFontStyle);
                            wsc.cell(10, 1).string('   2. Commission calculate from harvest money').style(boldSmallFontStyle);
                            wsc.cell(11, 1).string('   3. Cut off report when 23:59:59 of day').style(boldSmallFontStyle);

                            // ***** Summary Comission Panel
                            wsc.cell(2, 4).string('Company: ' + agentTypeID);
                            wsc.cell(3, 4).string('Report for month: ');

                            wsc.cell(4, 4).string('Number of transaction:').style(boxStyle);
                            wsc.cell(4, 5).string(a[0].transAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(5, 4).string('Amount:').style(boxStyle);
                            wsc.cell(5, 5).string(a[0].moneyAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(6, 4).string('Commission   (excluding vat):').style(boxStyle);
                            wsc.cell(6, 5).string(a[0].comission.toString()).style(dataMoneyStyle);

                            wsc.cell(7, 4).string('Vat of commission (excluding vat):').style(boxStyle);
                            wsc.cell(7, 5).string(a[0].vat_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(8, 4).string('Withholding tax of commission (excluding vat) :').style(boxStyle);
                            wsc.cell(8, 5).string(a[0].withholding_tax_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(9, 4).string('Net Commission (include vat):').style(boxStyle);
                            wsc.cell(9, 5).string(a[0].net_com.toString()).style(dataMoneyStyle);

                            // ***** Condition body panel
                            wsc.cell(14, 1).string('Commission lot (Month): ').style(dataHeaderStyle);
                            wsc.cell(15, 1).string('Status Collect Money : AUTO').style(dataHeaderStyle);

                            wsc.cell(17, 1).string('Row Labels').style(dataHeaderStyle);
                            wsc.cell(17, 2).string('Count of Transection ID').style(dataHeaderStyle);
                            wsc.cell(17, 3).string('Sum of Collect_money').style(dataHeaderStyle);
                            wsc.cell(17, 4).string('Sum of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 5).string('Sum of Vat of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 6).string('Sum of withholding tax of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 7).string('Sum of Net Commission (include vat)').style(dataHeaderStyle);
                            console.log(lstHeader);
                            return lstData;

                        }).then(function (lstHeader) {
                            return dailySumRepo.SelectByUserAndSerialNumWithOptionNoGroupBy(agentTypeID, user.agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo, agentLevel,
                                function (err, dataDetail) {


                                    console.log('-----------------')
                                    for (i = 0; i < Object.keys(lstHeader).length; i++) {
                                        console.log('-----------------' + lstHeader[i].serialNumber)
                                        currentRow = currentRow + 1;
                                        console.log('header' + currentRow);
                                        wsc.cell(currentRow, 1).string(lstHeader[i].dealerName).style(boldFontStylewithTopBotBorder);
                                        wsc.cell(currentRow, 2).string(lstHeader[i].transAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 3).string(lstHeader[i].moneyAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 4).string(lstHeader[i].comission.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 5).string(lstHeader[i].vat_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 6).string(lstHeader[i].withholding_tax_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 7).string(lstHeader[i].net_com.toString()).style(dataSummaryStyle);

                                        sumTransAmount = sumTransAmount + lstHeader[i].transAmount;
                                        summoneyAmount = summoneyAmount + lstHeader[i].moneyAmount;
                                        sumcomission = sumcomission + lstHeader[i].comission;
                                        sumvat_of_com = sumvat_of_com + lstHeader[i].vat_of_com;
                                        sumwithholding_tax_of_com = sumwithholding_tax_of_com + lstHeader[i].withholding_tax_of_com;
                                        sumnet_com = sumnet_com + lstHeader[i].net_com;


                                        for (j = 0; j < Object.keys(dataDetail).length; j++) {
                                            if (lstHeader[i].serialNumber == dataDetail[j].serialNumber) {
                                                currentRow = currentRow + 1;
                                                console.log('detail' + currentRow);
                                                wsc.cell(currentRow, 1).string('_______' + dataDetail[j].serviceName).style(boldFontStylewithTopBotBorder);
                                                wsc.cell(currentRow, 2).string(dataDetail[j].transAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 3).string(dataDetail[j].moneyAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 4).string(dataDetail[j].comission.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 5).string(dataDetail[j].vat_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 6).string(dataDetail[j].withholding_tax_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 7).string(dataDetail[j].net_com.toString()).style(dataDetailStyle);

                                                wsc.row(currentRow).group(i + 1, true);
                                            }
                                        }
                                    }

                                    currentRow = currentRow + 1;
                                    wsc.cell(currentRow, 1).string('Total ').style(dataTotalStyle);
                                    wsc.cell(currentRow, 2).string(sumTransAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 3).string(summoneyAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 4).string(sumcomission.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 5).string(sumvat_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 6).string(sumwithholding_tax_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 7).string(sumnet_com.toString()).style(dataTotalStyle);


                                    wb.write('Excel.xlsx', res);
                                    console.log('----------------Finish------------------');
                                });
                        })
                })

        })
}

var xl = require('excel4node');
var dailySumRepo = require('../data/dailySummaryRepo');
var serviceRepo = require('../data/serviceProfileRepo');
var summaryRepository = require('../data/activityLogRepo');
var tranRepository = require('../data/transectionLogRepo');
var confRepo = require('../data/configRepo');

exports.render = function (req, res) {

    var lstYear = [];
    var makeYear = new Date().getFullYear();
    for (var i = makeYear; i >= 2017; i--) {       
        lstYear.push(i+543);
    }
    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;


    confRepo.selectConfigValueByType('m002').then(function (lstMonth) {
        res.render('report_07', {
            username: user.agentFullName,
            lstYear: lstYear,
            lstMonth: lstMonth,
            company: user.agentTypeCode,
            ddlType: 'm'
        });
    }); 
}
exports.search = function (req, res) {
   
    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;

    dailySumRepo.SelectSummaryByUserWithOption(user.agentUsername, dtpicker, dtpickerFrom, dtpickerTo, ddltype)
        .then(function (resultSummary) {

            var serviceTable = '';
            return serviceRepo.findServiceProfile(user.agentTypeCode, user.agentChannelCode)
                .then(function (serviceData) {

                    res.render('report_05', {
                        username: user.agentFullName,
                        from: dtpicker,
                        dateto: dtpickerTo,
                        datefrom: dtpickerFrom,
                        company: user.agentTypeCode,
                        ddlType: ddltype,
                        service: serviceData,
                        numberTrans: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].transAmount),
                        moneyAmount: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].moneyAmount),
                        comission: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].comission),
                        vat_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].vat_of_com),
                        withholding_tax_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].withholding_tax_of_com),
                        net_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].net_com)
                    });
                });
        });
}
exports.initialData = function (req, res) {
    
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpicker = req.body.dtpickerToDate;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;

    res.setHeader('Content-Type', 'application/json');

    //dailySumRepo.SelectByUserWithOption(user.agentUsername, dtpicker, dtpickerFrom, dtpickerTo, ddltype)
    //    .then(function (data) {

    //        console.log(data);
    //        var result = JSON.stringify(data);
    //        res.send(data);
    //    });
}
exports.detailData = function (req, res) {
   
    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpicker = req.body.dtpickerToDate;
    var kioskID = req.body.kioskID;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;

    dailySumRepo.SelectByUserAndSerialNumWithOption(user.agentUsername, dtpicker, kioskID, dtpickerFrom, dtpickerTo, ddltype)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.export = function (req, res) {


    res.setHeader('Content-Type', 'application/json;  charset=tis-620;');

    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpicker = req.body.dtpickerToDate;
    var ddltype = req.body.ddlType1;
    var user = req.session.user;
    var dataGroup;
    console.log(user);
    // Create a new instance of a Workbook class 
    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 
    var wsc = wb.addWorksheet('Pivot');
    var wst = wb.addWorksheet('Transaction Log');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });

    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });

    var dataSummaryStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });

    var lstHeader = null;
    var lstChild = null;
    var ae = null;
    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);
    wsc.column(5).setWidth(20);
    wsc.column(6).setWidth(20);
    wsc.column(7).setWidth(35);

    wst.column(1).setWidth(35);
    wst.column(2).setWidth(25);
    wst.column(3).setWidth(30);
    wst.column(4).setWidth(45);
    wst.column(5).setWidth(25);
    wst.column(6).setWidth(5);
    wst.column(7).setWidth(25);
    wst.column(8).setWidth(25);
    wst.column(9).setWidth(25);
    wst.column(10).setWidth(25);
    wst.column(11).setWidth(25);
    wst.column(12).setWidth(25);
    wst.column(13).setWidth(30);
    wst.column(14).setWidth(30);
    wst.column(15).setWidth(10);
    wst.column(16).setWidth(10);
    wst.column(17).setWidth(10);
    wst.column(18).setWidth(10);
    wst.column(19).setWidth(10);
    wst.column(20).setWidth(10);
    wst.column(21).setWidth(10);
    wst.column(22).setWidth(10);
    wst.column(23).setWidth(10);
    wst.column(24).setWidth(10);
    wst.column(25).setWidth(10);
    wst.column(26).setWidth(10);
    wst.column(27).setWidth(45);
    wst.column(28).setWidth(45);
    wst.column(29).setWidth(25);
    wst.column(30).setWidth(25);
    wst.column(31).setWidth(25);
    wst.column(32).setWidth(25);
    wst.column(33).setWidth(25);
    wst.column(34).setWidth(25);
    wst.column(35).setWidth(45);

    var currentRow = 17;
    var sumTransAmount = 0;
    var summoneyAmount = 0;
    var sumcomission = 0;
    var sumvat_of_com = 0;
    var sumwithholding_tax_of_com = 0;
    var sumnet_com = 0;

    dailySumRepo.SelectSummaryByUserWithOption(user.agentUsername, dtpicker, dtpickerFrom, dtpickerTo, ddltype)
        .then(function (a) {
            return dailySumRepo.SelectByUserWithOption(user.agentUsername, dtpicker, dtpickerFrom, dtpickerTo, ddltype)
                .then(function (lstData) {

                    return serviceRepo.findServiceProfile(user.agentTypeCode, user.agentChannelCode)
                        .then(function (serviceData) {

                            lstHeader = lstData;

                            // *** Header 
                            wsc.cell(1, 1, 1, 6, true).string('Monthly  Commission Report ');
                            // *** Master Comission Panel
                            wsc.cell(3, 1).string('TermTrue Service').style(headerBoxStyle);
                            wsc.cell(3, 2).string('2.1 True operate (CP group)').style(headerBoxStyle);

                            var row = 3;
                            for (h = 0; h < Object.keys(serviceData).length; h++) {
                                row++;
                                wsc.cell(row, 1).string(serviceData[h].serviceName).style(boxStyle);
                                wsc.cell(row, 2).string(serviceData[h].com).style(boxStyle);
                            }



                            wsc.cell(8, 1).string('Remark').style(boldSmallFontStyle);
                            wsc.cell(9, 1).string('   1.Net Commission (include vat)').style(boldSmallFontStyle);
                            wsc.cell(10, 1).string('   2. Commission calculate from harvest money').style(boldSmallFontStyle);
                            wsc.cell(11, 1).string('   3. Cut off report when 23:59:59 of day').style(boldSmallFontStyle);

                            // ***** Summary Comission Panel
                            wsc.cell(2, 4).string('Company: ' + user.agentTypeCode);
                            wsc.cell(3, 4).string('Report for month: ');

                            wsc.cell(4, 4).string('Number of transaction:').style(boxStyle);
                            wsc.cell(4, 5).string(a[0].transAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(5, 4).string('Amount:').style(boxStyle);
                            wsc.cell(5, 5).string(a[0].moneyAmount.toString()).style(dataMoneyStyle);

                            wsc.cell(6, 4).string('Commission   (excluding vat):').style(boxStyle);
                            wsc.cell(6, 5).string(a[0].comission.toString()).style(dataMoneyStyle);

                            wsc.cell(7, 4).string('Vat of commission (excluding vat):').style(boxStyle);
                            wsc.cell(7, 5).string(a[0].vat_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(8, 4).string('Withholding tax of commission (excluding vat) :').style(boxStyle);
                            wsc.cell(8, 5).string(a[0].withholding_tax_of_com.toString()).style(dataMoneyStyle);

                            wsc.cell(9, 4).string('Net Commission (include vat):').style(boxStyle);
                            wsc.cell(9, 5).string(a[0].net_com.toString()).style(dataMoneyStyle);

                            // ***** Condition body panel
                            wsc.cell(14, 1).string('Commission lot (Month): ').style(dataHeaderStyle);
                            wsc.cell(15, 1).string('Status Collect Money : AUTO').style(dataHeaderStyle);

                            wsc.cell(17, 1).string('Row Labels').style(dataHeaderStyle);
                            wsc.cell(17, 2).string('Count of Transection ID').style(dataHeaderStyle);
                            wsc.cell(17, 3).string('Sum of Collect_money').style(dataHeaderStyle);
                            wsc.cell(17, 4).string('Sum of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 5).string('Sum of Vat of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 6).string('Sum of withholding tax of commission (excluding vat)').style(dataHeaderStyle);
                            wsc.cell(17, 7).string('Sum of Net Commission (include vat)').style(dataHeaderStyle);
                            console.log(lstHeader);
                            return lstData;

                        }).then(function (lstHeader) {
                            return dailySumRepo.SelectByUserAndSerialNumWithOptionNoGroupBy(user.agentUsername, dtpicker, dtpickerFrom, dtpickerTo, ddltype)
                                .then(function (dataDetail) {

                                    console.log('-----------------')
                                    for (i = 0; i < Object.keys(lstHeader).length; i++) {
                                        console.log('-----------------' + lstHeader[i].serialNumber)
                                        currentRow = currentRow + 1;
                                        console.log('header' + currentRow);
                                        wsc.cell(currentRow, 1).string(lstHeader[i].dealerName).style(boldFontStylewithTopBotBorder);
                                        wsc.cell(currentRow, 2).string(lstHeader[i].transAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 3).string(lstHeader[i].moneyAmount.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 4).string(lstHeader[i].comission.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 5).string(lstHeader[i].vat_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 6).string(lstHeader[i].withholding_tax_of_com.toString()).style(dataSummaryStyle);
                                        wsc.cell(currentRow, 7).string(lstHeader[i].net_com.toString()).style(dataSummaryStyle);

                                        sumTransAmount = sumTransAmount + lstHeader[i].transAmount;
                                        summoneyAmount = summoneyAmount + lstHeader[i].moneyAmount;
                                        sumcomission = sumcomission + lstHeader[i].comission;
                                        sumvat_of_com = sumvat_of_com + lstHeader[i].vat_of_com;
                                        sumwithholding_tax_of_com = sumwithholding_tax_of_com + lstHeader[i].withholding_tax_of_com;
                                        sumnet_com = sumnet_com + lstHeader[i].net_com;


                                        for (j = 0; j < Object.keys(dataDetail).length; j++) {
                                            if (lstHeader[i].serialNumber == dataDetail[j].serialNumber) {
                                                currentRow = currentRow + 1;
                                                console.log('detail' + currentRow);
                                                wsc.cell(currentRow, 1).string('_______' + dataDetail[j].serviceName).style(boldFontStylewithTopBotBorder);
                                                wsc.cell(currentRow, 2).string(dataDetail[j].transAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 3).string(dataDetail[j].moneyAmount.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 4).string(dataDetail[j].comission.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 5).string(dataDetail[j].vat_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 6).string(dataDetail[j].withholding_tax_of_com.toString()).style(dataDetailStyle);
                                                wsc.cell(currentRow, 7).string(dataDetail[j].net_com.toString()).style(dataDetailStyle);

                                                wsc.row(currentRow).group(i + 1, true);
                                            }
                                        }
                                    }

                                    currentRow = currentRow + 1;
                                    wsc.cell(currentRow, 1).string('Total ').style(dataTotalStyle);
                                    wsc.cell(currentRow, 2).string(sumTransAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 3).string(summoneyAmount.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 4).string(sumcomission.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 5).string(sumvat_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 6).string(sumwithholding_tax_of_com.toString()).style(dataTotalStyle);
                                    wsc.cell(currentRow, 7).string(sumnet_com.toString()).style(dataTotalStyle);

                                });
                        })
                })
        }).then(function () {
            var a = " SELECT  t.* ,s.serviceName,c.*,a.* , CONCAT(kr.alias, kr.shop_name) shopName" +
                " from kio_t_agentUser auser  " +
                " join kioskview kr on auser.agentTypeCode = kr.agentTypeCode and auser.agentChannelCode = kr.agentChannelCode  " +
                " join kiosk_summarycomission_log a on a.serial_number = kr.kioskSerialNum   " +
                " join kiosk_transaction_log t on t.serial_number = a.serial_number " +
                " AND t.time_stamp >= a.fromdt " +
                " AND t.time_stamp <= a.todt " +
                " and t.product_group = a.product_group " +
                " join kio_m_service s on t.product_group = s.serviceID " +
                " join kiosk_commission_log c on c.time_kiosk_id_unique = t.time_kiosk_id_unique " +
                // user.agentUsername, dtpickerFrom, dtpickerTo
                " WHERE (todt) >= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 00:00:00')   " +
                " AND (todt) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +
                " and auser.agentUsername = '" + user.agentUsername + "'  " +
                " and t.transection_id != 0 " +
                " AND t.insert_amount > 0 " +
                " AND t.result NOT LIKE ('CANCEL%') "

            console.log(a);

            //    { replacements: { user: USER, fromdt: DATE_FROM,todt: DATE_TO },type: sequelize.QueryTypes.SELECT });
            const mysql = require('mysql');
            const con = mysql.createConnection({
                host: '192.168.200.2',
                port: '3307',
                user: 'root',
                password: 'anundadb',
                database: 'testtrue_'
            });

            con.query(a, (err, data) => {
                if (err) throw err;

                console.log('Data received from Db:\n' + Object.keys(data).length);
                var tRow = 1;

                //set header 
                wst.cell(1, 1).string('time_kiosk_id_unique').style(boldFontStyle);
                wst.cell(1, 2).string('serial_number').style(boldFontStyle);
                wst.cell(1, 3).string('transection_id').style(boldFontStyle);
                wst.cell(1, 4).string('time_stamp').style(boldFontStyle);
                wst.cell(1, 5).string('phone_no').style(boldFontStyle);
                wst.cell(1, 6).string('ref1').style(boldFontStyle);
                wst.cell(1, 7).string('product_group').style(boldFontStyle);
                wst.cell(1, 8).string('product_name').style(boldFontStyle);
                wst.cell(1, 9).string('select_amount').style(boldFontStyle);
                wst.cell(1, 10).string('fee').style(boldFontStyle);
                wst.cell(1, 11).string('credit_customer').style(boldFontStyle);
                wst.cell(1, 12).string('insert_amount').style(boldFontStyle);
                wst.cell(1, 13).string('result').style(boldFontStyle);
                wst.cell(1, 14).string('result_kiosk').style(boldFontStyle);
                wst.cell(1, 15).string('result_tmx').style(boldFontStyle);
                wst.cell(1, 16).string('result_msg').style(boldFontStyle);
                wst.cell(1, 17).string('COIN_10').style(boldFontStyle);
                wst.cell(1, 18).string('COIN_5').style(boldFontStyle);
                wst.cell(1, 19).string('COIN_2').style(boldFontStyle);
                wst.cell(1, 20).string('COIN_1').style(boldFontStyle);
                wst.cell(1, 21).string('BANK_1000').style(boldFontStyle);
                wst.cell(1, 22).string('BANK_500').style(boldFontStyle);
                wst.cell(1, 23).string('BANK_100').style(boldFontStyle);
                wst.cell(1, 24).string('BANK_50').style(boldFontStyle);
                wst.cell(1, 25).string('BANK_20').style(boldFontStyle);
                wst.cell(1, 26).string('harvest').style(boldFontStyle);
                wst.cell(1, 27).string('returnopenkiosk').style(boldFontStyle);
                wst.cell(1, 28).string('openkiosk').style(boldFontStyle);
                wst.cell(1, 29).string('Status Collect Money').style(boldFontStyle);
                wst.cell(1, 30).string('Commision rate').style(boldFontStyle);
                wst.cell(1, 31).string('commission  = 7% (excluding vat)').style(boldFontStyle);
                wst.cell(1, 32).string('Vat of commission (excluding vat) = 7%').style(boldFontStyle);
                wst.cell(1, 33).string('withholding tax of commission (excluding vat) = 3% ').style(boldFontStyle);
                wst.cell(1, 34).string('Net Commission (include vat)').style(boldFontStyle);
                wst.cell(1, 35).string('Code shop').style(boldFontStyle);

                console.log(Object.keys(data).length + '11111111111')
                for (i = 0; i < Object.keys(data).length; i++) {

                    tRow = tRow + 1;

                    wst.cell(tRow, 1).string(data[0, i].time_kiosk_id_unique).style(dataSummaryStyle);
                    wst.cell(tRow, 2).string(data[0, i].serial_number).style(dataSummaryStyle);
                    wst.cell(tRow, 3).string(data[0, i].transection_id).style(dataSummaryStyle);
                    wst.cell(tRow, 4).string(data[0, i].time_stamp.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 5).string(data[0, i].phone_no).style(dataSummaryStyle);
                    wst.cell(tRow, 6).string(data[0, i].ref1).style(dataSummaryStyle);
                    wst.cell(tRow, 7).string(data[0, i].serviceName.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 8).string(data[0, i].product_name).style(dataSummaryStyle);
                    wst.cell(tRow, 9).string(data[0, i].select_amount).style(dataSummaryStyle);
                    wst.cell(tRow, 10).string(data[0, i].fee).style(dataSummaryStyle);
                    wst.cell(tRow, 11).string(data[0, i].credit_customer).style(dataSummaryStyle);
                    wst.cell(tRow, 12).string(data[0, i].insert_amount).style(dataSummaryStyle);
                    wst.cell(tRow, 13).string(data[0, i].result).style(dataSummaryStyle);
                    wst.cell(tRow, 14).string(data[0, i].result_kiosk).style(dataSummaryStyle);
                    wst.cell(tRow, 15).string(data[0, i].result_tmx).style(dataSummaryStyle);
                    wst.cell(tRow, 16).string(data[0, i].result_msg).style(dataSummaryStyle);
                    wst.cell(tRow, 17).string(data[0, i].COIN_10).style(dataSummaryStyle);
                    wst.cell(tRow, 18).string(data[0, i].COIN_5).style(dataSummaryStyle);
                    wst.cell(tRow, 19).string(data[0, i].COIN_2).style(dataSummaryStyle);
                    wst.cell(tRow, 20).string(data[0, i].COIN_1).style(dataSummaryStyle);
                    wst.cell(tRow, 21).string(data[0, i].BANK_1000).style(dataSummaryStyle);
                    wst.cell(tRow, 22).string(data[0, i].BANK_500).style(dataSummaryStyle);
                    wst.cell(tRow, 23).string(data[0, i].BANK_100).style(dataSummaryStyle);
                    wst.cell(tRow, 24).string(data[0, i].BANK_50).style(dataSummaryStyle);
                    wst.cell(tRow, 25).string(data[0, i].BANK_20).style(dataSummaryStyle);
                    wst.cell(tRow, 26).string(data[0, i].harvest.toString()).style(dataSummaryStyle);

                    wst.cell(tRow, 27).string(data[0, i].fromdt.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 28).string(data[0, i].todt.toString()).style(dataSummaryStyle);
                    // wst.cell(tRow, 26).string(data[0, i].incoming date.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 29).string('AUTO').style(dataSummaryStyle);
                    wst.cell(tRow, 30).string(data[0, i].com_rate.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 31).string(data[0, i].com.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 32).string(data[0, i].vat_of_com.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 33).string(data[0, i].withholding_tax_of_com.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 34).string(data[0, i].net_com.toString()).style(dataSummaryStyle);
                    wst.cell(tRow, 35).string(data[0, i].shopName.toString()).style(dataSummaryStyle);



                }



                wb.write('Excel.xlsx', res);
                console.log('----------------Finish------------------');

            });
        })
}

var xl = require('excel4node');
var helper = require('../helper/utility.helper');
var activityRepository = require('../data/activityLogRepo');
var tranRepository = require('../data/transectionLogRepo');
var dailySumRepo = require('../data/dailySummaryRepo');
var wordingRepo = require('../data/wordingRepo');
var AgentTypeRepo = require('../data/v_AgentTypeRepo');

exports.render = function (req, res) {

    var date = req.query.date;
    var type = req.query.type;
    var dtpickerFrom = (req.body.dtpickerFrom);
    var dtpickerTo = (req.body.dtpickerTo);
    var user = req.session.user;

    AgentTypeRepo.GetAllType(
        function (err, agentType) {

            res.render('report_02_01', {
                agentType: agentType,
                selectedAgentType: '',
                selectedChannelType: '',
                type: type,
                from: null,
                to: null,
                date: date,
                lv: user.agentLevel,
                username: user.agentFullName,
                userSelect: user.agentLevel,
                txtKioskID: '',
                issearch: 0
            });

        });
}
exports.search = function (req, res) {
    var type = req.body.sel1;
    var selectedAgentType = req.body.ddlType;
    var selectedChannelType = req.body.ddlChannel;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var KioskID = req.body.txtKioskID;
    var user = req.session.user;
    console.log("---------------------------")
    console.log(req.body)

    AgentTypeRepo.GetAllType(
        function (err, agentType) {

            res.render('report_02_01', {
                agentType: agentType,
                selectedAgentType: selectedAgentType,
                selectedChannelType: selectedChannelType,
                lv: user.agentLevel,
                username: user.agentFullName,
                userSelect: user.agentLevel,
                type: type,
                from: dtpickerFrom,
                to: dtpickerTo,
                date: null,
                txtKioskID: '', issearch: 1
            });
        });
}

exports.initialData = function (req, res) {

    console.log('--- INITIAL DATA FOR REPORT R02_01');
    console.log(req.body);
    var user = req.session.user;
    var agentTypeID = (req.body.typeID == '' ? user.agentTypeCode : req.body.typeID);
    var agentChannelID = (req.body.channelID == '' ? user.agentGroup : req.body.channelID);
    var agentLevel = (req.body.typeID == '' ? user.agentLevel : 1);

    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var txtKioskID = '';
   
    res.setHeader('Content-Type', 'application/json');
    activityRepository.findSummaryDailyReport_02_v2(agentTypeID, agentChannelID, agentLevel, dtpickerFrom, dtpickerTo, user.agentUsername,
        function (err, data) {
            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.initialChannel = function (req, res) {
    var user = req.session.user;
    var agentTypeID = req.body.agentType;
    res.setHeader('Content-Type', 'application/json');

    AgentTypeRepo.GetAllChannelByTypeID(agentTypeID,
        function (err, data) {

            console.log(data);
            var result = JSON.stringify(data);
            res.send(data);
        });

}

exports.detailData = function (req, res) {
    var user = req.session.user;
    var type = req.body.sel;
    var agentTypeID = (req.body.typeID == '' ? user.agentTypeCode : req.body.typeID);
    var agentChannelID = (req.body.channelID == '' ? user.agentGroup : req.body.channelID);
    var agentLevel = (req.body.typeID == '' ? user.agentLevel : 1);
    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var kioskID = req.body.kioskID;
    console.log(kioskID);

    res.setHeader('Content-Type', 'application/json');
    activityRepository.findDetsilDailyReport_02_v2(agentTypeID, agentChannelID, agentLevel, dtpickerFrom, dtpickerTo, kioskID, user.agentUsername,
        function (err, data) {


            var result = JSON.stringify(data);
            res.send(data);
        });

}
exports.export = function (req, res) {

    console.log('----------------export------------------');
    res.setHeader('Content-Type', 'application/json;  charset=tis-620;');

    var month = new Array();
    month[0] = "01";
    month[1] = "02";
    month[2] = "03";
    month[3] = "04";
    month[4] = "05";
    month[5] = "06";
    month[6] = "07";
    month[7] = "08";
    month[8] = "09";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";

    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpicker = req.body.dtpickerToDate;
    var ddltype = req.body.ddlType1;
    var user = req.session.user;
    var agentTypeID = (req.body.typeID == '' ? user.agentTypeCode : req.body.typeID);
    var agentChannelID = (req.body.channelID == '' ? user.agentGroup : req.body.channelID);
    var agentLevel = (req.body.typeID == '' ? user.agentLevel : 1);

    var dataGroup;
    console.log(user);

    // Create a new instance of a Workbook class 
    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 
    var wsc = wb.addWorksheet('Daily Report');
    var wst = wb.addWorksheet('Raw Transaction');
    var wscl = wb.addWorksheet('Call Log Report');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });
    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });
    var dataSummaryStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });
    var dataTransStyle = wb.createStyle({

        font: {
            color: '#24292E',
            bold: true,
            size: 13
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var lstHeader = null;
    var lstChild = null;
    var ae = null;

    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);

    var currentRow = 1;
    var sumKeepInKiosk = 0;
    var sumKeepOutKiosk = 0;
    var sumGrandTotal = 0;

    wordingRepo.GetWordingByGroup('report02_01',
        function (err, wording) {

            activityRepository.findExportSummaryDailyReport_02_v2(agentTypeID, agentChannelID, agentLevel, dtpickerFrom, dtpickerTo, user.agentUsername,
                function (err, rawHeader) {

                    console.log('----------------begin method------------------');
                    console.log(wording);
                    wsc.cell(1, 1).string('Shop Name').style(headerBoxStyle);
                    wsc.cell(1, 2).string(wording[0].wordingKey).style(headerBoxStyle);
                    wsc.cell(1, 3).string(wording[1].wordingKey).style(headerBoxStyle);
                    wsc.cell(1, 4).string('GrandTotal').style(headerBoxStyle);

                    activityRepository.findExportDailyReport_02_v2(agentTypeID, agentChannelID, agentLevel, dtpickerFrom, dtpickerTo, user.agentUsername,
                        function (err, rawDetail) {

                            if (typeof rawHeader != 'undefined') {
                                for (i = 0; i < Object.keys(rawHeader).length; i++) {

                                    currentRow = currentRow + 1;
                                    wsc.cell(currentRow, 1).string(typeof rawHeader[0, i].shop_name != 'undefined' && rawHeader[0, i].shop_name != null ? " #  " + rawHeader[0, i].shop_name : "").style(boldFontStylewithTopBotBorder);
                                    wsc.row(currentRow).group(1, false);
                                    for (j = 0; j < Object.keys(rawDetail).length; j++) {

                                        if (rawHeader[0, i].serialNum == rawDetail[0, j].serialNum) {
                                            currentRow = currentRow + 1;
                                            var d = rawDetail[0, j].date;
                                            //wsc.cell(currentRow, 1).string(' -  ' + d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + " ").style(boldFontStylewithTopBotBorder);
                                            wsc.cell(currentRow, 1).string(rawDetail[0, j].date).style(boldFontStylewithTopBotBorder);
                                            wsc.cell(currentRow, 2).string(parseFloat(typeof rawDetail[0, j].money_inbox != 'undefined' ? rawDetail[0, j].money_inbox : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataDetailStyle);
                                            wsc.cell(currentRow, 3).string(parseFloat(typeof rawDetail[0, j].daily_money != 'undefined' ? rawDetail[0, j].daily_money : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataDetailStyle);
                                            wsc.cell(currentRow, 4).string(parseFloat(typeof rawDetail[0, j].GrandTotal != 'undefined' ? rawDetail[0, j].GrandTotal : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataDetailStyle);

                                            wsc.row(currentRow).group(1, false);

                                        }
                                    }

                                    currentRow = currentRow + 1;
                                    wsc.cell(currentRow, 1).string(typeof rawHeader[0, i].shop_name != 'undefined' && rawHeader[0, i].shop_name != null ? rawHeader[0, i].shop_name : "").style(boldFontStylewithTopBotBorder);
                                    wsc.cell(currentRow, 2).string(parseInt(typeof rawHeader[0, i].money_inbox != 'undefined' ? rawHeader[0, i].money_inbox : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataSummaryStyle);
                                    wsc.cell(currentRow, 3).string(parseInt(typeof rawHeader[0, i].daily_money != 'undefined' ? rawHeader[0, i].daily_money : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataSummaryStyle);
                                    wsc.cell(currentRow, 4).string(parseInt(typeof rawHeader[0, i].GrandTotal != 'undefined' ? rawHeader[0, i].GrandTotal : 0).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataSummaryStyle);

                                    sumKeepInKiosk = sumKeepInKiosk + rawHeader[0, i].money_inbox;
                                    sumKeepOutKiosk = sumKeepOutKiosk + rawHeader[0, i].daily_money;
                                    sumGrandTotal = sumGrandTotal + rawHeader[0, i].GrandTotal;

                                }
                                currentRow = currentRow + 1;
                                wsc.cell(currentRow, 1).string('Total ').style(dataTotalStyle);
                                wsc.cell(currentRow, 2).string(sumKeepInKiosk.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTotalStyle);
                                wsc.cell(currentRow, 3).string(sumKeepOutKiosk.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTotalStyle);
                                wsc.cell(currentRow, 4).string(sumGrandTotal.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataTotalStyle);

                                dailySumRepo.selectTranByCondition(agentTypeID, agentLevel, agentChannelID, user.agentUserID, dtpickerFrom, dtpickerTo,
                                    function (err, data) {

                                        console.log('Data received from Db:\n' + Object.keys(data).length);
                                        var tRow = 1;

                                        //set header 
                                        wst.cell(1, 1).string('time_kiosk_id_unique').style(boldFontStyle);
                                        wst.cell(1, 2).string('true_wallet_id').style(boldFontStyle);
                                        wst.cell(1, 3).string('serial_number').style(boldFontStyle);
                                        wst.cell(1, 4).string('transection_id').style(boldFontStyle);
                                        wst.cell(1, 5).string('time_stamp').style(boldFontStyle);
                                        wst.cell(1, 6).string('phone_no').style(boldFontStyle);
                                        wst.cell(1, 7).string('ref1').style(boldFontStyle);
                                        wst.cell(1, 8).string('product_group').style(boldFontStyle);
                                        wst.cell(1, 9).string('product_name').style(boldFontStyle);
                                        wst.cell(1, 10).string('select_amount').style(boldFontStyle);
                                        wst.cell(1, 11).string('fee').style(boldFontStyle);
                                        wst.cell(1, 12).string('credit_customer').style(boldFontStyle);
                                        wst.cell(1, 13).string('insert_amount').style(boldFontStyle);
                                        wst.cell(1, 14).string('result').style(boldFontStyle);
                                        wst.cell(1, 15).string('result_kiosk').style(boldFontStyle);
                                        wst.cell(1, 16).string('result_tmx').style(boldFontStyle);
                                        wst.cell(1, 17).string('result_msg').style(boldFontStyle);
                                        wst.cell(1, 18).string('COIN_10').style(boldFontStyle);
                                        wst.cell(1, 19).string('COIN_5').style(boldFontStyle);
                                        wst.cell(1, 20).string('COIN_2').style(boldFontStyle);
                                        wst.cell(1, 21).string('COIN_1').style(boldFontStyle);
                                        wst.cell(1, 22).string('BANK_1000').style(boldFontStyle);
                                        wst.cell(1, 23).string('BANK_500').style(boldFontStyle);
                                        wst.cell(1, 24).string('BANK_100').style(boldFontStyle);
                                        wst.cell(1, 25).string('BANK_50').style(boldFontStyle);
                                        wst.cell(1, 26).string('BANK_20').style(boldFontStyle);
                                        wst.cell(1, 27).string('harvest').style(boldFontStyle);

                                        wst.cell(1, 36).string('Code shop').style(boldFontStyle);

                                        for (i = 0; i < Object.keys(data).length; i++) {

                                            tRow = tRow + 1;
                                            wst.cell(tRow, 1).string(data[0, i].time_kiosk_id_unique).style(dataTransStyle);
                                            wst.cell(tRow, 2).string(data[0, i].true_wallet_id).style(dataTransStyle);
                                            wst.cell(tRow, 3).string(data[0, i].serial_number).style(dataTransStyle);
                                            wst.cell(tRow, 4).string(data[0, i].transection_id).style(dataTransStyle);

                                            wst.cell(tRow, 5).string(
                                                data[0, i].time_stamp.getDate() + "/"
                                                + month[data[0, i].time_stamp.getMonth()] + "/"
                                                + data[0, i].time_stamp.getFullYear() + " "
                                                + (data[0, i].time_stamp.getHours() < 10 ? "0" + data[0, i].time_stamp.getHours() : data[0, i].time_stamp.getHours()) + ":"
                                                + (data[0, i].time_stamp.getMinutes() < 10 ? "0" + data[0, i].time_stamp.getMinutes() : data[0, i].time_stamp.getMinutes()) + ":"
                                                + (data[0, i].time_stamp.getSeconds() < 10 ? "0" + data[0, i].time_stamp.getSeconds() : data[0, i].time_stamp.getSeconds())
                                            ).style(dataTransStyle);
                                            //wst.cell(tRow, 5).string(data[0, i].time_stamp.toString()).style(dataTransStyle);
                                            wst.cell(tRow, 6).string(data[0, i].phone_no).style(dataTransStyle);
                                            wst.cell(tRow, 7).string(data[0, i].ref1).style(dataTransStyle);
                                            wst.cell(tRow, 8).string(data[0, i].serviceName.toString()).style(dataTransStyle);
                                            wst.cell(tRow, 9).string(data[0, i].product_name).style(dataTransStyle);
                                            wst.cell(tRow, 10).string(data[0, i].select_amount).style(dataTransStyle);
                                            wst.cell(tRow, 11).string(data[0, i].fee.toString()).style(dataTransStyle);
                                            wst.cell(tRow, 12).string(data[0, i].credit_customer).style(dataTransStyle);
                                            wst.cell(tRow, 13).string(data[0, i].insert_amount).style(dataTransStyle);
                                            wst.cell(tRow, 14).string(data[0, i].result).style(dataTransStyle);
                                            wst.cell(tRow, 15).string(data[0, i].result_kiosk).style(dataTransStyle);
                                            wst.cell(tRow, 16).string(data[0, i].result_tmx).style(dataTransStyle);
                                            wst.cell(tRow, 17).string(data[0, i].result_msg).style(dataTransStyle);
                                            wst.cell(tRow, 18).string(data[0, i].COIN_10).style(dataTransStyle);
                                            wst.cell(tRow, 19).string(data[0, i].COIN_5).style(dataTransStyle);
                                            wst.cell(tRow, 20).string(data[0, i].COIN_2).style(dataTransStyle);
                                            wst.cell(tRow, 21).string(data[0, i].COIN_1).style(dataTransStyle);
                                            wst.cell(tRow, 22).string(data[0, i].BANK_1000).style(dataTransStyle);
                                            wst.cell(tRow, 23).string(data[0, i].BANK_500).style(dataTransStyle);
                                            wst.cell(tRow, 24).string(data[0, i].BANK_100).style(dataTransStyle);
                                            wst.cell(tRow, 25).string(data[0, i].BANK_50).style(dataTransStyle);
                                            wst.cell(tRow, 26).string(data[0, i].BANK_20).style(dataTransStyle);
                                            wst.cell(tRow, 27).string(data[0, i].harvest.toString()).style(dataTransStyle);
                                            wst.cell(tRow, 36).string(data[0, i].shopName.toString()).style(dataTransStyle);

                                        }

                                        activityRepository.findExportCallLogReport(agentTypeID, agentChannelID, agentLevel, dtpickerFrom, dtpickerTo)
                                            .then(function (rawHeader) {
                                                console.log('----------------begin method------------------');
                                                var currentRow = 1;
                                                wscl.cell(1, 1).string(wording[2].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 2).string(wording[3].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 3).string(wording[4].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 4).string(wording[5].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 5).string(wording[6].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 6).string(wording[7].wordingKey).style(headerBoxStyle);
                                                wscl.cell(1, 7).string(wording[8].wordingKey).style(headerBoxStyle);
                                                console.log(rawHeader);
                                                for (i = 0; i < Object.keys(rawHeader).length; i++) {
                                                    currentRow = currentRow + 1;
                                                    wscl.cell(currentRow, 1).string(rawHeader[0, i].time_stamp.toString()).style(boldFontStylewithTopBotBorder);
                                                    wscl.cell(currentRow, 2).string(rawHeader[0, i].dealer_code.toString()).style(dataSummaryStyle);
                                                    wscl.cell(currentRow, 3).string(rawHeader[0, i].shop_name.toString()).style(dataSummaryStyle);
                                                    wscl.cell(currentRow, 4).string(parseInt(rawHeader[0, i].money_inbox).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataSummaryStyle);
                                                    wscl.cell(currentRow, 5).string(rawHeader[0, i].callLog.toString()).style(dataSummaryStyle);
                                                    wscl.cell(currentRow, 6).string(typeof rawHeader[0, i].createDate != 'undefined' && rawHeader[0, i].createDate != null ? rawHeader[0, i].createDate : ' - ').style(dataSummaryStyle);
                                                    wscl.cell(currentRow, 7).string(typeof rawHeader[0, i].createBy != 'undefined' && rawHeader[0, i].createBy != null ? rawHeader[0, i].createBy : ' - ').style(dataSummaryStyle);
                                                }

                                                wb.write('Daily_Report.xlsx', res);
                                                console.log('----------------Finish------------------');

                                            });

                                    });
                            };
                        });
                });
        });
}


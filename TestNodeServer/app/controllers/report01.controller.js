exports.render = function (req, res) {

    var summaryRepository = require('../data/summaryKioskTransaction');
    summaryRepository.findDayByID(req.session.user.agentUserID, 7)
        .then(function (id) {
            console.log(id);
            res.render('report_01', { info: id, selectType: '1' });
        });

}
exports.submit = function (req, res) {

    var summaryRepository = require('../data/summaryKioskTransaction');
    var selectType = req.body.sel1;
    var limit = req.body.sel2;
    console.log(req.body.sel1);

    if (selectType == '2') {
        summaryRepository.findMonthByID(req.session.user.agentUserID, limit)
            .then(function (id) {
                console.log(id);
                res.render('report_01', { info: id, selectType: selectType });
            });
    } else if (selectType == '3') {
        summaryRepository.findYearByID(req.session.user.agentUserID, limit)
            .then(function (id) {
                console.log(id);
                res.render('report_01', { info: id, selectType: selectType });
            });
    } else {
        summaryRepository.findDayByID(req.session.user.agentUserID, limit)
            .then(function (id) {
                console.log(id);
                res.render('report_01', { info: id, selectType: selectType });
            });
    }

}


var serviceRepo = require('../data/serviceProfileRepo');
var dailySumRepo = require('../data/dailySummaryRepo');

exports.render = function (req, res) {


    var month = new Array();
    month[0] = "01";
    month[1] = "02";
    month[2] = "03";
    month[3] = "04";
    month[4] = "05";
    month[5] = "06";
    month[6] = "07";
    month[7] = "08";
    month[8] = "09";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";

    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;

    var makeDate = new Date();
    makeDate.setYear(makeDate.getFullYear() + 543);
    var CurDate = new Date(makeDate.setMonth(makeDate.getMonth()));
    var PreviousDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));

    var To = CurDate.getDate() + '/' + month[CurDate.getMonth()] + '/' + CurDate.getFullYear();
    var From = PreviousDate.getDate() + '/' + month[PreviousDate.getMonth()] + '/' + PreviousDate.getFullYear();

    dailySumRepo.SelectByGroupAndType(user.agentGroup, user.agentLevel, user.agentUserID, user.agentTypeCode, dtpickerFrom, dtpickerTo)
        .then(function (resultSummary) {
            res.render('report_05_01', {
                username: user.agentFullName,
                from: dtpicker,
                dateto: To,
                datefrom: From,
                company: user.agentTypeCode,
                ddlType: 'm',
                agentlv: user.agentLevel,
                numberTrans: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].transAmount),
                moneyAmount: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].moneyAmount),
                comission: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].comission),
                vat_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].vat_of_com),
                withholding_tax_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].withholding_tax_of_com),
                net_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].net_com)
            });

        });
}
exports.search = function (req, res) {

    var dtpicker = req.body.dtpickerToDate;
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var ddltype = req.body.ddlType1;
    var lv = req.body.lv;
    console.log('+++++++++++++++++++++++++');
    console.log(lv);

    dailySumRepo.SelectByGroupAndType(user.agentGroup, user.agentLevel, user.agentUserID, user.agentTypeCode, dtpickerFrom, dtpickerTo)
        .then(function (resultSummary) {
            res.render('report_05_01', {
                username: user.agentFullName,
                from: dtpicker,
                dateto: dtpickerTo,
                datefrom: dtpickerFrom,
                company: user.agentTypeCode,
                ddlType: ddltype,
                agentlv: user.agentLevel,
                numberTrans: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].transAmount),
                moneyAmount: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].moneyAmount),
                comission: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].comission),
                vat_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].vat_of_com),
                withholding_tax_of_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].withholding_tax_of_com),
                net_com: (typeof resultSummary[0] === "undefined" ? '' : resultSummary[0].net_com)
            });
        });}
exports.RenderLv0 = function (req, res) {

    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var lv = req.body.lv;

    dailySumRepo.SelectByGroupAndType(user.agentGroup, lv, user.agentLevel, user.agentUserID, user.agentTypeCode, dtpickerFrom, dtpickerTo)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
}

exports.RenderLv1 = function (req, res) {

    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var lv = req.body.lv;

    dailySumRepo.SelectByGroupAndTypeGroupTypeAndChanel(user.agentGroup, lv, user.agentLevel, user.agentUserID, user.agentTypeCode, dtpickerFrom, dtpickerTo)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.RenderLv1WithProductDetail = function (req, res) {

    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var lv = req.body.lv;
    var channel = req.body.channel;

    dailySumRepo.SelectByGroupAndTypeGroupTypeAndChanelWithDetail(user.agentGroup, lv, user.agentLevel, user.agentUserID, user.agentTypeCode,channel, dtpickerFrom, dtpickerTo)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
}

exports.RenderLv2 = function (req, res) {

    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var channel = req.body.channel;
    var lv = req.body.lv;

    dailySumRepo.SelectByGroupAndTypeGroupDealerID(user.agentGroup, lv, user.agentLevel, user.agentUserID, user.agentTypeCode, dtpickerFrom, dtpickerTo)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.RenderLv2WithProductDetail = function (req, res) {

    res.setHeader('Content-Type', 'application/json');
    var dtpickerTo = req.body.dtpickerTo;
    var dtpickerFrom = req.body.dtpickerFrom;
    var user = req.session.user;
    var kioskID = req.body.kioskID;
    var lv = req.body.lv;


    dailySumRepo.SelectByGroupAndTypeGroupDealerIDWithDetail(user.agentGroup, lv, user.agentLevel, user.agentUserID, user.agentTypeCode, kioskID, dtpickerFrom, dtpickerTo)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
}
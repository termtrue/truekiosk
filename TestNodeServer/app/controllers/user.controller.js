﻿var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var xlsx = require('node-xlsx');
var agentUserRepo = require('../data/agentUserRepo');
var masterRepo = require('../data/masterRepo');

exports.render = function (req, res) {

    masterRepo.GetAgentType()
        .then(function (AgentType) {

            masterRepo.GetAgentChannel()
                .then(function (AgentChannel) {

                    masterRepo.GetAgentLevel()
                        .then(function (AgentLevel) {

                            console.log(AgentType);

                            res.render('user/', {
                                username: user.agentFullName,
                                agentChannel: AgentChannel,
                                agentType: AgentType,
                                agentLevel: AgentLevel,
                                selectedChannel: '',
                                selectedType: '',
                                selectedLevel: '',
                                isSearch: 0
                            });
                        });
                });
        });
};
exports.search = function (req, res) {

    var agentType = req.body.agentType;
    var agentChannel = req.body.agentChannel;
    var agentLevel = req.body.agentLevel;

    masterRepo.GetAgentType()
        .then(function (AgentType) {

            masterRepo.GetAgentChannel()
                .then(function (AgentChannel) {

                    masterRepo.GetAgentLevel()
                        .then(function (AgentLevel) {

                            res.render('user/', {
                                username: user.agentFullName,
                                agentChannel: AgentChannel,
                                agentType: AgentType,
                                agentLevel: AgentLevel,
                                selectedType: agentType,
                                selectedChannel: agentChannel,
                                selectedLevel: agentLevel,
                                isSearch: 1
                            });

                        });
                });
        });
};
exports.initialData = function (req, res) {

    var agentType = req.body.agentType;
    var agentChannel = req.body.agentChannel;
    var agentLevel = req.body.agentLevel;

    res.setHeader('Content-Type', 'application/json');
    agentUserRepo.findByCondition(agentType, agentChannel, agentLevel)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });
};


exports.render_upload = function (req, res) {
    res.render('user/upload', { username: req.session.user.agentUsername, finish: true });
};
exports.render_LogUpload = function (req, res) {
    res.render('user/logUpload');
};
exports.userUpload = function (req, res) {

    var form = new formidable.IncomingForm();
    var fileUploadedName = "";
    // specify that we want to allow the user to upload multiple files in a single request
    form.keepExtensions = true;
    form.multiples = true;
    form.uploadDir = path.join(__dirname, '../../uploads');

    form.on('fileBegin', function (name, file) {
        if (file.size > 1) {
            console.log(file.size);
            throw new error; // this should stop the file upload (maybe before it begins; i don't know the internal part)
        }

        console.log(file.size);
    })
        // every time a file has been uploaded successfully,
        // rename it to it's orignal name
        .on('file', function (field, file) {
            fs.rename(file.path, path.join(form.uploadDir, file.name), (err) => {
                console.log(err);
            });
            console.log(file.path);
            console.log(form.uploadDir);
            console.log(file.name);
            console.log(path.extname(file.name));

            fileUploadedName = file.name;
        })

        // log any errors that occur
        .on('error', function (err) {
            console.log('An error has occured: \n' + err);
        })

        // once all the files have been uploaded, send a response to the client
        .on('end', function () {



            var array = xlsx.parse(form.uploadDir + '/' + fileUploadedName);
            //var array = xlsx.parse(fs.readFileSync(__dirname + '/' + fileUploadedName));

            res.writeHead(200, { 'content-type': 'text/plain' });
            res.end('received fields:\n\n ');
        });

    // parse the incoming request containing the form data
    form.parse(req);

    // return;

};




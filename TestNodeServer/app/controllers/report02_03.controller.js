var xl = require('excel4node');
var helper = require('../helper/utility.helper');
var activityRepository = require('../data/activityLogRepo');
var tranRepository = require('../data/transectionLogRepo');
var callLogRepository = require('../data/CallLogRepo');

exports.render = function (req, res) {
    var date = req.query.date;
    var type = req.query.type;
    var dtpickerFrom = (req.body.dtpickerFrom);
    var dtpickerTo = (req.body.dtpickerTo);
    var user = req.session.user;
    res.render('report_02_03', {
        type: type, from: null, to: null, date: date, lv: user.agentLevel, username: user.agentFullName,
        userSelect: user.agentLevel
        , txtKioskID: ''
    });
}
exports.search = function (req, res) {
    var type = req.body.sel1;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var KioskID = req.body.txtKioskID;
    var user = req.session.user;
    res.render('report_02_03', {
        Lv: user.agentLevel,
        userSelect: user.agentLevel, username: req.session.user.agentUsername,
        type: type, from: dtpickerFrom, to: dtpickerTo, date: null, txtKioskID: ''
    });
}
exports.initialData = function (req, res) {
    console.log('--- INITIAL DATA FOR REPORT R02_01');
    console.log(req.body);

    var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
    var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
    var txtKioskID = '';
    var user = req.session.user;

    var agentTypeID = (req.body.typeID == '' ? user.agentTypeCode : req.body.typeID);
    var agentChannelID = (req.body.channelID == '' ? user.agentGroup : req.body.channelID);
    var agentLevel = (req.body.typeID == '' ? user.agentLevel : 1);    
    res.setHeader('Content-Type', 'application/json');

    console.log(user.agentGroup);
    activityRepository.findCallLogReport(agentTypeID, agentChannelID, agentLevel, user.agentUsername , dtpickerFrom, dtpickerTo)
        .then(function (data) {

            var result = JSON.stringify(data);
            res.send(data);
        });

}

exports.InsertCallLog = function (req, res) {
    console.log('--- Insert Call Log ----- ');

    var timeunique = req.body.timeunique;
    var value = (req.body.value != "" ? req.body.value : 0);
    var user = req.session.user;

    if (value != 0) {
        res.setHeader('Content-Type', 'application/json');
        console.log(req.body);
        callLogRepository.insertCallLog(timeunique, value, user.agentUsername,
            function (err, data) {

                res.send(true);
            });

    }
},

    exports.detailData = function (req, res) {
        var user = req.session.user;
        var type = req.body.sel;
        var dtpickerFrom = helper.YMDToDisplayDMYMisun(req.body.dtpickerFrom);
        var dtpickerTo = helper.YMDToDisplayDMYMisun(req.body.dtpickerTo);
        var kioskID = req.body.kioskID;
        console.log(kioskID);

        res.setHeader('Content-Type', 'application/json');
        activityRepository.findDetsilDailyReport(user.agentTypeCode, user.agentGroup, user.agentLevel, dtpickerFrom, dtpickerTo, kioskID)
            .then(function (data) {
                var result = JSON.stringify(data);
                res.send(data);
            });

    }
exports.export = function (req, res) {

    console.log('----------------export------------------');
    res.setHeader('Content-Type', 'application/json;  charset=tis-620;');

    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;

    var user = req.session.user;
    var dataGroup;
    console.log(user);
    // Create a new instance of a Workbook class 
    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 

    var wsc = wb.addWorksheet('Call Log Report');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });

    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });

    var dataSummaryStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });

    var lstHeader = null;
    var lstChild = null;
    var ae = null;
    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);
    wsc.column(5).setWidth(30);

    var currentRow = 1;
    var sumKeepInKiosk = 0;
    var sumKeepOutKiosk = 0;
    var sumGrandTotal = 0;

    activityRepository.findExportCallLogReport(dtpickerFrom, dtpickerTo)
        .then(function (rawHeader) {
            console.log('----------------begin method------------------');
            wsc.cell(1, 1).string('Day').style(headerBoxStyle);
            wsc.cell(1, 2).string('Dealer_id').style(headerBoxStyle);
            wsc.cell(1, 3).string('Shop Name').style(headerBoxStyle);
            wsc.cell(1, 4).string('Money_inbox').style(headerBoxStyle);
            wsc.cell(1, 5).string('Call Log').style(headerBoxStyle);


            for (i = 0; i < Object.keys(rawHeader).length; i++) {
                currentRow = currentRow + 1;
                wsc.cell(currentRow, 1).string(rawHeader[i].time_stamp.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 2).string(rawHeader[i].dealer_code.toString()).style(dataSummaryStyle);
                wsc.cell(currentRow, 3).string(rawHeader[i].shop_name.toString()).style(dataSummaryStyle);
                wsc.cell(currentRow, 4).string(parseInt(rawHeader[i].money_inbox).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")).style(dataSummaryStyle);
                wsc.cell(currentRow, 5).string(rawHeader[i].callLog.toString()).style(dataSummaryStyle);


            }

            wb.write('Call_Log_Report.xlsx', res);
            console.log('----------------Finish------------------');

        });
}


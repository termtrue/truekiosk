var tranRepository = require('../data/transectionLogRepo');
var userKioskRepository = require('../data/userKioskViewRepo');
var serviceRepository = require('../data/serviceProfileRepo');
var xl = require('excel4node');

exports.render = function (req, res) {
    var user = req.session.user;
    var month = new Array();
    month[0] = "01";
    month[1] = "02";
    month[2] = "03";
    month[3] = "04";
    month[4] = "05";
    month[5] = "06";
    month[6] = "07";
    month[7] = "08";
    month[8] = "09";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";

    var makeDate = new Date();
    makeDate.setYear(makeDate.getFullYear() + 543);
    var CurDate = new Date(makeDate.setMonth(makeDate.getMonth()));
    var PreviousDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));

    var To = CurDate.getDate() + '/' + month[CurDate.getMonth()] + '/' + CurDate.getFullYear();
    var From = PreviousDate.getDate() + '/' + month[PreviousDate.getMonth()] + '/' + PreviousDate.getFullYear();


    userKioskRepository.findUserKiosk(user.agentUsername, user.agentTypeCode, user.agentGroup, user.agentLevel)
        .then(function (lstKiosk) {

            serviceRepository.findServiceMaster()
                .then(function (lstService) {

                    res.render('report_03', {
                        username: user.agentFullName,
                        isSearch: 0,
                        serialNumber: "",
                        phone: "",
                        ref1: "",
                        EWallet: "",
                        proGroup: "",
                        from: From,
                        to: To,
                        date: From,
                        lv: user.agentLevel,
                        userSelect: user.agentLevel,
                        txtKioskID: '',
                        lstService: lstService,
                        lstKiosk: lstKiosk,
                        lstProGroup: ['- Select Product Group -', 'Top-Up', 'Bill Pay', 'Topping', 'Wallet Top-Up'],
                        lstErrCode: ['- Select Error Code -', '0', '25', 'Cancel', '14', '7004', '3001', '9000', '192002', '182557', '7003', '182090', '2063', '171056', '4001', '202', '183252', '23', '800007', '2002', '2005', '7001', '192000', '7002', '99', '191020', '19', '1004'],
                        lstErrMsg: ['- Select Error Message -', 'Success', 'failed_by_kiosk', 'Cancel_by_kiosk', 'Fail_by_kiosk', 'Fail_by_TMX'],
                        selectProGroup: 0,
                        selectErrCode: 0,
                        selectErrMsg: 0
                    });
                });
        });
}
exports.search = function (req, res) {
    console.log(req.body);
    console.log('SEARCH');

    var serialNumber = req.body.serialNumber;
    var phone = req.body.phone;
    var ref1 = req.body.ref1;
    var EWallet = req.body.EWallet;
    var proGroup = req.body.proGroup;
    var errCode = req.body.errCode;
    var errMsg = req.body.errMsg;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var user = req.session.user;


    userKioskRepository.findUserKiosk(user.agentUsername, user.agentTypeCode, user.agentGroup, user.agentLevel)
        .then(function (lstKiosk) {

            serviceRepository.findServiceMaster()
                .then(function (lstService) {

                    res.render('report_03', {
                        username: user.agentFullName,
                        isSearch: 1,
                        serialNumber: serialNumber,
                        phone: phone,
                        ref1: ref1,
                        EWallet: EWallet,
                        proGroup: proGroup,
                        from: dtpickerFrom,
                        to: dtpickerTo,
                        date: null,
                        lv: user.agentLevel,
                        userSelect: user.agentLevel,
                        txtKioskID: '',
                        lstService: lstService,
                        lstKiosk: lstKiosk,
                        lstErrCode: ['- Select Error Code -', '0', '25', 'Cancel', '14', '7004', '3001', '9000', '192002', '182557', '7003', '182090', '2063', '171056', '4001', '202', '183252', '23', '800007', '2002', '2005', '7001', '192000', '7002', '99', '191020', '19', '1004'],
                        lstErrMsg: ['- Select Error Message -', 'Success', 'failed_by_kiosk', 'Cancel_by_kiosk', 'Fail_by_kiosk', 'Fail_by_TMX'],
                        selectProGroup: proGroup,
                        selectErrCode: errCode,
                        selectErrMsg: errMsg
                    });

                });
        });
}

exports.initialData = function (req, res) {

    var serialNumber = req.body.serialNumber;
    var phone = req.body.phone;
    var ref1 = req.body.ref1;
    var ewallet = req.body.ewallet;
    var progroup = req.body.progroup;
    var errcode = req.body.errcode;
    var errmsg = req.body.errmsg;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var user = req.session.user;

    console.log(req.body);
    res.setHeader('Content-Type', 'application/json');
    console.log('---------- Table -----------')
    tranRepository.findAllByCondition(serialNumber, phone, ref1, ewallet, progroup, errcode, errmsg, dtpickerFrom, dtpickerTo, user.agentTypeCode, user.agentGroup, user.agentLevel)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.DataDetail = function (req, res) {

    var time_kiosk_id_unique = req.body.time_kiosk_id_unique;
    console.log(time_kiosk_id_unique);
    res.setHeader('Content-Type', 'application/json');
    tranRepository.findAllByUnique(time_kiosk_id_unique)
        .then(function (data) {
            var result = JSON.stringify(data);
            res.send(data);
        });
}
exports.export = function (req, res) {


    res.setHeader('Content-Type', 'application/json');
    var serialNumber = req.body.serialNumber;
    var phone = req.body.phone;
    var ref1 = req.body.ref1;
    var ewallet = req.body.EWallet;
    var progroup = req.body.proGroup;
    var errcode = req.body.errCode;
    var errmsg = req.body.errMsg;
    var dtpickerFrom = req.body.dtpickerFrom;
    var dtpickerTo = req.body.dtpickerTo;
    var user = req.session.user;



    var wb = new xl.Workbook({
        defaultFont: {
            size: 12,
            name: 'TH Sarabun New',
            color: '#000000'
        },
        dateFormat: 'm/d/yy hh:mm:ss'
    });

    // Add Worksheets to the workbook 

    var wsc = wb.addWorksheet('Transaction Report');

    // Create a reusable style 
    var headerBoxStyle = wb.createStyle({
        font: {
            color: '#FFFFFF',
            size: 12,
            bold: true,
            background: 'black'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.
            fgColor: '24292E', // you can add two extra characters to serve as alpha, i.e. '2172d7aa'.
            // bgColor: 'ffffff' // bgColor only applies on patternTypes other than solid.
        }
    });
    var boxStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }
    });

    var boldFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        }
    });
    var boldSmallFontStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 10,

        }
    });
    var boldFontStylewithTopBotBorder = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12,

        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }
    });
    var dataHeaderStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        }
    });
    var dataMoneyStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {
            left: {
                style: 'medium', //?18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#24292E' // HTML style hex value
            },
            right: {
                style: 'medium',
                color: '#24292E'
            },
            top: {
                style: 'medium',
                color: '#24292E'
            },
            bottom: {
                style: 'medium',
                color: '#24292E'
            },
        }

    });

    var dataSummaryStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataDetailStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
        border: {

            top: {
                style: 'thin',
                color: '#24292E'
            },
            bottom: {
                style: 'thin',
                color: '#24292E'
            },
        }

    });
    var dataTotalStyle = wb.createStyle({
        font: {
            color: '#24292E',
            bold: true,
            size: 12
        },
        fill: {
            type: 'pattern', // the only one implemented so far.
            patternType: 'solid', // most common.         
            bgColor: 'bfd7ff', // bgColor only applies on patternTypes other than solid.
            fgColor: 'bfd7ff' // HTML style hex value. required.
        },
        alignment: { // ?18.8.1
            horizontal: 'right'
        },
    });

    var lstHeader = null;
    var lstChild = null;
    var ae = null;
    //Header 
    wsc.column(1).setWidth(40);
    wsc.column(2).setWidth(25);
    wsc.column(3).setWidth(20);
    wsc.column(4).setWidth(30);
    wsc.column(5).setWidth(40);
    wsc.column(6).setWidth(25);
    wsc.column(7).setWidth(20);
    wsc.column(8).setWidth(30);
    wsc.column(9).setWidth(40);
    wsc.column(10).setWidth(25);
    wsc.column(11).setWidth(20);
    wsc.column(12).setWidth(30);
    wsc.column(13).setWidth(40);
    wsc.column(14).setWidth(25);
    wsc.column(15).setWidth(20);
    wsc.column(16).setWidth(30);
    wsc.column(17).setWidth(40);
    wsc.column(18).setWidth(25);
    wsc.column(19).setWidth(20);
    wsc.column(20).setWidth(30);
    wsc.column(21).setWidth(40);
    wsc.column(22).setWidth(25);
    wsc.column(23).setWidth(20);
    wsc.column(24).setWidth(30);
    wsc.column(25).setWidth(40);
    wsc.column(26).setWidth(25);
    wsc.column(27).setWidth(20);
    wsc.column(28).setWidth(30);
    wsc.column(29).setWidth(40);
    wsc.column(30).setWidth(25);


    var currentRow = 1;
    var sumKeepInKiosk = 0;
    var sumKeepOutKiosk = 0;
    var sumGrandTotal = 0;

    tranRepository.findAllByCondition(serialNumber, phone, ref1, ewallet, progroup, errcode, errmsg, dtpickerFrom, dtpickerTo, user.agentTypeCode, user.agentGroup, user.agentLevel)
        .then(function (info) {
            console.log('----------------begin method------------------');
            wsc.cell(1, 1).string('time_kiosk_id_unique').style(headerBoxStyle);
            wsc.cell(1, 2).string('true_wallet_id').style(headerBoxStyle);
            wsc.cell(1, 3).string('serial_number').style(headerBoxStyle);
            wsc.cell(1, 4).string('transaction_id').style(headerBoxStyle);

            wsc.cell(1, 5).string('time_stamp').style(headerBoxStyle);
            wsc.cell(1, 6).string('phone_no').style(headerBoxStyle);
            wsc.cell(1, 7).string('ref1').style(headerBoxStyle);
            wsc.cell(1, 8).string('product_group').style(headerBoxStyle);
            wsc.cell(1, 9).string('product_name').style(headerBoxStyle);
            wsc.cell(1, 10).string('select_amount').style(headerBoxStyle);
            wsc.cell(1, 11).string('fee').style(headerBoxStyle);
            wsc.cell(1, 12).string('credit_customer').style(headerBoxStyle);
            wsc.cell(1, 13).string('insert_amount').style(headerBoxStyle);
            wsc.cell(1, 14).string('result').style(headerBoxStyle);
            wsc.cell(1, 15).string('COIN_10').style(headerBoxStyle);
            wsc.cell(1, 16).string('COIN_5').style(headerBoxStyle);
            wsc.cell(1, 17).string('COIN_2').style(headerBoxStyle);
            wsc.cell(1, 18).string('COIN_1').style(headerBoxStyle);
            wsc.cell(1, 19).string('BANK_1000').style(headerBoxStyle);
            wsc.cell(1, 20).string('BANK_500').style(headerBoxStyle);
            wsc.cell(1, 21).string('BANK_100').style(headerBoxStyle);
            wsc.cell(1, 22).string('BANK_50').style(headerBoxStyle);
            wsc.cell(1, 23).string('BANK_20').style(headerBoxStyle);
            wsc.cell(1, 24).string('havest').style(headerBoxStyle);
            //wsc.cell(1, 25).string('openkiosk').style(headerBoxStyle);
            //wsc.cell(1, 26).string('returnOpenkiosk').style(headerBoxStyle);
            wsc.cell(1, 25).string('dealer_code').style(headerBoxStyle);
            wsc.cell(1, 26).string('shop_name').style(headerBoxStyle);
            //wsc.cell(1, 29).string('day').style(headerBoxStyle);
            //wsc.cell(1, 30).string('time').style(headerBoxStyle);

            for (i = 0; i < Object.keys(info).length; i++) {
                currentRow = currentRow + 1;
                wsc.cell(currentRow, 1).string(info[i].time_kiosk_id_unique.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 2).string(info[i].true_wallet_id.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 3).string(info[i].serial_number.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 4).string(info[i].transection_id.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 5).string(info[i].time_stamp.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 6).string(info[i].phone_no.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 7).string(info[i].ref1.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 8).string(info[i].product_group.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 9).string(info[i].product_name.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 10).string(info[i].select_amount.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 11).string(info[i].fee.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 12).string(info[i].credit_customer.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 13).string(info[i].insert_amount.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 14).string(info[i].result.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 15).string(info[i].COIN_10.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 16).string(info[i].COIN_5.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 17).string(info[i].COIN_2.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 18).string(info[i].COIN_1.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 19).string(info[i].BANK_1000.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 20).string(info[i].BANK_500.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 21).string(info[i].BANK_100.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 22).string(info[i].BANK_50.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 23).string(info[i].BANK_20.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 24).string(info[i].harvest.toString()).style(boldFontStylewithTopBotBorder);

                //wsc.cell(currentRow, 1).string(info[i].openkiosk.toString()).style(boldFontStylewithTopBotBorder);
                //wsc.cell(currentRow, 1).string(info[i].returnOpenKiosk.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 25).string(info[i].dealer_code.toString()).style(boldFontStylewithTopBotBorder);
                wsc.cell(currentRow, 26).string(info[i].shop_name.toString()).style(boldFontStylewithTopBotBorder);
                //wsc.cell(currentRow, 1).string(info[i].day.toString()).style(boldFontStylewithTopBotBorder);
                //wsc.cell(currentRow, 1).string(info[i].time.toString()).style(boldFontStylewithTopBotBorder);

            }
            wb.write('Transaction Log.xlsx', res);
            console.log('----------------Finish------------------');
        });


}
﻿var helper = require('../helper/utility.helper');
var profileRepo = require('../data/agentUserRepo');

exports.render = function (req, res) {

    var user = req.session.user;
    profileRepo.findByagentUserID(user.agentUserID)
        .then(function (profile) {
            console.log(profile);
            res.render('profile/', { userProfile: profile, username: req.session.user.agentUsername });
        });


}

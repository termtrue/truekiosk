﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var permissionRepo = {
    findLevelByID: function () {
        return sequelize.query("select * from kio_t_agentUser u join kio_t_agentUserLevel l on u.agentUserID = l.user_id",
            { type: sequelize.QueryTypes.SELECT })
    }
    
}

module.exports = permissionRepo;
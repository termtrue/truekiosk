﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var agentUserRep = {
    findAll: function () {
        return sequelize.query("select agentUsername,agentLastLogin,islock,IsDisable,agentType " +
            " , agentTypeCode, agentChannelCode, agentRegionCode, agentLoginFailCounted " +
            " , agentPersonalType, agentLevel, agentGroup,agentFullName " +
            " from kio_t_agentUser",
            { type: sequelize.QueryTypes.SELECT })
    },
    findByCondition: function (TYPE, GROUP, LEVEL) {
        return sequelize.query("select agentUsername,agentLastLogin,islock,IsDisable,agentType " +
            " , agentTypeCode, agentChannelCode, agentRegionCode, agentLoginFailCounted " +
            " , agentPersonalType, agentLevel, agentGroup ,IsLock,IsDisable,agentFullName" +
            " from kio_t_agentUser where 1=1" +
            (typeof TYPE === "undefined" ||TYPE == "" ? "" : " AND agentTypeCode = " + TYPE ) +
            (typeof GROUP === "undefined" ||GROUP == "" ? "" : " AND agentGroup = " + GROUP ) +
            (typeof LEVEL === "undefined" ||LEVEL == "" ? "" : " AND agentLevel = " + LEVEL )
            ,
            { type: sequelize.QueryTypes.SELECT })
    },
    findByagentUserID: function (ID) {
        return db.agent_profile.findOne({
            where: {
                agent_user_id: ID,
                agent_profile_status: 1
            }
        });
    },
    searchData: function () {
        return sequelize.query("select agentUsername,agentLastLogin,islock,IsDisable,agentType " +
            " , agentTypeCode, agentChannelCode, agentRegionCode, agentLoginFailCounted " +
            " , agentPersonalType, agentLevel, agentGroup ,agentFullName" +
            " from kio_t_agentUser",
            { type: sequelize.QueryTypes.SELECT })
    }
}
module.exports = agentUserRep;
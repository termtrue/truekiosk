var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var configRepo = {
    GetAgentType: function () {
        return sequelize.query("select id,agent_type_name,date_create,date_modified,agent_type_code " +          
            " from agent_type",
            { type: sequelize.QueryTypes.SELECT })
    },
    GetAgentChannel: function () {
        return sequelize.query("select id,agent_channel_name,agent_code,date_create,date_modified " +
            " from agent_channel",
            { type: sequelize.QueryTypes.SELECT })
    },
    GetAgentLevel: function () {
        return sequelize.query("select configName,configValue " +
            " from kio_t_config where type = 'm001'",
            { type: sequelize.QueryTypes.SELECT })
    }
   
}

module.exports = configRepo;
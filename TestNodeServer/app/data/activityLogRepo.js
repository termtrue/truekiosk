var db = require('../models');
var dbPool = require('../helper/utility.connection');
var dbMysql = require('../helper/utility.mysql');
var moment = require('moment');
var sequelize = dbPool.getConnection();


var activityLogRepo = {
    // PAGE: DAILY REPORT
    findSummaryDailyReport_02_v2: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, USER, callback) {
        var sql = " select shop_name, serialNum, concat(DATE_FORMAT(date,'%d-%m-'),DATE_FORMAT(ADDDATE(date, INTERVAL 543 YEAR),'%Y'),DATE_FORMAT(date,' %T')) date,sum(money_inbox) money_inbox,sum(daily_money) daily_money,sum(GrandTotal) GrandTotal from ( select case when serial_number is not null then serial_number else serial_numberb end serialNum , " +
            " CASE WHEN shop_nameb IS NOT NULL THEN	shop_nameb ELSE	shop_namea	END shop_name, case when datea is not null then datea else dateb end date , case when datea is not null then money_inbox else 0 end money_inbox , " +
            " case when dateb is not null then daily_money else 0 end daily_money, (IFNULL(money_inbox, 0) + IFNULL(daily_money, 0)) GrandTotal  " +
            " from(select * from( " +
            " SELECT  sum(sl.sum_insert_amount) money_inbox, sl.todt, Date(sl.todt) datea, sl.serial_number,ukv.shop_name shop_namea FROM userKioskDetailView ukv  " +
            " join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE+
            " and ukv.agentGroup = " + GROUP+
            " and ukv.agentLevel = " + LEVEL+
            (LEVEL == 2 ? " and ukv.agentusername = " + USER : "") +
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a left join ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, Date(tl.time_stamp) dateb , tl.serial_number serial_numberb, ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            " and ukv.agentGroup = " + GROUP +
            " and ukv.agentLevel = " + LEVEL +
            (LEVEL == 2 ? " and ukv.agentusername = " + USER : "") +
            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) ,IFNULL(( SELECT	max(l.todt)	FROM kiosk_summarydaily_log l	" +
            " where l.serial_number = tl.serial_number AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')" +
            " ORDER BY	l.todt DESC	LIMIT 1),STR_TO_DATE('01/01/2017', '%d/%m/%Y')))"+
            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY tl.serial_number, date(tl.time_stamp))b on a.serial_number = b.serial_numberb and a.datea = b.dateb " +

            " UNION " +
            " select * from( " +
            " SELECT sum(sl.sum_insert_amount) money_inbox, sl.todt, Date(sl.todt) datea, sl.serial_number ,ukv.shop_name shop_namea" +
            " FROM userKioskDetailView ukv join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number  " +
            " and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            " and ukv.agentGroup = " + GROUP +
            " and ukv.agentLevel = " + LEVEL +
            (LEVEL == 2 ? " and ukv.agentusername = " + USER : "") +
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a RIGHT JOIN  ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, Date(tl.time_stamp) dateb , tl.serial_number serial_numberb,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            " and ukv.agentGroup = " + GROUP +
            " and ukv.agentLevel = " + LEVEL +
            (LEVEL == 2 ? " and ukv.agentusername = " + USER : "") +
            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) ,IFNULL(( SELECT	max(l.todt)	FROM kiosk_summarydaily_log l	" +
            " where l.serial_number = tl.serial_number AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')" +
            " ORDER BY	l.todt DESC	LIMIT 1),STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +
            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY tl.serial_number, date(tl.time_stamp) )b on a.serial_number = b.serial_numberb and a.datea = b.dateb ) t  )c group by serialNum order by shop_name";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            //if (typeof callback === "function") {
            //    // Call it, since we have confirmed it is callable?
                callback(err, data);
            //}

            
        });
    },
    findDetsilDailyReport_02_v2: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, KIOSK_ID, USER, callback) {
        var sql = " select case when shop_name is not null then shop_name else shop_nameb end shop_name , case when serial_number is not null then serial_number else serial_numberb end serialNum , " +
            "  case when datea is not null then datea else dateb end date , case when datea is not null then money_inbox else 0 end money_inbox , " +
            " case when dateb is not null then daily_money else 0 end daily_money, (IFNULL(money_inbox, 0) + IFNULL(daily_money, 0)) GrandTotal  " +
            " from(select * from( " +
            " SELECT  sum(sl.sum_insert_amount) money_inbox, sl.todt, concat(DATE_FORMAT(sl.todt, '%d-%m-'), DATE_FORMAT(ADDDATE(sl.todt, INTERVAL 543 YEAR ), '%Y'), DATE_FORMAT(sl.todt, ' %T')) datea, sl.serial_number, ukv.shop_name FROM userKioskDetailView ukv  " +
            " join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"' ") +
            " and ukv.host_name = '"+KIOSK_ID+"' "+
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a left join ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, concat(DATE_FORMAT(tl.time_stamp,'%d-%m-%Y'),' 23:59:59') dateb,  tl.serial_number serial_numberb ,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"'") +
            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +

            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number "+
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 "+
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +

            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') and ukv.host_name = '"+KIOSK_ID+"'" +
            " GROUP BY tl.serial_number, date(tl.time_stamp))b on a.serial_number = b.serial_numberb and a.datea = b.dateb " +
            
            " UNION " +
            " select * from( " +
            " SELECT sum(sl.sum_insert_amount) money_inbox, sl.todt, concat(DATE_FORMAT(sl.todt, '%d-%m-'), DATE_FORMAT(ADDDATE(sl.todt, INTERVAL 543 YEAR ), '%Y'), DATE_FORMAT(sl.todt, ' %T')) datea, sl.serial_number ,ukv.shop_name" +
            " FROM userKioskDetailView ukv join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number  " +
            " and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"'") +
            " and ukv.host_name = '" + KIOSK_ID + "' " +
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a RIGHT JOIN  ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, concat(DATE_FORMAT(tl.time_stamp,'%d-%m-%Y'),' 23:59:59') dateb,  tl.serial_number serial_numberb ,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"'") +
            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +

            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number " +
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 " +
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +

            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')  and ukv.host_name = '"+KIOSK_ID+"' "+
            " GROUP BY tl.serial_number, date(tl.time_stamp) )b on a.serial_number = b.serial_numberb and a.datea = b.dateb ) t   ";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            //if (typeof callback === "function") {
            //    // Call it, since we have confirmed it is callable?
            callback(err, data);
            //}
        });
    },

    findExportSummaryDailyReport_02_v2: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, USER,callback) {

        var ayear = parseInt(DATE_FROM.substr(6, 4)) - 543;
        DATE_FROM = DATE_FROM.substring(0, 6) + ayear;


        var byear = parseInt(DATE_TO.substr(6, 4)) - 543;
        DATE_TO = DATE_TO.substring(0, 6) + byear;

        var sql = " select shop_name, serialNum,date,sum(money_inbox) money_inbox,sum(daily_money) daily_money,sum(GrandTotal) GrandTotal from ( "+
            " select case when serial_number is not null then serial_number else serial_numberb end serialNum , " +
            " case when shop_name is not null then shop_name else shop_nameb end shop_name, case when datea is not null then datea else dateb end date , case when datea is not null then money_inbox else 0 end money_inbox , " +
            " case when dateb is not null then daily_money else 0 end daily_money, (IFNULL(money_inbox, 0) + IFNULL(daily_money, 0)) GrandTotal  " +
            " from(select * from( " +
            " SELECT  sum(sl.sum_insert_amount) money_inbox, sl.todt, Date(sl.todt) datea, sl.serial_number,ukv.shop_name FROM userKioskDetailView ukv  " +
            " join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '"+USER+"' ") +
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            // part2
            " ) a left join ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, Date(tl.time_stamp) dateb , tl.serial_number serial_numberb ,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '"+USER+"' ") +
            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +            
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +
            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number " +
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 " +
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +


            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY tl.serial_number, date(tl.time_stamp))b on a.serial_number = b.serial_numberb and a.datea = b.dateb " +
            " UNION " +

            " select * from( " +
            " SELECT sum(sl.sum_insert_amount) money_inbox, sl.todt, Date(sl.todt) datea, sl.serial_number ,ukv.shop_name" +
            " FROM userKioskDetailView ukv join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number  " +
            " and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '"+USER+"' ") +
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a RIGHT JOIN  ( " +
            //part2
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, Date(tl.time_stamp) dateb , tl.serial_number serial_numberb ,ukv.shop_name shop_nameb" +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : " and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"' ") +

            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +
            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number " +
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 " +
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +

            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY tl.serial_number, date(tl.time_stamp) )b on a.serial_number = b.serial_numberb and a.datea = b.dateb ) t  )c group by serialNum order by shop_name";
        console.log('Export Header');
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            //if (typeof callback === "function") {
            //    // Call it, since we have confirmed it is callable?
            callback(err, data);
            //}
        });
    },
    findExportDailyReport_02_v2: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, USER, callback) {


        var ayear = parseInt(DATE_FROM.substr(6, 4)) - 543;
        DATE_FROM = DATE_FROM.substring(0, 6) + ayear;


        var byear = parseInt(DATE_TO.substr(6, 4)) - 543;
        DATE_TO = DATE_TO.substring(0, 6) + byear;


        var sql = " select case when shop_name is not null then shop_name else shop_nameb end shop_name , case when serial_number is not null then serial_number else serial_numberb end serialNum , " +
            " case when datea is not null then datea else dateb end date , case when datea is not null then money_inbox else 0 end money_inbox , " +
            " case when dateb is not null then daily_money else 0 end daily_money, (IFNULL(money_inbox, 0) + IFNULL(daily_money, 0)) GrandTotal  " +
            " from(select * from( " +
            " SELECT  sum(sl.sum_insert_amount) money_inbox, sl.todt, concat(DATE_FORMAT(sl.todt, '%d-%m-'), DATE_FORMAT(ADDDATE(sl.todt, INTERVAL 543 YEAR ), '%Y'), DATE_FORMAT(sl.todt, ' %T')) datea, sl.serial_number,ukv.shop_name FROM userKioskDetailView ukv  " +
            " join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = '" + USER +"' ") +
           
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a left join ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, concat(DATE_FORMAT(tl.time_stamp,'%d-%m-%Y'),' 23:59:59') dateb, tl.serial_number serial_numberb ,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername =  '" + USER +"' ") +

            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +
            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number " +
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 " +
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +

            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY tl.serial_number, date(tl.time_stamp))b on a.serial_number = b.serial_numberb and a.datea = b.dateb " +
            " UNION " +
            " select * from( " +
            " SELECT sum(sl.sum_insert_amount) money_inbox, sl.todt, concat(DATE_FORMAT(sl.todt, '%d-%m-'), DATE_FORMAT(ADDDATE(sl.todt, INTERVAL 543 YEAR ), '%Y'), DATE_FORMAT(sl.todt, ' %T')) datea, sl.serial_number ,ukv.shop_name" +
            " FROM userKioskDetailView ukv join kiosk_summarydaily_log sl on ukv.host_name = sl.serial_number  " +
            " and ukv.agentLevel = sl.agentlevel and ukv.agentGroup = sl.agentgroup " +
            " WHERE	1= 1 " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername =  '" + USER +"' ") +
           
            " and	sl.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') " +
            " AND date(sl.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " group by todt ORDER BY ukv.host_name, sl.todt " +
            " ) a RIGHT JOIN  ( " +
            " select sum(tl.insert_amount) daily_money , tl.time_stamp, concat(DATE_FORMAT(tl.time_stamp,'%d-%m-%Y'),' 23:59:59') dateb, tl.serial_number serial_numberb ,ukv.shop_name shop_nameb " +
            " from kiosk_transaction_log tl join userKioskDetailView ukv on tl.serial_number = ukv.host_name " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername =  '" + USER +"' ") +

            " where tl.time_stamp > IFNULL(( " +
            " select l.todt from kiosk_summarydaily_log l  " +
            " WHERE  (l.todt > STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') AND date(l.todt) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'))  " +
            " AND l.serial_number = tl.serial_number	order by l.todt desc limit 1 ) " +
            ",IFNULL(( SELECT  max(l.todt)  FROM kiosk_summarydaily_log l WHERE l.serial_number = tl.serial_number " +
            " AND date(l.todt) <= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  ORDER BY l.todt DESC  LIMIT 1 " +
            " ), STR_TO_DATE('01/01/2017', '%d/%m/%Y')))" +

            " AND tl.serial_number = ukv.host_name and date(tl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')  " +
            " GROUP BY tl.serial_number, date(tl.time_stamp) )b on a.serial_number = b.serial_numberb and a.datea = b.dateb ) t  ";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            //if (typeof callback === "function") {
            //    // Call it, since we have confirmed it is callable?
            callback(err, data);
            //}
        });
    },

    findSummaryDailyReport_02: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, USER) {
        return sequelize.query(" select ukv.dealer_name, ukv.host_name, IFNULL(a.Money_Inbox, 0) money_inbox, IFNULL(b.daily_money,0) daily_money , IFNULL(a.Money_Inbox, 0) + IFNULL(b.daily_money, 0) GrandTotal from ( " +
            " SELECT	l.serial_number serial_numbera , l.time_stamp time_stampa, ukv.dealer_name, IFNULL(SUM(l.money_inbox), 0) Money_Inbox, 0 daily_money " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND money_inbox != 0 " +
            " group by l.serial_number) a " +
            " RIGHT JOIN userKioskView ukv ON ukv.host_name = a.serial_numbera " +
            " left join (SELECT 	n.serial_number, DATE(n.time_stamp), 'dealer_name' dealer_name, 0 money_inbox, sum(n.insert_amount) daily_money " +
            " FROM kiosk_transaction_log n " +
            " JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE (n.time_stamp) > IFNULL(( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1 " +
            " ) IS NULL THEN( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')	 " +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1 " +
            " )		ELSE	( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1	) END  " +
            " ), '2000-01-01') AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') group by n.serial_number " +
            " ) b on ukv.host_name = b.serial_number WHERE " +
            "  ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " GROUP BY	serial_number, ukv.dealer_name " +
            " ORDER BY	serial_number",
            { replacements: { type: TYPE, group: GROUP, level: LEVEL, date_from: DATE_FROM, date_to: DATE_TO, username: USER }, type: sequelize.QueryTypes.SELECT });
    },
    findDetsilDailyReport_02: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, KIOSK_ID, USER) {
        return sequelize.query(
            " select case when c.time_stampa is not null then DATE_FORMAT(c.time_stampa,'%d/%m/%Y')  else DATE_FORMAT(c.time_stamp,'%d/%m/%Y') end time_stamp , " +
            " case when c.Money_Inboxa is not null then IFNULL(c.Money_Inboxa,0) else IFNULL(c.Money_Inbox, 0) end Money_Inbox , " +
            " case when c.daily_moneya > 0 then IFNULL(c.daily_moneya, 0) else IFNULL(c.daily_money, 0) end daily_money, " +
            " IFNULL(c.Money_Inboxa,0) + IFNULL(c.daily_money, 0)  GrandTotal from 	( " +
            " select * from( SELECT	l.serial_number serial_numbera, DATE(l.time_stamp) time_stampa, IFNULL(SUM(l.money_inbox), 0) Money_Inboxa, 0 daily_moneya " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND money_inbox != 0 " +
            " AND ukv.host_name = '" + KIOSK_ID + "' " +
            " GROUP BY l.serial_number, l.time_stamp) a  " +
            " left join ( " +
            " SELECT	n.serial_number, DATE(n.time_stamp) time_stamp, 0 money_inbox, sum(IFNULL(n.insert_amount, 0)) daily_money " +
            " FROM	kiosk_transaction_log n	JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(n.time_stamp) > ( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO' OR sl.`mode` = 'MANUAL') " +
            "	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1	) IS NULL  " +
            " THEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1		AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY time_stamp DESC LIMIT 1 " +
            " )	ELSE		( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM kiosk_activity_log sl " +
            " WHERE		1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1 " +
            " )	END " +
            " ) " +
            " AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')AND n.serial_number = '" + KIOSK_ID + "' " +
            " GROUP BY n.serial_number, DATE(n.time_stamp)) b on a.serial_numbera = b.serial_number and a.time_stampa = b.time_stamp " +
            " union " +
            " select * from( " +
            " SELECT	l.serial_number serial_numbera, DATE(l.time_stamp) time_stampa, IFNULL(SUM(l.money_inbox), 0) Money_Inboxa, 0 daily_moneya " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')	AND date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND money_inbox != 0 " +
            " AND ukv.host_name = '" + KIOSK_ID + "' " +
            " GROUP BY l.serial_number, l.time_stamp) a  " +
            " right join ( " +
            " SELECT	n.serial_number, DATE(n.time_stamp) time_stamp, 0 money_inbox, sum(IFNULL(n.insert_amount, 0)) daily_money " +
            " FROM	kiosk_transaction_log n	JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(n.time_stamp) > ( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')AND date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND (sl.`mode` = 'AUTO' OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1	) IS NULL  " +
            " THEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1		AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 		AND sl.serial_number = n.serial_number " +
            " ORDER BY time_stamp DESC LIMIT 1 " +
            " )	ELSE		( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM kiosk_activity_log sl " +
            " WHERE		1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')	AND date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1 " +
            " )	END " +
            " ) " +
            " AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')AND n.serial_number = '" + KIOSK_ID + "' " +
            " GROUP BY n.serial_number, DATE(n.time_stamp) ) b on a.serial_numbera = b.serial_number and a.time_stampa = b.time_stamp )c ",

            { replacements: { type: TYPE, group: GROUP, level: LEVEL, date_from: DATE_FROM, date_to: DATE_TO, username: USER }, type: sequelize.QueryTypes.SELECT });
    },

    findExportSummaryDailyReport_02: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, USER) {

        var ayear = parseInt(DATE_FROM.substr(6, 4)) - 543;
        DATE_FROM = DATE_FROM.substring(0, 6) + ayear;


        var byear = parseInt(DATE_TO.substr(6, 4)) - 543;
        DATE_TO = DATE_TO.substring(0, 6) + byear;

        return sequelize.query(" select ukv.dealer_name, ukv.host_name serial_number, IFNULL(a.Money_Inbox, 0) money_inbox,  IFNULL(b.daily_money,0) daily_money  , IFNULL(a.Money_Inbox, 0) + IFNULL(b.daily_money, 0) GrandTotal from ( " +
            " SELECT	l.serial_number serial_numbera , l.time_stamp time_stampa, ukv.dealer_name, IFNULL(SUM(l.Money_Inbox), 0) money_inbox, 0 daily_money " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND money_inbox != 0 " +
            " group by l.serial_number) a " +
            " RIGHT JOIN userKioskView ukv ON ukv.host_name = a.serial_numbera " +
            " left join (SELECT 	n.serial_number, DATE(n.time_stamp), 'dealer_name' dealer_name, 0 money_inbox, sum(n.insert_amount) daily_money " +
            " FROM kiosk_transaction_log n " +
            " JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE (n.time_stamp) > IFNULL(( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1 " +
            " ) IS NULL THEN( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')	 " +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1 " +
            " )		ELSE	( " +
            " SELECT		sl.time_stamp maxDate	FROM		kiosk_activity_log sl " +
            " WHERE		1= 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'		OR sl.`mode` = 'MANUAL')		and sl.serial_number = n.serial_number  " +
            " ORDER BY		time_stamp DESC	LIMIT 1	) END  " +
            " ), '2000-01-01') AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') group by n.serial_number " +
            " ) b on ukv.host_name = b.serial_number WHERE " +
            "  ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " GROUP BY	serial_number, ukv.dealer_name " +
            " ORDER BY	serial_number",
            { replacements: { type: TYPE, group: GROUP, level: LEVEL, date_from: DATE_FROM, date_to: DATE_TO, username: USER }, type: sequelize.QueryTypes.SELECT });
    },
    findExportDailyReport_02: function (TYPE, GROUP, LEVEL, DATE_FROM, DATE_TO, KIOSK_ID, USER) {

        var ayear = parseInt(DATE_FROM.substr(6, 4)) - 543;
        DATE_FROM = DATE_FROM.substring(0, 6) + ayear;


        var byear = parseInt(DATE_TO.substr(6, 4)) - 543;
        DATE_TO = DATE_TO.substring(0, 6) + byear;

        return sequelize.query(
            " select CASE WHEN c.serial_numbera IS NOT NULL THEN	c.serial_numbera ELSE	c.serial_number END serial_number, " +
            " CASE WHEN c.time_stampa IS NOT NULL THEN	c.time_stampa ELSE c.time_stamp END time," +
            " case when c.time_stampa is not null then DATE_FORMAT(c.time_stampa,'%d/%m/%Y')  else DATE_FORMAT(c.time_stamp, '%d/%m/%Y') end time_stamp , " +
            " case when c.Money_Inboxa is not null then IFNULL(c.Money_Inboxa,0) else IFNULL(c.Money_Inbox, 0) end money_inbox , " +
            " case when c.daily_moneya > 0 then IFNULL(c.daily_moneya, 0) else IFNULL(c.daily_money, 0) end daily_money, " +
            " IFNULL(c.Money_Inboxa,0) + IFNULL(c.daily_money, 0)  GrandTotal from 	( " +
            " select * from( SELECT	l.serial_number serial_numbera, DATE(l.time_stamp) time_stampa, IFNULL(SUM(l.money_inbox), 0) Money_Inboxa, 0 daily_moneya " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND money_inbox != 0 " +

            " GROUP BY l.serial_number, l.time_stamp) a  " +
            " left join ( " +
            " SELECT	n.serial_number, DATE(n.time_stamp) time_stamp, 0 money_inbox, sum(IFNULL(n.insert_amount, 0)) daily_money " +
            " FROM	kiosk_transaction_log n	JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(n.time_stamp) > ( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO' OR sl.`mode` = 'MANUAL') " +
            "	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1	) IS NULL  " +
            " THEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1		AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY time_stamp DESC LIMIT 1 " +
            " )	ELSE		( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM kiosk_activity_log sl " +
            " WHERE		1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') )" +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1 " +
            " )	END " +
            " ) " +
            " AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY n.serial_number, DATE(n.time_stamp)) b on a.serial_numbera = b.serial_number and a.time_stampa = b.time_stamp " +
            " union " +
            " select * from( " +
            " SELECT	l.serial_number serial_numbera, DATE(l.time_stamp) time_stampa, IFNULL(SUM(l.money_inbox), 0) Money_Inboxa, 0 daily_moneya " +
            " FROM	kiosk_activity_log l " +
            " JOIN userKioskView ukv ON ukv.host_name = l.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(l.`mode` = 'MANUAL'	OR l.`mode` = 'AUTO') " +
            " AND (l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')	AND date(l.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND money_inbox != 0 " +

            " GROUP BY l.serial_number, l.time_stamp) a  " +
            " right join ( " +
            " SELECT	n.serial_number, DATE(n.time_stamp) time_stamp, 0 money_inbox, sum(IFNULL(n.insert_amount, 0)) daily_money " +
            " FROM	kiosk_transaction_log n	JOIN userKioskView ukv ON ukv.host_name = n.serial_number " +
            " and ukv.agentTypeCode = " + TYPE +
            (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = " + GROUP : "") +
            (LEVEL != 2 ? " and ukv.agentLevel = " + LEVEL + "" : "and ukv.agentLevel = " + LEVEL + " and ukv.agentusername = :username") +
            " WHERE	(n.time_stamp) > ( " +
            " SELECT	CASE	WHEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')AND date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND (sl.`mode` = 'AUTO' OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1	) IS NULL  " +
            " THEN ( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM	kiosk_activity_log sl " +
            " WHERE	1 = 1		AND sl.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY time_stamp DESC LIMIT 1 " +
            " )	ELSE		( " +
            " SELECT	sl.time_stamp maxDate " +
            " FROM kiosk_activity_log sl " +
            " WHERE		1 = 1 " +
            " AND (sl.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')	AND date(sl.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')) " +
            " AND (sl.`mode` = 'AUTO'	OR sl.`mode` = 'MANUAL') " +
            " 	AND sl.serial_number = n.serial_number " +
            " ORDER BY	time_stamp DESC	LIMIT 1 " +
            " )	END " +
            " ) " +
            " AND DATE(n.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " +
            " GROUP BY n.serial_number, DATE(n.time_stamp) ) b on a.serial_numbera = b.serial_number and a.time_stampa = b.time_stamp ) c ORDER BY serial_number,time ",

            { replacements: { type: TYPE, group: GROUP, level: LEVEL, date_from: DATE_FROM, date_to: DATE_TO, username: USER }, type: sequelize.QueryTypes.SELECT });
    },

    findCallLogReport: function (TYPE, GROUP, LEVEL, USER, DATE_FROM, DATE_TO) {
        return sequelize.query(

            " SELECT DATE_FORMAT(al.time_stamp,'%d/%m/%Y') time_stamp, time_stamp stamp, al.time_kiosk_id_unique time_unique, kv.dealer_code,	kv.shop_name,	al.money_inbox,	IFNULL(cl.callLogValue,0) callLog,  concat(DATE_FORMAT(createDate,'%d-%m-'),DATE_FORMAT(ADDDATE(createDate, INTERVAL 543 YEAR),'%Y'),DATE_FORMAT(createDate,' %T')) createDate, createBy  " +
            " FROM kiosk_activity_log al " +
            " left join (SELECT * FROM kiosk_call_log WHERE id IN (SELECT MAX(id) FROM kiosk_call_log GROUP BY `time_kiosk_id_unique`)) cl "+
            " on cl.time_kiosk_id_unique = al.time_kiosk_id_unique "+
            " JOIN userKioskView kv ON kv.host_name = al.serial_number " +
            " where kv.agentTypeCode = " + TYPE +
            " and kv.agentGroup = " + GROUP +            
            (LEVEL != 2 ? " and kv.agentLevel = " + LEVEL + "" : " and kv.agentLevel = " + LEVEL + " and kv.agentusername = '" + USER + "' ") +
            " and (al.`mode` = 'MANUAL' OR al.`mode` = 'AUTO') AND money_inbox != 0  " +
            " AND (al.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y') and date(al.time_stamp) <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y')  ) order by stamp ",

            { type: sequelize.QueryTypes.SELECT });

    },

    findExportCallLogReport: function (TYPE, GROUP, LEVEL,DATE_FROM, DATE_TO) {
        return sequelize.query(

            " SELECT DATE_FORMAT(al.time_stamp,'%d/%m/%Y') time_stamp, time_stamp stamp, kv.dealer_code,	kv.shop_name,	al.money_inbox,	IFNULL(cl.callLogValue,0) callLog, concat(DATE_FORMAT(createDate,'%d-%m-'),DATE_FORMAT(ADDDATE(createDate, INTERVAL 543 YEAR),'%Y'),DATE_FORMAT(createDate,' %T')) createDate, createBy  " +
            " FROM kiosk_activity_log al " +
            " left join (SELECT * FROM kiosk_call_log WHERE id IN (SELECT MAX(id) FROM kiosk_call_log GROUP BY `time_kiosk_id_unique`)) cl " +
            " on cl.time_kiosk_id_unique = al.time_kiosk_id_unique " +
            " JOIN userKioskView kv ON kv.host_name = al.serial_number " +
            " where kv.agentTypeCode = " + TYPE + " and kv.agentGroup = " + GROUP + " and kv.agentLevel = " + LEVEL + "  and (al.`mode` = 'MANUAL' OR al.`mode` = 'AUTO') AND money_inbox != 0  " +
            " AND (time_stamp >= DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d') and date(time_stamp) <= DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d') ) order by stamp"
            ,
            { type: sequelize.QueryTypes.SELECT });

    }
}

module.exports = activityLogRepo;
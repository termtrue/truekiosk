var db = require('../models');
var dbPool = require('../helper/utility.connection');
var dbMysql = require('../helper/utility.mysql');
var sequelize = dbPool.getConnection();


var dailySummaryRepo = {
    SelectSummaryByUser: function (username, datetime) {
        return sequelize.query(" select serialNumber, sum(transAmount) transAmount, " +
            " sum(moneyAmount) moneyAmount, sum(comission) comission, " +
            " sum(vat_of_com) vat_of_com, sum(withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(net_com) net_com " +
            " from kio_l_dailySummary " +
            " WHERE agentUsername = '" + username + "'  " +
            " and summaryDate = DATE_ADD(STR_TO_DATE('" + datetime + "', '%d/%m/%Y'), INTERVAL -543 YEAR) " +
            "", { type: sequelize.QueryTypes.SELECT })
    },

    SelectByUserWithNoGroup: function (username, datetime, dtpickerFrom, dtpickerTo, ddltype) {
        console.log(ddltype);


        return sequelize.query(" SELECT serial_number  serialNumber, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " sum(a.sum_insert_amountwithfee) moneyAmount, " +
            " sum(a.sum_com) comission, " +
            " sum(a.sum_vat_of_com) vat_of_com, " +
            " sum(a.sum_withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(a.sum_net_com) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentUsername = '" + username + "'  ", { type: sequelize.QueryTypes.SELECT })
    },



    // Report 05 Commission
    // -- for render / search 
    SelectSummaryByUserWithOption: function (type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {
        //var con = dbPool.getMysqlCon();
        var sql = " SELECT serial_number  serialNumber, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " ukv.shop_name aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " sum(a.sum_insert_amountwithfee) moneyAmount, " +
            " sum(a.sum_com) comission, " +
            " sum(a.sum_vat_of_com) vat_of_com, " +
            " sum(a.sum_withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(a.sum_net_com) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + ddlLv +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " order by ukv.shop_name";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;
            callback(err, data);
        });

    },
    // -- initialData
    SelectByUserWithOption: function (TYPE, LEVEL, GROUP, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT 	ukv.host_name serialNumber,                                                                                   " +
            " CONCAT(ukv.dealer_code,'_(',ukv.shop_name,')')dealerName,                                                               " +
            " ukv.shop_name aliasName,                                                                      " +
            " IFNULL(sum(a.count_trans), 0) transAmount,                                                                              " +
            " IFNULL(s.servicename, ' - ') serviceName,                                                                               " +
            " ukv.alias alias,                                                                                                        " +
            " IFNULL(sum(a.sum_insert_amountwithfee), 0) moneyAmount,                                                                        " +
            " IFNULL(sum(a.sum_com), 0) comission,                                                                                    " +
            " IFNULL(sum(a.sum_vat_of_com), 0) vat_of_com,                                                                            " +
            " IFNULL(sum(a.sum_withholding_tax_of_com), 0) withholding_tax_of_com,                                                    " +
            " IFNULL(sum(a.sum_net_com), 0) net_com                                                                                   " +
            " FROM userKioskView ukv                                                                                                  " +
            " LEFT JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name AND a.agentlevel = ukv.agentLevel             " +
            "  AND (todt) > concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            "  AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +

            " LEFT JOIN kio_m_service s ON s.serviceID = a.product_group " +
            "  where   (1=1) and ukv.agentTypeCode = " + TYPE +
            " and ukv.agentGroup = " + GROUP +
            " and ukv.agentLevel = " + ddlLv +
            (LEVEL == 2 ? " and ukv.agentuserid = " + id : "") +
            "   GROUP BY ukv.agentprofile_id order by ukv.shop_name";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });

    },
    // -- DetailData
    SelectByUserAndSerialNumWithOption: function (kioskID, type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT serial_number  serialNumber, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " sum(a.sum_insert_amountwithfee) moneyAmount, " +
            " sum(a.sum_com) comission, " +
            " sum(a.sum_vat_of_com) vat_of_com, " +
            " sum(a.sum_withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(a.sum_net_com) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + ddlLv +
            " and ukv.host_name = '" + kioskID + "' " +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY ukv.agentprofile_id , serviceName";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    // -- Export Detail
    SelectByUserAndSerialNumWithOptionNoGroupBy: function (type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT serial_number  serialNumber, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " ukv.shop_name aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " sum(a.sum_insert_amountwithfee) moneyAmount, " +
            " sum(a.sum_com) comission, " +
            " sum(a.sum_vat_of_com) vat_of_com, " +
            " sum(a.sum_withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(a.sum_net_com) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + ddlLv +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY ukv.agentprofile_id , serviceName order by ukv.shop_name";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    selectTranByConditionWithCom: function (type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT  t.* ,s.serviceName,c.*,a.* , CONCAT(kr.alias, kr.shop_name) shopName" +
            " from kio_t_agentUser auser  " +
            " join kioskview kr on auser.agentTypeCode = kr.agentTypeCode and auser.agentChannelCode = kr.agentChannelCode  " +
            " join kiosk_summarycomission_log a on a.serial_number = kr.kioskSerialNum   " +
            " join kiosk_transaction_log t on t.serial_number = a.serial_number " +
            " AND t.time_stamp > a.fromdt " +
            " AND t.time_stamp <= a.todt " +
            " and t.product_group = a.product_group " +
            " join kio_m_service s on t.product_group = s.serviceID " +
            " JOIN kiosk_commission_log c ON c.time_kiosk_id_unique = t.time_kiosk_id_unique and a.agentgroup = c.agentgroup and a.agentlevel = c.agentlevel " +
            " and a.agentLevel = " + lv +
            // user.agentUsername, dtpickerFrom, dtpickerTo
            " WHERE (todt) >= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 00:00:00')   " +
            " AND (todt) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +
            " and auser.agentTypeCode = '" + type + "' " +
            " and auser.agentLevel = " + lv +
            (lv == 0 ? "" : " and auser.agentGroup = " + group) +
            (lv == 2 ? " and auser.agentuserid = " + id : "") +
            " and t.transection_id != 0 " +
            " AND t.insert_amount > 0 " +
            " AND (t.result_msg = 'Success' OR (t.insert_amount >0 and t.result_tmx != 'Cancel')) ";

        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    selectTranByConditionWithComNew: function (type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT  t.* ,s.serviceName,c.*,a.* , CONCAT(kr.alias, kr.shop_name) shopName" +
            " from userKioskView kr " +
            " join kiosk_summarycomission_log a on a.serial_number = kr.host_name " +
            " join kiosk_transaction_log t on t.serial_number = a.serial_number " +
            " AND t.time_stamp > a.fromdt " +
            " AND t.time_stamp <= a.todt " +
            " and t.product_group = a.product_group " +
            " join kio_m_service s on t.product_group = s.serviceID " +
            " JOIN kiosk_commission_log c ON c.time_kiosk_id_unique = t.time_kiosk_id_unique " +
            " WHERE (todt) >= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 00:00:00')   " +
            " AND (todt) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +

            " and kr.agentTypeCode = '" + type + "' " +
            " and kr.agentLevel = " + ddlLv +
            (lv == 0 ? "" : " and kr.agentGroup = " + group) +
            (lv == 2 ? " and kr.agentuserid = " + id : "") +
            " and a.agentgroup = " + group +
            " and a.agentlevel = " + ddlLv +
            " and c.agentgroup = " + group +
            " and c.agentlevel = " + ddlLv +
            " and t.transection_id != 0 " +
            " AND t.insert_amount > 0 " +
            " and c.insertamountwithfee > 0 " +
            " AND (t.result_msg = 'Success' OR (t.insert_amount >0 and t.result_tmx != 'Cancel')) ";

        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    selectTranByCondition: function (type, lv, group, id, dtpickerFrom, dtpickerTo, ddlLv, callback) {

        var sql = " SELECT t.*,	s.serviceName,	CONCAT( kr.alias, kr.shop_name ) shopName " +
            " FROM userKioskView kr  " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = kr.host_name " +
            " JOIN kiosk_transaction_log t ON t.serial_number = a.serial_number" +
            " AND t.time_stamp > a.fromdt" +
            " AND t.time_stamp <= a.todt" +
            " AND t.product_group = a.product_group" +
            " JOIN kio_m_service s ON t.product_group = s.serviceID " +
            " WHERE (todt) >= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 00:00:00')   " +
            " AND (todt) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +
            " and kr.agentTypeCode = '" + type + "' " +
            " and kr.agentLevel = " + lv +
            (lv == 0 ? "" : " and kr.agentGroup = " + group) +
            (lv == 2 ? " and kr.agentuserid = " + id : "") +
            " and a.agentgroup = '" + group + "' " +
            " and a.agentLevel = " + lv +
            " and t.transection_id != 0 " +
            " AND t.insert_amount > 0 " +
            " AND (t.result_msg = 'Success' OR (t.insert_amount >0 and t.result_tmx != 'Cancel')) " +
            " UNION " +
            " SELECT t.*,	s.serviceName,	CONCAT( kr.alias, kr.shop_name ) shopName " +
            " FROM userKioskView kr  " +
            " JOIN kiosk_transaction_log t ON t.serial_number = kr.host_name" +
            " JOIN kio_m_service s ON t.product_group = s.serviceID " +
            " WHERE(t.time_stamp) >= (" +
            " SELECT a.fromdt" +
            " FROM kiosk_summarycomission_log a " +
            " WHERE (todt) >= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 00:00:00')   " +
            " AND (todt) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +
            " AND a.serial_number = kr.host_name " +
            " ORDER BY	fromdt DESC 	LIMIT 1 	) " +
            " AND (t.time_stamp) <= concat(DATE_FORMAT(DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL - 543 YEAR), '%Y-%m-%d'), ' 23:59:59')  " +
            " and kr.agentTypeCode = '" + type + "' " +
            " and kr.agentLevel = " + lv +
            (lv == 0 ? "" : " and kr.agentGroup = " + group) +
            (lv == 2 ? " and kr.agentuserid = " + id : "") +          
            " and t.transection_id != 0 " +
            " AND t.insert_amount > 0 " +
            " AND (t.result_msg = 'Success' OR (t.insert_amount >0 and t.result_tmx != 'Cancel')) ";

        console.log('-----------------------****-------------------------------');
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },


    SelectByGroupAndType: function (group, panelLv, lv, id, type, dtpickerFrom, dtpickerTo) {
        return sequelize.query(" SELECT serial_number  serialNumber, " +
            " ukv.agentChannelCode, " +
            " concat(ukv.agent_type_name,'(',	ukv.agent_channel_name,')') agentTypeChannel, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " IFNULL(sum(a.sum_insert_amountwithfee),0) moneyAmount, " +
            " IFNULL(sum(a.sum_com),0) comission, " +
            " IFNULL(sum(a.sum_vat_of_com),0) vat_of_com, " +
            " IFNULL(sum(a.sum_withholding_tax_of_com),0) withholding_tax_of_com, " +
            " IFNULL(sum(a.sum_net_com),0) net_com, " +
            " ukv.agentLevel level " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + panelLv +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") + " GROUP BY ukv.agentLevel "
            , { type: sequelize.QueryTypes.SELECT })

    },
    SelectByGroupAndTypeGroupTypeAndChanel: function (group, panelLv, lv, id, type, dtpickerFrom, dtpickerTo) {
        return sequelize.query(
            " SELECT serial_number  serialNumber, " +
            " ukv.agentChannelCode, " +
            " concat(ukv.agent_type_name,'(',	ukv.agent_channel_name,')') agentTypeChannel, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " IFNULL(sum(a.sum_insert_amountwithfee),0) moneyAmount, " +
            " IFNULL(sum(a.sum_com),0) comission, " +
            " IFNULL(sum(a.sum_vat_of_com),0) vat_of_com, " +
            " IFNULL(sum(a.sum_withholding_tax_of_com),0) withholding_tax_of_com, " +
            " IFNULL(sum(a.sum_net_com),0) net_com " +
            " FROM userKioskView ukv " +
            " LEFT JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            "  AND (todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            "  AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +

            " LEFT JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " where ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + panelLv +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY	ukv.agentTypeCode,ukv.agentChannelCode", { type: sequelize.QueryTypes.SELECT })

    },
    SelectByGroupAndTypeGroupTypeAndChanelWithDetail: function (group, panelLv, lv, id, type, channel, dtpickerFrom, dtpickerTo) {
        return sequelize.query(" SELECT serial_number  serialNumber, " +
            " ukv.agentChannelCode, " +
            " concat(ukv.agent_type_name,'(',	ukv.agent_channel_name,')') agentTypeChannel, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " IFNULL(sum(a.sum_insert_amountwithfee),0) moneyAmount, " +
            " IFNULL(sum(a.sum_com),0) comission, " +
            " IFNULL(sum(a.sum_vat_of_com),0) vat_of_com, " +
            " IFNULL(sum(a.sum_withholding_tax_of_com),0) withholding_tax_of_com, " +
            " IFNULL(sum(a.sum_net_com),0) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + panelLv +
            " and ukv.agentChannelCode = " + channel +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY a.product_group,ukv.agentTypeCode,ukv.agentChannelCode", { type: sequelize.QueryTypes.SELECT })

    },
    SelectByGroupAndTypeGroupDealerID: function (group, panelLv, lv, id, type, dtpickerFrom, dtpickerTo) {
        return sequelize.query(" SELECT serial_number  serialNumber, " +
            " ukv.agentChannelCode, " +
            " concat(ukv.agent_type_name,'(',	ukv.agent_channel_name,')') agentTypeChannel, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " IFNULL(sum(a.sum_insert_amountwithfee),0) moneyAmount, " +
            " IFNULL(sum(a.sum_com),0) comission, " +
            " IFNULL(sum(a.sum_vat_of_com),0) vat_of_com, " +
            " IFNULL(sum(a.sum_withholding_tax_of_com),0) withholding_tax_of_com, " +
            " IFNULL(sum(a.sum_net_com),0) net_com " +
            " FROM userKioskView ukv " +
            " LEFT JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            "  AND (todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            "  AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " LEFT JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " where ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + panelLv +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY	ukv.host_name", { type: sequelize.QueryTypes.SELECT })

    },
    SelectByGroupAndTypeGroupDealerIDWithDetail: function (group, panelLv, lv, id, type, kioskID, dtpickerFrom, dtpickerTo) {
        return sequelize.query(" SELECT serial_number  serialNumber, " +
            " ukv.agentChannelCode, " +
            " concat(ukv.agent_type_name,'(',	ukv.agent_channel_name,')') agentTypeChannel, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " IFNULL(sum(a.sum_insert_amountwithfee),0) moneyAmount, " +
            " IFNULL(sum(a.sum_com),0) comission, " +
            " IFNULL(sum(a.sum_vat_of_com),0) vat_of_com, " +
            " IFNULL(sum(a.sum_withholding_tax_of_com),0) withholding_tax_of_com, " +
            " IFNULL(sum(a.sum_net_com),0) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + panelLv +
            " and ukv.host_name = '" + kioskID + "' " +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY	a.product_group,ukv.host_name", { type: sequelize.QueryTypes.SELECT })

    }
}



module.exports = dailySummaryRepo;
var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var configRepo = {


    findUserKiosk: function (USERNAME, TYPE,GROUP, LV) {
        return sequelize.query("SELECT uk.shop_name,uk.dealer_name,uk.host_name,uk.agent_type_name,uk.agent_channel_name " +
            " FROM	userKioskView uk " +
            " WHERE	uk.agentUsername = '"+USERNAME+"' " +
            " AND uk.agentLevel = " + LV+
            " AND uk.agentGroup = " + GROUP+
            " AND uk.agentTypeCode = " + TYPE, { type: sequelize.QueryTypes.SELECT })
    }
}

module.exports = configRepo;
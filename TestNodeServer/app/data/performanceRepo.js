var db = require('../models');
var dbPool = require('../helper/utility.connection');
var dbMysql = require('../helper/utility.mysql');
var sequelize = dbPool.getConnection();


var dailySummaryRepo = {
    SelectPerformanceByRange: function (startMonth,startYear,endMonth, endYear) {
       
        var sql = " SELECT serial_number  serialNumber, " +
            " CONCAT(ukv.dealer_code ,'_(', ukv.shop_name,')') dealerName, " +
            " CONCAT(ukv.alias ,'_(', ukv.shop_name,')') aliasName, " +
            " sum(a.count_trans) transAmount, " +
            " s.servicename serviceName, " +
            " ukv.alias alias, " +
            " sum(a.sum_insert_amount) moneyAmount, " +
            " sum(a.sum_com) comission, " +
            " sum(a.sum_vat_of_com) vat_of_com, " +
            " sum(a.sum_withholding_tax_of_com) withholding_tax_of_com, " +
            " sum(a.sum_net_com) net_com " +
            " FROM userKioskView ukv " +
            " JOIN kiosk_summarycomission_log a ON a.serial_number = ukv.host_name and a.agentlevel = ukv.agentLevel " +
            " JOIN kio_m_service s ON s.serviceID = a.product_group " +
            " WHERE	(todt) >= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerFrom + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 00:00:00') " +
            " AND (todt) <= concat(DATE_FORMAT( DATE_ADD(STR_TO_DATE('" + dtpickerTo + "', '%d/%m/%Y'), INTERVAL -543 YEAR),'%Y-%m-%d'), ' 23:59:59') " +
            " and ukv.agentTypeCode = '" + type + "' " +
            " and ukv.agentLevel = " + ddlLv +
            " and ukv.host_name = '" + kioskID + "' " +
            (lv == 0 ? "" : " and ukv.agentGroup = " + group) +
            (lv == 2 ? " and ukv.agentuserid = " + id : "") +
            " GROUP BY ukv.agentprofile_id , serviceName";

        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    }
    
}
module.exports = dailySummaryRepo;
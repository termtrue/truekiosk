﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var agentUserRep = {
    findByID: function (ID) {
        return db.kio_t_agentUser.findOne({
            where: {
                agentUserID: ID,
                IsDisable: 0,
                IsLock: 0
            }
        });
    },
    findAllData: function () {
        return db.kio_t_agentUser.findAll();
    },
    findByUsername: function (username) {
        return db.kio_t_agentUser.findOne({
            where: {
                agentUsername: username,
                IsDisable: 0,
                IsLock: 0
            }

        });
    },
    findByUsernameAndPassword: function (username, password) {
        return db.kio_t_agentUser.findOne({
            where: {
                agentUsername: username,
                agentPassword: password,
                IsDisable: 0,
                IsLock: 0
            },
            include: [{ all: true }]
        });
    },

    updatePwdByUsernameAndPwd: function (username, pawword) {
        return db.kio_t_agentUser.update({
            agentPassword: pawword,
        },
            {
                where: {
                    agentUsername: username
                }
            }
        );
    },
    updateLockCounterByUsername: function (username) {
        return db.kio_t_agentUser.update({
            agentLoginFailCounted: sequelize.literal('agentLoginFailCounted +1'),
        },
            {
                where: {
                    agentUsername: username
                }
            }
        );
    },
    updateLockCounterToZero: function (username) {
        return db.kio_t_agentUser.update({
            agentLoginFailCounted: 0,
        },
            {
                where: {
                    agentUsername: username
                }
            }
        );
    }

}

module.exports = agentUserRep;
﻿var db = require('../models');

var activityUserLog = {

    createActivityUserLog: function (logType, logDate, logValue, kioskIDData, userID, createBy, bank1000, bank500, bank100, bank50, bank20, coin10, coin5, coin2, coin1) {
        var stringDate = logDate.split('/');

        return db.kio_t_activityUserLog.create({
            logType: logType,
            logDate: new Date(stringDate[2] - 543 + '-' + stringDate[1] + '-' + stringDate[0]),
            logValue: logValue,
            kioskID: kioskIDData,
            userID: userID,
            bank1000: bank1000,
            bank500: bank500,
            bank100: bank100,
            bank50: bank50,
            bank20: bank20,
            coin10: coin10,
            coin5: coin5,
            coin2: coin2,
            coin1: coin1,
            createDate: new Date(),
            createBy: createBy,
            status: '1'

        });
    },
    updateToDisableLogActivityUser: function (kioskID, logDate) {
        var stringDate = logDate.split('/');
        return db.kio_t_activityUserLog.update({
            logStatus: '0'
        },
            {
                where: {
                    kioskID: kioskID,
                    logDate: new Date(stringDate[2] - 543 + '-' + stringDate[1] + '-' + stringDate[0])
                }
            }
        );
    }
}

module.exports = activityUserLog;
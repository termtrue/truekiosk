﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var ServiceRepo = {
    findServiceProfile: function (LEVEL, TYPE) {
        return sequelize.query(" select sp.serviceID, s.serviceName, s.ServiceNameEn, sp.commission, sp.commissionType, " +
            " CASE WHEN sp.commissionType = 'P' THEN CONCAT(sp.commission, ' % ') ELSE CONCAT(sp.commission, ' ฿') END  com  " +
            " FROM kio_m_serviceProfile sp " +
            " JOIN kio_m_service s ON s.serviceID = sp.serviceID " +
            " where sp.agentTypeID = :type " +
            " and sp.agent_level = :level and s.ServiceNameEn is not null",
            { replacements: { level: LEVEL, type: TYPE }, type: sequelize.QueryTypes.SELECT })
    },
    findServiceMaster: function () {
        return sequelize.query(" select serviceID,serviceName,ServiceNameEn,isactive from kio_m_service where isactive = 1 order by seq ",
            {  type: sequelize.QueryTypes.SELECT })
    }
}

module.exports = ServiceRepo;

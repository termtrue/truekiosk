var db = require('../models');
var dbPool = require('../helper/utility.connection');
var dbMysql = require('../helper/utility.mysql');
var sequelize = dbPool.getConnection();


var dailySummaryRepo = {
    GetAllType: function (callback) {

        var sql = "select typeid,agent_type_code,agent_type_name from kio_v_agentTypeChannel group by typeid,agent_type_code,agent_type_name ";

        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    GetAllTypeByTypeID: function (TypeID, callback) {

        var sql = "select typeid,agent_type_code,agent_type_name from kio_v_agentTypeChannel where typeid = '"+TypeID+"' group by typeid,agent_type_code,agent_type_name ";

        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    GetAllChannel: function (callback) {

        var sql = "select channelID,agent_channel_code,agent_channel_name from kio_v_agentTypeChannel group by channelID,agent_channel_code,agent_channel_name";

        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    },
    GetAllChannelByTypeID: function (typeID, callback) {

        var sql = "select channelID,agent_channel_code,agent_channel_name from kio_v_agentTypeChannel  where typeID = '" + typeID + "' group by channelID,agent_channel_code,agent_channel_name";
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;

            callback(err, data);
        });
    }

}
module.exports = dailySummaryRepo;
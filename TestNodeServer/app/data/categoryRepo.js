var db = require('../models');

var categoryRepo = {
    findAll: function () {
        return db.kio_t_category.findAll({
            where: {
                status: '1'
            }
        });
    }   
}

module.exports = categoryRepo;
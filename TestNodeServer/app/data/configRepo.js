var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var configRepo = {
    findAllData: function () {
        return db.kio_t_config.findAll();
    },
    // PAGE: MAIN STEP05/06/07
    selectConfigValueByConfigName: function (configName) {
        return sequelize.query("SELECT configValue FROM kio_t_config Where configName = '"+configName+"'", {type: sequelize.QueryTypes.SELECT })
    },
    selectConfigValueByType: function (type) {
        return sequelize.query("SELECT * FROM kio_t_config Where type = '" + type + "'", { type: sequelize.QueryTypes.SELECT })
    },
    updateConfigStringSql: function (configValue, configName,create_by) {
        return sequelize.query("UPDATE kio_t_config "+
                                "SET configValue='"+configValue+"',"+
                                "updateDate=  NOW(), updateBy='"+create_by+"'"+
                                "WHERE configCode='"+configName+"';", {type: sequelize.QueryTypes.UPDATE })
    }
}

module.exports = configRepo;
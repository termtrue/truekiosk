var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: '< MySQL username >',
    password: '< MySQL password >',
    database: '<your database name>'
});



var transectionLogRepo = {
    // PAGE: REPORT 03
    findAllByCondition: function (SERIAL_NUMBER, PHONE, REF1, E_WALLET, PRO_GROUP, ERR_CODE, ERR_MSG, DATE_FROM, DATE_TO, TYPE, GROUP, LV) {

        console.log('----------Transaction Repository: findAllByCondition----------');

        var dateFromBuffer = DATE_FROM.split("/");
        var DATE_FROM = dateFromBuffer[0] + '/' + dateFromBuffer[1] + '/' + (parseInt(dateFromBuffer[2]) - 543);


        var dateToBuffer = DATE_TO.split("/");
        var DATE_TO = dateToBuffer[0] + '/' + dateToBuffer[1] + '/' + (parseInt(dateToBuffer[2]) - 543);


        return sequelize.query(" SELECT l.*, kv.agent_channel_name, kv.agent_true_wallet_id, kv.dealer_code, " +
            " kv.agent_wallet_password, kv.agent_kiosk_password, kv.agent_name, kv.agent_lastname, kv.agent_thai_id, " +
            " kv.agent_e_mail, kv.shop_name, kv.agent_address, kv.agent_tel_1, kv.agent_tel_2, kv.agent_tel_3, " +
            " kv.agent_tel_4, kv.agent_tel_5, kv.agentTypeCode, kv.agentChannelCode " +
            " FROM kiosk_transaction_log l " +
            " LEFT JOIN userKioskDetailView kv  on kv.host_name = l.serial_number " +
            " where 1= 1  " +
            " AND kv.agentTypeCode = " + TYPE +
            " AND kv.agentGroup = " + GROUP +
            " AND kv.agentLevel = " + LV +
            (SERIAL_NUMBER != "" ? " AND l.serial_number = '" + SERIAL_NUMBER + "' " : "") +
            (PHONE != "" ? " AND l.phone_no = '" + PHONE + "' " : "") +
            (REF1 != "" ? " AND l.ref1 = '" + REF1 + "' " : "") +
            (E_WALLET != "" ? " AND l.true_wallet_id = '" + E_WALLET + "' " : "") +
            (typeof PRO_GROUP === "undefined" || PRO_GROUP == "99" ? "" : " AND l.product_group = '" + PRO_GROUP + "' ") +
            (typeof ERR_CODE === "undefined" || ERR_CODE == "- Select Error Code -" ? "" : " AND l.result_tmx = '" + ERR_CODE + "' ") +
            (typeof ERR_MSG === "undefined" || ERR_MSG == "- Select Error Message -" ? "" : " AND l.result_msg = '" + ERR_MSG + "' ") +
            (DATE_FROM != "" ? " AND l.time_stamp >= STR_TO_DATE('" + DATE_FROM + "', '%d/%m/%Y')  " : "") +
            (DATE_TO != "" ? " AND l.time_stamp <= STR_TO_DATE('" + DATE_TO + "', '%d/%m/%Y') " : "") +
            " ORDER BY l.time_stamp DESC ",
            { type: sequelize.QueryTypes.SELECT })
    },
    findAllByConditionWithCurrentDate: function (TYPE, GROUP, LEVEL, USER, DATE) {
        console.log('----------Transaction Repository: findAllByConditionWithCurrentDate----------');
        var dateBuffer = DATE.split("/");
        var DATE = dateBuffer[0] + '/' + dateBuffer[1] + '/' + (parseInt(dateBuffer[2]) - 543);

        return sequelize.query(" SELECT l.*, kv.agent_channel_name, kv.agent_true_wallet_id, kv.dealer_code, " +
            " kv.agent_wallet_password, kv.agent_kiosk_password, kv.agent_name, kv.agent_lastname, kv.agent_thai_id, " +
            " kv.agent_e_mail, kv.shop_name, kv.agent_address, kv.agent_tel_1, kv.agent_tel_2, kv.agent_tel_3, " +
            " kv.agent_tel_4, kv.agent_tel_5, kv.agentTypeCode, kv.agentChannelCode " +
            " FROM kiosk_transaction_log l " +
            " LEFT JOIN userKioskDetailView kv  on kv.host_name = l.serial_number " +
            " where 1= 1  " +
            " AND kv.agentTypeCode = " + TYPE +
            " AND kv.agentGroup = " + GROUP +
            (LEVEL != 2 ? " and kv.agentLevel = " + LEVEL + "" : " and kv.agentLevel = " + LEVEL + " and kv.agentusername = '" + USER + "' ") +

            " AND DATE(l.time_stamp) = STR_TO_DATE('" + DATE + "','%d/%m/%Y') " +
            " ORDER BY l.time_stamp DESC ",
            { type: sequelize.QueryTypes.SELECT })
    },
    findAllByUnique: function (UNIQUE) {

        return sequelize.query(" select * from kiosk_transaction_log t join kioskview kr on t.serial_number = kr.host_name where t.time_kiosk_id_unique = '" + UNIQUE + "'",
            { type: sequelize.QueryTypes.SELECT })
    },


    findByKiosk: function (KIOSK) {
        return db.transection_log.findAll({
            where: {
                s_n_kiosk: KIOSK
            }
        });
    },
    findDailyProductReport: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO, KIOSK_ID) {
        return sequelize.query("SELECT " +
            "l.product_name AS col1,  " +
            "CASE WHEN SUM(l.COIN_10) > 0 THEN SUM(l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN SUM(l.COIN_5) > 0 THEN SUM(l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN SUM(l.COIN_2) > 0 THEN SUM(l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN SUM(l.COIN_1) > 0 THEN SUM(l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN SUM(l.BANK_1000) > 0 THEN SUM(l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN SUM(l.BANK_500) > 0 THEN SUM(l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN SUM(l.BANK_100) > 0 THEN SUM(l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN SUM(l.BANK_50) > 0 THEN SUM(l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN SUM(l.BANK_20) > 0 THEN SUM(l.BANK_20) ELSE 0 END  AS col10,  " +
            "SUM(l.insert_amount) AS col11," +
            "SUM(l.fee) AS col12, " +
            "ROUND(SUM(l.select_amount),2) AS col13, " +
            "c.com_rate AS col14 ,  " +
            "ROUND(SUM(c.com),2) AS col15 ,  " +
            "ROUND(SUM(c.vat_of_com),2) AS col16 , " +
            "ROUND(SUM(c.withholding_tax_of_com),2) AS col17 ,  " +
            "ROUND(SUM(c.net_com),2) AS col18 ,  " +
            "l.site_id AS col19 , " +
            "l.code_shop AS col20  " +
            "FROM " +
            "(select * from kiosk_transaction_log " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX')" +
            " l " +
            "left join " +
            "(select * from kiosk_commission_log " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX')" +
            " c on l.transection_id = c.transection_id " +
            "join userKioskView r  on r.host_name = l.serial_number where (1=1)" +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +

            (KIOSK_ID != '' ? " and serial_number = :kioskid " : "") +
            "  and date(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')  AND  STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            "GROUP BY   " +
            "l.product_name " +
            "ORDER BY l.product_name asc",
            { replacements: { type: TYPE, channel: CHANNEL, region: REGION, kioskid: KIOSK_ID, date_from: DATE_FROM, date_to: DATE_TO }, type: sequelize.QueryTypes.SELECT })
    },
    findDetailDailyProductReport01: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO, PRODUCT_NAME, KIOSK_ID) {
        console.log(PRODUCT_NAME);
        return sequelize.query("SELECT " +
            "DATE_FORMAT(l.time_stamp,'%d/%m/%Y') AS day, " +
            "l.product_name AS col1,   " +
            "CASE WHEN SUM(l.COIN_10) > 0 THEN SUM(l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN SUM(l.COIN_5) > 0 THEN SUM(l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN SUM(l.COIN_2) > 0 THEN SUM(l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN SUM(l.COIN_1) > 0 THEN SUM(l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN SUM(l.BANK_1000) > 0 THEN SUM(l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN SUM(l.BANK_500) > 0 THEN SUM(l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN SUM(l.BANK_100) > 0 THEN SUM(l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN SUM(l.BANK_50) > 0 THEN SUM(l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN SUM(l.BANK_20) > 0 THEN SUM(l.BANK_20) ELSE 0 END  AS col10,  " +
            "SUM(l.insert_amount) AS col11, " +
            "SUM(l.fee) AS col12, " +
            "ROUND(SUM(l.select_amount),2) AS col13, " +
            "c.com_rate AS col14 ,  " +
            "ROUND(SUM(c.com),2) AS col15 ,  " +
            "ROUND(SUM(c.vat_of_com),2) AS col16 , " +
            "ROUND(SUM(c.withholding_tax_of_com),2) AS col17 ,  " +
            "ROUND(SUM(c.net_com),2) AS col18 ,  " +
            "l.site_id AS col19 , " +
            "l.code_shop AS col20  " +
            "FROM  " +
            "(select * from kiosk_transaction_log " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX') " +
            " l " +
            "left join " +
            "(select * from kiosk_commission_log " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX') " +
            "c on l.transection_id = c.transection_id " +
            "join userKioskView r  on r.host_name = l.serial_number where (1=1)" +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +

            (KIOSK_ID != '' ? " and serial_number = :kioskid " : "") +
            "AND date(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')   " +
            "AND STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            "AND l.product_name = :product_name " +
            "GROUP BY   " +
            "l.product_name, date(l.time_stamp) " +
            "ORDER bY date(l.time_stamp) desc ",
            { replacements: { type: TYPE, channel: CHANNEL, region: REGION, kioskid: KIOSK_ID, date_from: DATE_FROM, date_to: DATE_TO, product_name: PRODUCT_NAME }, type: sequelize.QueryTypes.SELECT })
    },

    // PAGE: REPORT02 / REPORT04 / REPORT06
    findDetailTransectionReportMain: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO, KIOSK_ID, LV) {
        return sequelize.query("SELECT " +
            "r.host_name AS col1,   " +
            "CASE WHEN SUM(l.COIN_10) > 0 THEN SUM(l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN SUM(l.COIN_5) > 0 THEN SUM(l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN SUM(l.COIN_2) > 0 THEN SUM(l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN SUM(l.COIN_1) > 0 THEN SUM(l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN SUM(l.BANK_1000) > 0 THEN SUM(l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN SUM(l.BANK_500) > 0 THEN SUM(l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN SUM(l.BANK_100) > 0 THEN SUM(l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN SUM(l.BANK_50) > 0 THEN SUM(l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN SUM(l.BANK_20) > 0 THEN SUM(l.BANK_20) ELSE 0 END  AS col10,  " +
            " TRUNCATE (SUM(IFNULL(l.insert_amount,0)), 2) AS col11, " +
            " TRUNCATE (SUM(IFNULL(l.select_amount, 0)), 2) AS col12, " +
            " '' AS col13, " +
            " IFNULL(c.com_rate, 0) AS col14, " +
            " ROUND(SUM(IFNULL(c.com, 0)), 2) AS col15, " +
            " ROUND(SUM(IFNULL(c.vat_of_com, 0)), 2) AS col16, " +
            " ROUND(SUM(IFNULL(c.withholding_tax_of_com, 0)), 2) AS col17, " +
            " ROUND(SUM(IFNULL(c.net_com, 0)), 2) AS col18," +
            " l.product_name AS col19 , " +
            " l.code_shop AS col20 , " +
            "TRUNCATE(SUM(CASE WHEN l.harvest = 1 THEN l.insert_amount ELSE 0 END ), 2) AS col21, " +
            "TRUNCATE(SUM(CASE WHEN l.harvest = 0 THEN l.insert_amount ELSE 0 END ), 2) AS col22 " +
            "FROM  userKioskView r   " +
            " left join kiosk_transaction_log l on r.host_name = l.serial_number and 	l.transection_id != '' " +
            " AND l.transection_id != '000000000000000' " +
            " AND l.transection_id != '00000000000000' " +
            " AND l.transection_id != '00017201XXXXXXXX' " +
            " AND DATE(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')  AND STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            " left join kiosk_commission_log c on l.time_kiosk_id_unique = c.time_kiosk_id_unique and 	c.transection_id != ''  " +
            " AND c.transection_id != '000000000000000'  " +
            " AND c.transection_id != '00000000000000'  " +
            " AND c.transection_id != '00017201XXXXXXXX' " +
            " AND c.agentLevel = :lv " +
            " WHERE (1 = 1) " +

            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +
            (LV != '' ? " and r.agentLevel = :lv  " : "") +
            (KIOSK_ID != '' ? " and l.serial_number = :kioskid " : "") +
            "GROUP BY   " +
            "r.host_name " +
            "ORDER bY r.host_name desc ",
            { replacements: { lv: LV, type: TYPE, channel: CHANNEL, region: REGION, kioskid: KIOSK_ID, date_from: DATE_FROM, date_to: DATE_TO }, type: sequelize.QueryTypes.SELECT })
    },
    findDetailTransectionReport: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO, KIOSK_ID) {
        return sequelize.query("SELECT " +
            "DATE_FORMAT(l.time_stamp, ' %d/%m/%Y %T') AS col1,   " +
            "CASE WHEN (l.COIN_10) > 0 THEN (l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN (l.COIN_5) > 0 THEN (l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN (l.COIN_2) > 0 THEN (l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN (l.COIN_1) > 0 THEN (l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN (l.BANK_1000) > 0 THEN (l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN (l.BANK_500) > 0 THEN (l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN (l.BANK_100) > 0 THEN (l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN (l.BANK_50) > 0 THEN (l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN (l.BANK_20) > 0 THEN (l.BANK_20) ELSE 0 END  AS col10,  " +
            "(l.insert_amount) AS col11, " +
            "(l.select_amount) AS col12, " +
            "l.product_name AS col13, " +
            "c.com_rate AS col14 ,  " +
            "ROUND((c.com),2) AS col15 ,  " +
            "ROUND((c.vat_of_com),2) AS col16 , " +
            "ROUND((c.withholding_tax_of_com),2) AS col17 ,  " +
            "ROUND((c.net_com),2) AS col18 ,  " +
            "l.site_id AS col19 , " +
            "l.code_shop AS col20  " +
            "FROM  " +
            "(select * from kiosk_transaction_log " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX')" +
            " l " +
            "left join " +
            "(select * from kiosk_commission_log  " +
            "where transection_id != '' && transection_id != '000000000000000' " +
            "&& transection_id != '00000000000000' && transection_id != '00017201XXXXXXXX')" +
            " c on l.transection_id = c.transection_id " +
            "join userKioskView r  on r.host_name = l.serial_number where (1=1) " +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +

            (KIOSK_ID != '' ? " and l.serial_number = :kioskID " : "") +
            " AND DATE(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')   " +
            " AND STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            "ORDER bY l.time_stamp desc ",
            { replacements: { type: TYPE, channel: CHANNEL, region: REGION, date_from: DATE_FROM, date_to: DATE_TO, kioskID: KIOSK_ID }, type: sequelize.QueryTypes.SELECT })
    },
    findDetailErrorReportMain: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO) {
        return sequelize.query("SELECT " +
            'serial_number AS col1,   ' +
            "CASE WHEN SUM(l.COIN_10) > 0 THEN SUM(l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN SUM(l.COIN_5) > 0 THEN SUM(l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN SUM(l.COIN_2) > 0 THEN SUM(l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN SUM(l.COIN_1) > 0 THEN SUM(l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN SUM(l.BANK_1000) > 0 THEN SUM(l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN SUM(l.BANK_500) > 0 THEN SUM(l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN SUM(l.BANK_100) > 0 THEN SUM(l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN SUM(l.BANK_50) > 0 THEN SUM(l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN SUM(l.BANK_20) > 0 THEN SUM(l.BANK_20) ELSE 0 END  AS col10,  " +
            "COUNT(l.result)  AS col11, " +
            "''  AS col12, " +
            "''  AS col13 " +
            "FROM  " +
            "kiosk_transaction_log  l   " +
            "join userKioskView r  on r.host_name = l.serial_number where (1=1)" +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +

            " AND DATE(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')   " +
            " AND STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            " AND l.result  not like '%/0' " +
            "GROUP BY   " +
            "l.serial_number " +
            "ORDER bY l.serial_number desc ",
            { replacements: { type: TYPE, channel: CHANNEL, region: REGION, date_from: DATE_FROM, date_to: DATE_TO }, type: sequelize.QueryTypes.SELECT })
    },
    findDetailErrorReport: function (TYPE, CHANNEL, REGION, DATE_FROM, DATE_TO, KIOSK_ID) {
        return sequelize.query("SELECT " +
            "DATE_FORMAT(l.time_stamp, ' %d/%m/%Y %T') AS col1,   " +
            "CASE WHEN (l.COIN_10) > 0 THEN (l.COIN_10) ELSE 0 END  AS col2,   " +
            "CASE WHEN (l.COIN_5) > 0 THEN (l.COIN_5) ELSE 0 END  AS col3, " +
            "CASE WHEN (l.COIN_2) > 0 THEN (l.COIN_2) ELSE 0 END  AS col4, " +
            "CASE WHEN (l.COIN_1) > 0 THEN (l.COIN_1) ELSE 0 END  AS col5, " +
            "CASE WHEN (l.BANK_1000) > 0 THEN (l.BANK_1000) ELSE 0 END  AS col6, " +
            "CASE WHEN (l.BANK_500) > 0 THEN (l.BANK_500) ELSE 0 END  AS col7, " +
            "CASE WHEN (l.BANK_100) > 0 THEN (l.BANK_100) ELSE 0 END  AS col8, " +
            "CASE WHEN (l.BANK_50) > 0 THEN (l.BANK_50) ELSE 0 END  AS col9, " +
            "CASE WHEN (l.BANK_20) > 0 THEN (l.BANK_20) ELSE 0 END  AS col10,  " +
            "l.result  AS col11, " +
            "'Desc' AS col12, " +
            "l.product_name   AS col13 " +
            "FROM  " +
            "kiosk_transaction_log  l   " +
            "join userKioskView r  on r.host_name = l.serial_number where (1=1) " +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +

            " AND DATE(l.time_stamp)  BETWEEN STR_TO_DATE(:date_from, '%d/%m/%Y')   " +
            " AND STR_TO_DATE(:date_to, '%d/%m/%Y')  " +
            " AND l.result  not like '%/0' " +
            " AND l.serial_number = :kioskID " +
            "ORDER bY l.time_stamp desc ",
            { replacements: { type: TYPE, channel: CHANNEL, region: REGION, date_from: DATE_FROM, date_to: DATE_TO, kioskID: KIOSK_ID }, type: sequelize.QueryTypes.SELECT })
    },
    findDailySummaryDashboard: function (TYPE, CHANNEL, LEVEL, DAY) {

        return sequelize.query("SELECT date(log.time_stamp) as day," +
            "sum(log.insert_amount) moneyCount," +
            "(select count(distinct log.serial_number) a  FROM kiosk_transaction_log log join userKioskView r  on r.host_name = log.serial_number " +
            " join kio_t_agentUserGroup ug on ug.agentprofile_id = r.agentprofile_id " +
            " where (1=1)  " +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +
            (LEVEL != 'ALL' ? " and ug.agent_level = :level  " : "") +

            " ) kioskCount  , " +
            "count(*) transCount, " +
            "(select count(*) a  FROM kiosk_transaction_log log join userKioskView r  on r.host_name = log.serial_number " +
            " join kio_t_agentUserGroup ug on ug.agentprofile_id = r.agentprofile_id " +
            " where(1 = 1) " +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +
            (LEVEL != 'ALL' ? " and ug.agent_level = :level  " : "") +

            "and log.result  not like '%/0' and date(log.time_stamp) = trim(:day)) notificationCount " +
            "FROM kiosk_transaction_log log " +
            "join userKioskView r  on r.host_name = log.serial_number " +
            " join kio_t_agentUserGroup ug on ug.agentprofile_id = r.agentprofile_id " +
            " where (1=1)" +
            (TYPE != 'ALL' ? " and r.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and r.agentChannelCode = :channel  " : "") +
            (LEVEL != 'ALL' ? " and ug.agent_level = :level  " : "") +

            " and date(log.time_stamp) = trim(:day) " +
            "group by day ",
            { replacements: { type: TYPE, channel: CHANNEL, level: LEVEL, day: DAY }, type: sequelize.QueryTypes.SELECT });
    },
    findDailySummaryDashboardByLevel: function (LEVEL, GROUP, DAY) {
        var lvLoop = 0;

        switch (LEVEL) {
            case 0:
                lvLoop = 2;
                break;
            case 1:
                lvLoop = 1;
                break;
            default:
                lvLoop = 0;
                break;
        }

        var qry = "";
        for (var i = 0; i <= lvLoop; i++) {
            qry += (i != 0 ? " union all " : "");
            qry += "SELECT date(log.time_stamp) as day," +
                "sum(log.insert_amount) moneyCount," +
                "(select count(distinct log.serial_number) a  FROM kiosk_transaction_log log join userKioskView r  on r.host_name = log.serial_number " +
                " join userKioskView ukv on ukv.agentprofile_id = r.agentprofile_id  " +
                " where (1=1)  " +
                (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = :group  " : "") +
                (LEVEL != 'ALL' ? " and ukv.agentLevel = " + i + "  " : "") +

                " ) kioskCount  , " +
                "count(*) transCount, " +
                "(select count(*) a  FROM kiosk_transaction_log log join userKioskView r  on r.host_name = log.serial_number " +
                " join userKioskView ukv on ukv.agentprofile_id = r.agentprofile_id  " +
                " where(1 = 1) " +
                (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = :group  " : "") +
                (LEVEL != 'ALL' ? " and ukv.agentLevel = " + i + "   " : "") +

                "and log.result  not like '%/0' and date(log.time_stamp) = trim(:day)) notificationCount " +
                "FROM kiosk_transaction_log log " +
                "join userKioskView r  on r.host_name = log.serial_number " +
                " join userKioskView ukv on ukv.agentprofile_id = r.agentprofile_id  " +
                " where (1=1)" +
                (GROUP != 'ALL' && GROUP != 0 ? " and ukv.agentGroup = :group  " : "") +
                (LEVEL != 'ALL' ? " and ukv.agentLevel = " + i + "  " : "") +

                " and date(log.time_stamp) = trim(:day) " +
                "group by day ";
        }

        return sequelize.query(qry,
            { replacements: { group: GROUP, day: DAY }, type: sequelize.QueryTypes.SELECT });
    },




    // PAGE: MAIN STEP01
    findLastDayHasData: function (TYPE, GROUP, REGION, USERID, LEVEL) {
        return sequelize.query(
            " SELECT  IFNULL(DATE_FORMAT(log.time_stamp, ' %Y/%m/%d'),NOW()) as lstTime FROM kiosk_transaction_log log right join kio_t_agentUserGroup  r on r.serial_number = log.serial_number " +
            " where (1=1) " +
            (LEVEL < 3 ? " and r.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and r.group_id = :group  " : "") +
            (LEVEL == 2 ? " and r.user_id= :userid  " : "") +
            " and r.agent_level = :level  " +
            " order by log.time_stamp desc  limit 1 ",
            { replacements: { type: TYPE, group: GROUP, region: REGION, userid: USERID, level: LEVEL }, type: sequelize.QueryTypes.SELECT });
    },
    // PAGE: MAIN STEP02
    findDailySummaryDashboardByLevel_Header: function (TYPE, LEVEL, GROUP, DAY, USERID) {

        var qry = "";
        qry = "select t.*, IFNULL(k.kioskCount,0) kioskCount from (SELECT IFNULL(date(log.time_stamp),now()) AS DAY, " +
            " IFNULL(sum(log.insert_amount),0) moneyCount, " +
            " IFNULL(count(*),0) transCount, " +
            " IFNULL(SUM(IF(log.result_tmx != '0' AND log.result_tmx != 'Cancel', 1, 0)),0) AS notificationCount, " +
            " ukv.agent_type, " +
            " ukv.group_id ,ukv.agent_level" +
            " FROM kiosk_transaction_log log " +
            " JOIN kio_t_agentUserGroup ukv ON log.serial_number = ukv.serial_number  " +
            " where (1=1)  " +
            (LEVEL < 3 ? " and ukv.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and ukv.group_id = :group  " : "") +
            " and ukv.agent_level = :level  " +
            (LEVEL == 2 ? " and ukv.user_id= :userid  " : "") +
            " and date(log.time_stamp) = trim(:day) " +
            "group by day ) t " +
            " join (SELECT 	count(t.agentprofile_id) kioskCount, t.agent_type, t.group_id, t.agent_level " +
            " FROM kio_t_agentUserGroup t 	WHERE	(1 = 1)	" +
            (LEVEL < 3 ? " and t.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and t.group_id = :group  " : "") +
            " and t.agent_level = :level  " +
            (LEVEL == 2 ? " and t.user_id = :userid  " : "") +
            " group by t.agent_level ) " +
            " k on t.agent_level = k.agent_level  ";
        return sequelize.query(qry,
            { replacements: { type: TYPE, level: LEVEL, group: GROUP, day: DAY, userid: USERID }, type: sequelize.QueryTypes.SELECT });
    },
    // PAGE: MAIN STEP03
    findDailySummaryDashboardByLevel_Detail_L1: function (TYPE, LEVEL, GROUP, DAY, USERID) {
        var qry = "";
        qry = " select t.*,k.kioskCount from " +
            " ( SELECT concat(c.agent_channel_name) kioskname, date(log.time_stamp) AS DAY, " +
            " sum(log.insert_amount) moneyCount, " +
            " count(*) transCount, " +
            " SUM(IF(log.result_tmx != '0' AND log.result_tmx != 'Cancel', 1, 0)) AS notificationCount, " +
            " ukv.agent_type, " +
            " ukv.group_id, ukv.agent_level " +
            " FROM kiosk_transaction_log log " +
            " JOIN kio_t_agentUserGroup ukv ON log.serial_number = ukv.serial_number  " +
            " join agent_channel c on ukv.agent_channel = c.id " +
            " where (1=1)  " +
            (LEVEL < 3 ? " and ukv.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and ukv.group_id = :group  " : "") +
            " and ukv.agent_level = :level  " +
            (LEVEL == 2 ? " and ukv.user_id= :userid  " : "") +
            " and date(log.time_stamp) = trim(:day) " +
            "group by day,ukv.agent_type, ukv.group_id ) t " +
            " join (SELECT DISTINCT	count(ukv.agentprofile_id) kioskCount, ukv.agent_type, ukv.group_id " +
            " FROM kio_t_agentUserGroup ukv WHERE	(1 = 1)	" +
            (LEVEL < 3 ? " and ukv.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and ukv.group_id = :group  " : "") +
            " and ukv.agent_level = :level  " +
            (LEVEL == 2 ? " and ukv.user_id= :userid  " : "") +
            " group by ukv.agent_type, ukv.group_id  ) " +
            " k on t.agent_type = k.agent_type and 	t.group_id = k.group_id ";


        return sequelize.query(qry,
            { replacements: { type: TYPE, level: LEVEL, group: GROUP, day: DAY, userid: USERID }, type: sequelize.QueryTypes.SELECT });
    },
    // PAGE: MAIN STEP04
    findDailySummaryDashboardByLevel_Detail_L2: function (TYPE, LEVEL, GROUP, DAY, USERID) {
        var qry = "";


        qry = "SELECT p.shop_name shopName, date(log.time_stamp) AS DAY, " +
            " IF(sum(log.insert_amount) is not null, sum(log.insert_amount), 0) moneyCount, " +
            " IF(sum(log.insert_amount) is not null, count(*) , 0) transCount, " +
            " SUM(IF(log.result_tmx != '0' AND log.result_tmx != 'Cancel', 1, 0)) AS notificationCount, " +
            " ukv.agent_type, " +
            " ukv.group_id, ukv.agent_level " +
            " FROM kiosk_transaction_log log " +
            " right JOIN kio_t_agentUserGroup ukv ON log.serial_number = ukv.serial_number  " +
            " right join agent_profile_lv1 p on ukv.agentprofile_id = p.id " +
            " and date(log.time_stamp) = trim(:day) " +
            " where (1=1)  " +
            (LEVEL < 3 ? " and ukv.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and ukv.group_id = :group  " : "") +
            " and ukv.agent_level = :level  " +
            (LEVEL == 2 ? " and ukv.user_id= :userid  " : "") +

            "group by day,ukv.agentprofile_id ";


        return sequelize.query(qry,
            { replacements: { type: TYPE, level: LEVEL, group: GROUP, day: DAY, userid: USERID }, type: sequelize.QueryTypes.SELECT });
    },
    // PAGE: MAIN STEP10
    findDailySummaryMainChart: function (TYPE, LEVEL, GROUP, REGION, USERID, CHARDTYPE) {

        var selectParam = "";
        var groupParam = "";
        var orderParam = "";

        switch (CHARDTYPE) {
            case "1":
                selectParam = " DATE_FORMAT(log.time_stamp, '%d/%m/%Y') dateData, ";
                groupParam = " group by date(log.time_stamp) ";
                orderParam = " order by date(log.time_stamp) desc limit 7 ";
                break;
            case "2":
                selectParam = " DATE_FORMAT(log.time_stamp, '%d/%m/%Y') dateData, ";
                groupParam = " group by date(log.time_stamp) ";
                orderParam = " order by date(log.time_stamp) desc limit 30 ";
                break;
            default:
                selectParam = " DATE_FORMAT(log.time_stamp, ' %m/%Y') dateData, ";
                groupParam = " group by month(log.time_stamp), year(log.time_stamp) ";
                orderParam = " order by month(log.time_stamp), year(log.time_stamp) desc limit 12 ";
                break;
        }


        return sequelize.query("SELECT " +
            selectParam +
            " sum(log.insert_amount) moneyData " +
            " FROM kiosk_transaction_log log  " +
            " join kio_t_agentUserGroup ukv  on ukv.serial_number = log.serial_number where (1=1)" +
            (LEVEL < 3 ? " and ukv.agent_type = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and ukv.group_id = :group  " : "") +
            " and ukv.agent_level = :level  " +
            (LEVEL == 2 ? " and ukv.user_id= :userid  " : "") +

            groupParam +
            orderParam,
            { replacements: { type: TYPE, level: LEVEL, group: GROUP, region: REGION, userid: USERID }, type: sequelize.QueryTypes.SELECT });
    },
}

module.exports = transectionLogRepo;

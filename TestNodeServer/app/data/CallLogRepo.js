﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var dbMysql = require('../helper/utility.mysql');
var sequelize = dbPool.getConnection();

var callLogRepo = {
    insertCallLog02: function (TimeUinque, Value, User) {
        return sequelize.query(" INSERT INTO kiosk_call_log (time_kiosk_id_unique, callLogValue, createDate, createBy) " +
            " VALUES ('" + TimeUinque + "', " + Value + ", now(), '" + User + "') ",
            { type: sequelize.QueryTypes.INSERT })
    },
    insertCallLog: function (TimeUinque, Value, User, callback) {
        var sql = " INSERT INTO kiosk_call_log (time_kiosk_id_unique, callLogValue, createDate, createBy) " +
            " VALUES ('" + TimeUinque + "', " + Value + ", now(), '" + User + "') ";
        console.log(sql);
        dbMysql.qry(sql, (err, data) => {
            if (err) throw err;
            callback(err, data);
        });
    }

}

module.exports = callLogRepo;
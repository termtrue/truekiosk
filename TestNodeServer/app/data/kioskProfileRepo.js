﻿var db = require('../models');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();
var KioskRepo = {

    findOneDataByRole: function (agentID) {
        console.log('----------Kiosk Repository: findOneDataByRole----------');
        return sequelize.query("SELECT kp.* " +
            "FROM kio_t_agent_kiosk_role akr join kioskview kr on akr.roleID = kr.roleID " +
            "join kio_t_kioskProfile kp on kr.kioskID = kp.kioskProfileID " +
            "where akr.agentID = :agentID",
            { replacements: { agentID: agentID }, type: sequelize.QueryTypes.SELECT })
    },
    // USED BY KIOSK DETAIL
    findDataByTypeAndChannel: function (TYPE, GROUP, LEVEL, USER) {
        console.log('----------Kiosk Repository: FindDataByTypeAndChannel----------');

        return sequelize.query("SELECT kr.host_name,kr.shop_name,kr.agent_name,kr.agent_lastname,kr.agent_address, mp.operation_date, mp.Latitude, mp.Longitude  " +
            " FROM userKioskDetailView kr  " +
            " join main_profile mp on kr.host_name = mp.site_id COLLATE utf8_unicode_ci" +
            " where (1 = 1)   " +
            (LEVEL < 3 ? " and kr.agentTypeCode = :type  " : "") +
            (LEVEL < 3 && LEVEL > 0 ? " and kr.agentGroup = :group  " : "") +
            (LEVEL != 2 ? " and kr.agentLevel = " + LEVEL + "" : " and kr.agentLevel = " + LEVEL + " and kr.agentusername = '" + USER + "'"),

            { replacements: { type: TYPE, group: GROUP, level: LEVEL, user: USER }, type: sequelize.QueryTypes.SELECT })
    },
    findDataByUserID: function (USERNAME) {
        console.log('----------Kiosk Repository: findDataByUserID----------');

        return sequelize.query("SELECT kp.host_name , kp.active, kmp.Latitude, kmp.Longitude " +
            " FROM kioskview kr " +
            " join host kp on kr.kioskSerialNum = kp.host_name " +
            " join kio_m_kiosk_main_profile kmp on kmp.site_id = kp.host_name " +
            " where (1=1)  " +
            (TYPE != 'ALL' ? " and kr.agentTypeCode = :type  " : "") +
            (CHANNEL != 'ALL' && CHANNEL != 0 ? " and kr.agentChannelCode = :channel  " : ""),

            { replacements: { type: TYPE, channel: CHANNEL }, type: sequelize.QueryTypes.SELECT })
    }
}

module.exports = KioskRepo;
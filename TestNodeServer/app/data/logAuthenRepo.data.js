﻿var db = require('../models');

var logAuthenRepo = {
    findAllData: function () {
        return db.kio_t_logAuthen.findAll();
    },

    findByID: function (ID) {
        return db.kio_t_logAuthen.findAll({
            where: {
                logID: ID
            }
        });
    },

    createLog: function (user_id, login_status, error_code, remark, create_by, user_ip) {
        return db.kio_t_logAuthen.create({
            logUserID: user_id,
            logLoginStatus: login_status,
            logErrorCode: error_code,
            logRemark: remark,
            createDate: new Date(),
            createBy: create_by,
            logUserIP: user_ip
        });
    }
}

module.exports = logAuthenRepo;
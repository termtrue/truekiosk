var db = require('../models');
var fs = require('fs');
var dbPool = require('../helper/utility.connection');
var sequelize = dbPool.getConnection();

var advertRepo = {
    findAllData: function () {
        return sequelize.query("SELECT a.*, DATE_FORMAT(a.startDate,'%d/%m/%Y') as startDateFormat, DATE_FORMAT(a.endDate,'%d/%m/%Y') as endDateFormat,c.categoryName "+
                                    'FROM kio_t_advert a inner join '+
                                    'kio_t_category c on a.categoryID = c.categoryID', {type: sequelize.QueryTypes.SELECT })
    },
    // PAGE: MAIN STEP08
    selectRandomAdvert: function (limit) {
        return sequelize.query("SELECT * FROM kio_t_advert "+
                                    "WHERE status = '1' and title != 'default' "+
                                    "and startDate <= NOW() and endDate >= NOW() "+
                                    "ORDER BY RAND() "+
                                    "LIMIT "+limit, {type: sequelize.QueryTypes.SELECT })
    },
     // PAGE: MAIN STEP09
    selectDefaultAdvert: function () {
        return sequelize.query("SELECT * FROM kio_t_advert where title = 'default' ", {type: sequelize.QueryTypes.SELECT })
    },
    insertAdvert: function (title, url, description, picture_name, start_date, end_date, status, category_id, create_by) {
        return db.kio_t_advert.create({
            title: title,
            url: url,
            description: description,
            pictureName: picture_name,
            startDate: start_date,
            endDate: end_date,
            status: status,
            categoryID: category_id,
            createDate: new Date(),
            createBy: create_by
        });
    },
    updateAdvert: function (title, url, description, picture_name, start_date, end_date, status, category_id, create_by,advert_id) {
        return db.kio_t_advert.update({
            title: title,
            url: url,
            description: description,
            pictureName: picture_name,
            startDate: start_date,
            endDate: end_date,
            status: status,
            categoryID: category_id,
            createDate: new Date(),
            createBy: create_by
            },
            {   where: {
                     advertID: 1
                    }
            }
        );
    },
    updateAdvertStringSql: function (title, url, description, picture_name, start_date, end_date, status, category_id, create_by,advert_id) {
        return sequelize.query("UPDATE kio_t_advert "+
                                "SET title='"+title+"',url='"+url+"',description='"+description+"', pictureName='"+picture_name+"', "+
                                " startDate = STR_TO_DATE('"+start_date+"', '%d/%m/%Y') , endDate=STR_TO_DATE('"+end_date+"', '%d/%m/%Y') , status='"+status+"', categoryID='"+category_id+"', "+
                                "updateDate=  NOW(), updateBy='"+create_by+"'"+
                                "WHERE advertID='"+advert_id+"';", {type: sequelize.QueryTypes.UPDATE })
    },    
    insertAdvertStringSql: function (title, url, description, picture_name, start_date, end_date, status, category_id, create_by) {
        return sequelize.query("INSERT INTO kio_t_advert (title,url,description,pictureName,startDate,endDate,status,categoryID,createDate,createBy)"+
                                "VALUES ('"+title+"','"+url+"','"+description+"','"+picture_name+"',STR_TO_DATE('"+start_date+"', '%d/%m/%Y'), "+
                                "STR_TO_DATE('"+end_date+"', '%d/%m/%Y'),'"+status+"','"+category_id+"',NOW(),'"+create_by+"');"
                                , {type: sequelize.QueryTypes.INSERT })
    },
    deleteAdvertStringSql: function (advert_id) {
        return sequelize.query("DELETE FROM kio_t_advert WHERE advertID='"+advert_id+"';"
                                , {type: sequelize.QueryTypes.DELETE })
    },
    updateImageAdvert: function (advert_id,image) {
        var imageData = fs.readFileSync(__dirname + image);
        console.log(">>>>>>>>>>>>>>>>>>>"+advert_id);
        return db.kio_t_advert.update({
            image:imageData
            },
            {   where: {
                     advertID: advert_id
                    }
            }
        );
    }
}

module.exports = advertRepo;
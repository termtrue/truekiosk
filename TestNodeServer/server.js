﻿process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('./config/express');
var app = express();
var db = require('./app/models');

function startApp() {
    app.listen(3000, function () {
        console.log('Express server listening on port ' + 80);
    });
}

db.sequelize.sync()
    .then(startApp)
    .catch(function (e) {
        throw new Error(e);
    });

module.exports = app;




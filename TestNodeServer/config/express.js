﻿var express = require('express');
var session = require('express-session');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var sass = require('node-sass-middleware');
var fileUpload = require('express-fileupload');
module.exports = function () {
    var app = express();

    if (process.env.NODE_ENV == 'development') {
        app.use(morgan('dev'));
    } else {
        app.use(compression);
    }

    app.set('trust proxy', 1) // trust first proxy
    app.set('views', './app/views');
    app.set('view engine', 'ejs');
    
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(fileUpload());
    app.use(session({
        secret: 'keyboard cat',
        resave: true,
        saveUninitialized: true,
        cookie: { maxAge: 600000 },
        rolling: true
    }));   

    app.use(function (req, res, next) {
        if (typeof req.session.user != 'undefined' && req.session.user){
            res.locals.userType = req.session.user.agentType;
            res.locals.agentTypeCode = req.session.user.agentTypeCode;
            res.locals.agentChannelCode = req.session.user.agentChannelCode;
            res.locals.agentLevel = req.session.user.agentLevel;
        }
        next();
    });

    require('../app/routes/index.route')(app);

    require('../app/routes/user.route')(app);
    require('../app/routes/kiosk.route')(app);
    require('../app/routes/main.route')(app);
    require('../app/routes/report.route')(app);
    require('../app/routes/profile.route')(app);
      
    require('../app/routes/account.route')(app);
    require('../app/routes/advert.route')(app);

    app.use(express.static(__dirname + '../public'));
    app.use(express.static('./public'));  

    return app;
};

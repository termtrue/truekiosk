﻿var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'test'
        },
        port: 3306,
        db: {
            database: "test",
            user: "boy",
            password: "boy2530",
            options: {
                host: 'localhost',
                dialect: 'mysql',

                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            }
        }
    }
}

module.exports = config[env];
$('.upload-btn').on('click', function () {

});

$('#btnChooseFile').on('click', function () {
    $('#upload-input').click();
    $('.progress-bar').text('0%');
    $('.progress-bar').width('0%');
});

$('#btnCommitFile').on('click', function () {
    var files = $('#upload-input').get(0).files;
    var formData = new FormData();

    // loop through all the selected files and add them to the formData object
    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // add the files to formData object for the data payload
        formData.append('uploads[]', file, file.name);
    }

    $.ajax({
        url: '/user/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        xhr: function () {
            // create an XMLHttpRequest
            var xhr = new XMLHttpRequest();

            // listen to the 'progress' event
            xhr.upload.addEventListener('progress', function (evt) {

                if (evt.lengthComputable) {
                    // calculate the percentage of upload completed
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);

                    // update the Bootstrap progress bar with the new percentage
                    $('.progress-bar').text(percentComplete + '%');
                    $('.progress-bar').width(percentComplete + '%');

                    // once the upload reaches 100%, set the progress bar text to done
                    if (percentComplete === 100) {
                        $('.progress-bar').html('Done').then(function () { showUploadResult(); alert('ss') });

                    }

                }

            }, false);

            return xhr;
        }
    });
});

$('#upload-input').on('change', function () {

    var files = $(this).get(0).files;

    if (files.length > 0) {
        // create a FormData object which will be sent as the data payload in the
        // AJAX request
        if (files.length > 100000) {
            alert('��Ҵ����Թ��˹�');
            return false;
        }
    }
});

function showUploadResult() {

    var table = $('#dtTable').DataTable({
        "bDestroy": true,
        'bStateSave': true,
        "fnDraw": false,
        "bLengthChange": false,
        "responsive": true,
        "language": {
            "lengthMenu": "�ʴ��� _MENU_ ���˹��ྨ",
            "zeroRecords": "��辺������",
            "info": "�ʴ��ŷ��˹�� _PAGE_ �ҡ _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search": "����:",
            "paginate": {
                "first": "�������",
                "last": "�ش����",
                "next": "����",
                "previous": "��͹˹��"
            }
        },

        "ajax": {

            "url": '/user/userUploadPage',
            "type": 'POST',
            "data": {
                sel: $('#sel1').val(),
                dtpickerFrom: $('#dtpickerFrom').val(),
                dtpickerTo: $('#dtpickerTo').val()
            },
            "dataSrc": ""
        },
        "columns": [
            {
                "className": 'details-control',
                "orderable": true,
                "data": null,
                "defaultContent": ''
            },
            { "data": "col1" },
            { "data": "col2" },
            { "data": "col3" },
            { "data": "col4" },
            { "data": "col5" }
        ]
    });

    $('#adsTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            if ($('#sel1').val() == '1') {
                row.child(format1(row.data())).show();
            } else {
                row.child(format2(row.data())).show();
            }
            tr.addClass('shown');
        }
    });
    return false;
}
